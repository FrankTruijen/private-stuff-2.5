/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

#include "gps_se873.h"

struct _gps_se873_handle {
    int fd;
};

#define COMMAND_PARAMETERS_NR           32
#define COMMAND_PARAMETERS_LEN          32

typedef struct {
    char s[COMMAND_PARAMETERS_NR][COMMAND_PARAMETERS_LEN];
    uint32_t nr;
} command_parameters_t;

static const char *MatchCommandPatternNMEA0183(const char *buf, int len)
{   int i = 0, j = 0;

    for (i = 0; i < len; i++) {
        if (buf[i] == '$') {
            for (j = i; j < (len-1); j++) {
                if (buf[j] == '*') {
                    return &buf[i];
                }
            }
        }
    }
    return NULL;
} /* MatchCommandPatternNMEA0183 */

static command_parameters_t CommandPatternToParametersNMEA0183(const char *buf, int len)
{   uint32_t i = 0, iindex = 0, istring = 0;
    command_parameters_t cmd_parameters;
    
    memset(&cmd_parameters, 0, sizeof(command_parameters_t));
    for (i = 0; i < len; i++) {
        /* End of command */
        if (buf[i] == '*') {
            break;
        }
        /* Next parameter */
        if (buf[i] == ',') {
            istring = 0;
            iindex++;
            continue;
        }

        if (iindex < COMMAND_PARAMETERS_NR) {
            if (istring < COMMAND_PARAMETERS_LEN) {
                cmd_parameters.s[iindex][istring] = buf[i];
            }
        } else {
            break;
        }
        istring++;
    }
    cmd_parameters.nr = iindex;
    return cmd_parameters;
} /* CommandPatternToParametersNMEA0183 */

static void GetDegreesNMEA0183(const char *str, char *sresult)
{   char *point;

    if (sresult == NULL) {
        return;
    }
    
    strcpy(sresult, "");
    if (str == NULL) {
        return;
    }

    if ((point = strchr(str, '.')) != NULL) {
        if ((point - str) == 4) {
            memcpy(sresult, str, 2);
            sresult[2] = '\0';
        } else {
            if ((point - str) == 5) {
                memcpy(sresult, str, 3);
                sresult[3] = '\0';
            }
        }
    } 
} /* GetDegreesNMEA0183 */

static void GetMinutesNMEA0183(const char *str, char *sresult)
{   char *point;

    if (sresult == NULL) {
        return;
    }
    
    strcpy(sresult, "");
    if (str == NULL) {
        return;
    }

    if ((point = strchr(str, '.')) != NULL) {
        if ((point - str) >= 4 && (point - str) <= 5) {
            char *s = point-2;
            strcpy(sresult, s);
        }
    } 
} /* GetMinutesNMEA0183 */

static void GetHemispereNMEA0183(const char *str, char *sresult)
{
    if (sresult == NULL) {
        return;
    }
    
    strcpy(sresult, "x");
    if (str == NULL) {
        return;
    }

    if (strcmp(str, "") == 0) {
        strcpy(sresult, "");
    }
    sprintf(sresult, "%c", str[0]);
} /* GetHemispereNMEA0183 */


int GpsSe873_Init(gps_se873_handle_t *phandle, const char *sdevice)
{   
    struct termios tty;
    char serial[32];
    int delay = 0;

    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (gps_se873_handle_t) malloc(sizeof(struct _gps_se873_handle))) == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    gps_se873_handle_t handle = *phandle;
    handle->fd = -1;

    if ((handle->fd = open(sdevice, O_RDONLY|O_NONBLOCK)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        GpsSe873_Terminate(phandle);
        return -1;
    }

    memset (&tty, 0, sizeof tty);
    if (tcgetattr (handle->fd, &tty) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        GpsSe873_Terminate(phandle);
        return -1;
    }
    cfsetospeed (&tty, B9600);
    cfsetispeed (&tty, B9600);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8; // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    tty.c_iflag &= ~IGNBRK;                     // disable break processing
    tty.c_lflag = 0;                            // no signaling chars, no echo,
                                                // no canonical processing
    tty.c_oflag = 0;                            // no remapping, no delays
    tty.c_cc[VMIN]  = 0;                        // read doesn't block
    tty.c_cc[VTIME] = 5;                        // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY);     // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);            // ignore modem controls,
                                                // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);          // shut off parity
    tty.c_cflag |= 0;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (handle->fd, TCSANOW, &tty) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        GpsSe873_Terminate(phandle);
        return -1;
    }

    /* Check if data is received for 2 seconds. When this
     * is the case the module is available on the PCB. 
     */
    while (read(handle->fd, serial, sizeof(serial)) <= 0) {
        usleep(100000);
        if (++delay > 20) {
            printf("No data received from GPS module function %s line %d\n", __func__, __LINE__);
            GpsSe873_Terminate(phandle);
            return -1;            
        }
    }
    return 0;
} /* GpsSe873_Init */

int GpsSe873_GetLocation(gps_se873_handle_t handle, gps_se873_location_t *plocation)
{
    int nr_serial = 0;
    char serial[1024];
    const char *pattern = NULL;

    if ((nr_serial = read(handle->fd, serial, sizeof(serial))) >= 0) {
        pattern = serial;

serial[(nr_serial < sizeof(serial)) ? (nr_serial) : (nr_serial -1)] = '\0';
printf("\n\n\n\n%s\n\n\n\n", serial);

        while ((pattern = MatchCommandPatternNMEA0183(pattern, nr_serial-(pattern-serial))) != NULL) {
            command_parameters_t cp = CommandPatternToParametersNMEA0183(pattern, nr_serial-(pattern-serial));

            if (strstr(cp.s[0], "GGA") != NULL || strstr(cp.s[0], "GNS") != NULL) {
                GetDegreesNMEA0183(cp.s[2], plocation->latitude.degree);
                GetMinutesNMEA0183(cp.s[2], plocation->latitude.minutes);
                GetHemispereNMEA0183(cp.s[3], plocation->latitude.hemisphere);

                GetDegreesNMEA0183(cp.s[4], plocation->longitude.degree);
                GetMinutesNMEA0183(cp.s[4], plocation->longitude.minutes);
                GetHemispereNMEA0183(cp.s[5], plocation->longitude.hemisphere);
            }
            /* Search for next command pattern */
            pattern++;
        }
    } else {
        if (errno != EAGAIN) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return -1;
        }
    }
    return 0;
} /* GpsSe873_GetLocation */

void GpsSe873_Terminate(gps_se873_handle_t *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    if ((*phandle)->fd >=0) close((*phandle)->fd);

    free(*phandle);
    *phandle = NULL;
} /* GpsSe873_Terminate */
