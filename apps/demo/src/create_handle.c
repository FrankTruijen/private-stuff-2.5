/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "create_handle.h"

int CreateHandle_Init(void **phandle, uint32_t size)
{
    if (phandle == NULL) {
        return -1;
    }
    *phandle = malloc(size);
    memset(*phandle, 0, size);
    return 0;
} /* CreateHandle_Init */

void CreateHandle_Terminate(void **phandle)
{
    if (phandle == NULL) {
        return;
    }    
    free(*phandle);
    *phandle = NULL;
} /* CreateHandle_Terminate */
