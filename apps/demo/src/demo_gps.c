/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "demo_gps.h"
#include "create_handle.h"
#include "satelite_background.h"

struct _demo_gps_handle {
    keyboard_handle_t    keyboard;
    display_handle_t     display;
    gps_se873_handle_t   gps;

    char                 slong[32];
    char                 slat[32];
};

int is_integer(char *s)
{
    int i = 0;
    int len = strlen(s);
    
    if(len == 0) {
        return 0;
    }

    for (i = 0; i < len; i++) {
        char c = s[i];
        if ((c < '0' || c > '9') && c != '.') {
            return 0;
        }
    }
    return 1;
}

int DemoGps_Init(
    demo_gps_handle_t       *phandle,
    keyboard_handle_t       keyboard,
    display_handle_t        display,
    gps_se873_handle_t      gps)
{
    if (keyboard == NULL || display == NULL || gps == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (CreateHandle_Init((void **)phandle, sizeof(struct _demo_gps_handle)) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    (*phandle)->keyboard = keyboard;
    (*phandle)->display = display;
    (*phandle)->gps = gps;
    
    strcpy((*phandle)->slong, "no signal");
    strcpy((*phandle)->slat, "no signal");
    return 0;
} /* DemoGps_Init */

void DemoGps_Terminate(
    demo_gps_handle_t       *phandle)
{
    CreateHandle_Terminate((void **)phandle);
} /* DemoGps_Terminate */

execute_return_t DemoGps_Execute(
    demo_gps_handle_t       handle)
{
    uint32_t *bgra = NULL;
    gps_se873_location_t loc;
    key_event_t key_event = Keyboard_GetEvent(handle->keyboard);

    if (key_event == KEY_EVENT_ERROR) {
        printf("%s[%d]\n", __func__, __LINE__);
        return EXECUTE_RETURN_ERROR;
    }
    if (key_event == KEY_EVENT_BACK) {
        return EXECUTE_RETURN_FINISHED;
    }

    memset(&loc, 0, sizeof(loc));
    if (GpsSe873_GetLocation(handle->gps, &loc) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return EXECUTE_RETURN_ERROR;
    }

    if (is_integer(loc.longitude.degree) && is_integer(loc.latitude.degree)) {
        snprintf(handle->slong, sizeof(handle->slong)-1, "%sD%sM %s", loc.longitude.degree, loc.longitude.minutes, loc.longitude.hemisphere);
        snprintf(handle->slat,  sizeof(handle->slat)-1,  " %sD%sM %s", loc.latitude.degree,  loc.latitude.minutes,  loc.latitude.hemisphere);
    }

    ClearText(handle->display);
    InsertText(handle->display, "Long",         0, 240, 0, 0, 0, FONT_SIZE_16);
    InsertText(handle->display, handle->slong,  0, 256, 0, 0, 0, FONT_SIZE_16);
    InsertText(handle->display, "lat",          0, 280, 0, 0, 0, FONT_SIZE_16);
    InsertText(handle->display, handle->slat,   0, 296, 0, 0, 0, FONT_SIZE_16);

    /* Show satelite icon on display */
    if (Display_GetImageBufferBGRA(handle->display, &bgra) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (WIDTH_satelite_background  != Display_GetResolutionX(handle->display) ||
        HEIGHT_satelite_background != Display_GetResolutionY(handle->display)) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    YtoBGRA(data_satelite_background, WIDTH_satelite_background, HEIGHT_satelite_background, bgra);
    
    if (Display_PutImageBufferBGRA(handle->display) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    return EXECUTE_RETURN_OK;
} /* DemoGps_Execute */
