/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <ftrScanAPI.h>

#include "finger_print_scanner.h"

struct _finger_print_scanner_handle {
    void *finger_print_scanner;
    FTRSCAN_IMAGE_SIZE image_size;
};

int FingerPrintScanner_Init(finger_print_scanner_handle_t *phandle)
{
    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (finger_print_scanner_handle_t) malloc(sizeof(struct _finger_print_scanner_handle))) == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;    
    }
    finger_print_scanner_handle_t handle = *phandle;

    if ((handle->finger_print_scanner = ftrScanOpenDevice()) == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        FingerPrintScanner_Terminate(phandle);
        return -1;
    }

	if (ftrScanGetImageSize(handle->finger_print_scanner, &handle->image_size) == 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        FingerPrintScanner_Terminate(phandle);
		return -1;
	}
    return 0;
} /* FingerPrintScanner_Init */

int FingerPrintScanner_IsFingerPresent(finger_print_scanner_handle_t handle)
{
    if (ftrScanIsFingerPresent(handle->finger_print_scanner, NULL)) {
        return 1;
    }
    return 0;
} /* FingerPrintScanner_IsFingerPresent */

int FingerPrintScanner_GetImageY(finger_print_scanner_handle_t handle, uint8_t *y)
{
    if (ftrScanGetFrame(handle->finger_print_scanner, y, NULL) == 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
		return -1;
    }
    return 0;
} /* FingerPrintScanner_GetImageY */

uint32_t FingerPrintScanner_GetResolutionX(finger_print_scanner_handle_t handle)
{
    return handle->image_size.nWidth;
} /* FingerPrintScanner_GetResolutionX */

uint32_t FingerPrintScanner_GetResolutionY(finger_print_scanner_handle_t handle)
{
    return handle->image_size.nHeight;
} /* FingerPrintScanner_GetResolutionY */

void FingerPrintScanner_Terminate(finger_print_scanner_handle_t *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    ftrScanCloseDevice((*phandle)->finger_print_scanner);

    free(*phandle);
    *phandle = NULL;
} /* FingerPrintScanner_Terminate */
