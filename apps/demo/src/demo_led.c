/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#define _POSIX_C_SOURCE 200809L

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "demo_led.h"
#include "create_handle.h"
#include "led_background.h"

uint64_t GetCurrentTimeMs() 
{
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);
    return ((uint64_t)spec.tv_sec * 1000 + (uint64_t)(spec.tv_nsec / 1000000));
} /* GetCurrentTime */

typedef enum {
    SM_OFF = 0,
    SM_STATUS_LED_GREEN,
    SM_STATUS_LED_RED,
       
    SM_DISPLAY_OK,
    SM_DISPLAY_ACTIVE,
    SM_DISPLAY_HOUR_GLASS,
       
    SM_FOIL_LEFT_THUMB,
    SM_FOIL_LEFT_INDEX,
    SM_FOIL_LEFT_MIDDLE_FINGER,
    SM_FOIL_LEFT_RING_FINGER,
    SM_FOIL_LEFT_PINKY,
    SM_FOIL_RIGHT_THUMB,
    SM_FOIL_RIGHT_INDEX,
    SM_FOIL_RIGHT_MIDDLE_FINGER,
    SM_FOIL_RIGHT_RING_FINGER,
    SM_FOIL_RIGHT_PINKY,
    SM_FOIL_ERROR,
    SM_FOIL_OK,
    SM_FOIL_ARROW,
       
    SM_LASER,
} demo_led_state_machine_t;

struct _demo_led_handle {
    keyboard_handle_t           keyboard;
    display_handle_t            display;
    led_rgb_handle_t            led_status_rb;
    led_rgb_handle_t            led_status_rt;
    led_rgb_handle_t            led_status_lb;
    led_rgb_handle_t            led_status_lt;
    led_handle_t                led_laser_0;
    led_handle_t                led_laser_1;
    led_handle_t                led_display_ok;
    led_handle_t                led_display_active;
    led_handle_t                led_display_hourglass;
    led_handle_t                led_foil_left_thumb;
    led_handle_t                led_foil_left_index_finger;
    led_handle_t                led_foil_left_middle_finger;
    led_handle_t                led_foil_left_ring_finger;
    led_handle_t                led_foil_left_pinky;
    led_handle_t                led_foil_right_thumb;
    led_handle_t                led_foil_right_index_finger;
    led_handle_t                led_foil_right_middle_finger;
    led_handle_t                led_foil_right_ring_finger;
    led_handle_t                led_foil_right_pinky;
    led_handle_t                led_foil_arrow;
    led_handle_t                led_foil_error;
    led_handle_t                led_foil_ok;
    
    uint64_t                    state_time;
    demo_led_state_machine_t    state;
};

int DemoLed_Init(
    demo_led_handle_t   *phandle,
    keyboard_handle_t   keyboard,
    display_handle_t    display,
    led_rgb_handle_t    led_status_rb,
    led_rgb_handle_t    led_status_rt,
    led_rgb_handle_t    led_status_lb,
    led_rgb_handle_t    led_status_lt,
    led_handle_t        led_laser_0,
    led_handle_t        led_laser_1,
    led_handle_t        led_display_ok,
    led_handle_t        led_display_active,
    led_handle_t        led_display_hourglass,
    led_handle_t        led_foil_left_thumb,
    led_handle_t        led_foil_left_index_finger,
    led_handle_t        led_foil_left_middle_finger,
    led_handle_t        led_foil_left_ring_finger,
    led_handle_t        led_foil_left_pinky,
    led_handle_t        led_foil_right_thumb,
    led_handle_t        led_foil_right_index_finger,
    led_handle_t        led_foil_right_middle_finger,
    led_handle_t        led_foil_right_ring_finger,
    led_handle_t        led_foil_right_pinky,
    led_handle_t        led_foil_arrow,
    led_handle_t        led_foil_error,
    led_handle_t        led_foil_ok) {
    
    if (phandle == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (led_status_rb == NULL || led_status_rt == NULL || led_status_lb == NULL || led_status_lt == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
        
    if (led_laser_0 == NULL || led_laser_1 == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (led_display_ok == NULL || led_display_active == NULL || led_display_hourglass == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (led_foil_left_thumb         == NULL   || led_foil_left_index_finger     == NULL   || led_foil_left_middle_finger == NULL  ||
        led_foil_left_ring_finger   == NULL   || led_foil_left_pinky            == NULL   || led_foil_right_thumb        == NULL  ||
        led_foil_right_index_finger == NULL   || led_foil_right_middle_finger   == NULL   || led_foil_right_ring_finger  == NULL  ||
        led_foil_right_pinky        == NULL   || led_foil_arrow                 == NULL   || led_foil_error              == NULL  ||
        led_foil_ok                 == NULL) {

        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (keyboard == NULL || display == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (CreateHandle_Init((void **)phandle, sizeof(struct _demo_led_handle)) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    (*phandle)->keyboard                     = keyboard;
    (*phandle)->display                      = display;
    (*phandle)->led_status_rb                = led_status_rb;
    (*phandle)->led_status_rt                = led_status_rt;
    (*phandle)->led_status_lb                = led_status_lb;
    (*phandle)->led_status_lt                = led_status_lt;
    (*phandle)->led_laser_0                  = led_laser_0;
    (*phandle)->led_laser_1                  = led_laser_1;
    (*phandle)->led_display_ok               = led_display_ok;
    (*phandle)->led_display_active           = led_display_active;
    (*phandle)->led_display_hourglass        = led_display_hourglass;
    (*phandle)->led_foil_left_thumb          = led_foil_left_thumb;
    (*phandle)->led_foil_left_index_finger   = led_foil_left_index_finger;
    (*phandle)->led_foil_left_middle_finger  = led_foil_left_middle_finger;
    (*phandle)->led_foil_left_ring_finger    = led_foil_left_ring_finger;
    (*phandle)->led_foil_left_pinky          = led_foil_left_pinky;
    (*phandle)->led_foil_right_thumb         = led_foil_right_thumb;
    (*phandle)->led_foil_right_index_finger  = led_foil_right_index_finger;
    (*phandle)->led_foil_right_middle_finger = led_foil_right_middle_finger;
    (*phandle)->led_foil_right_ring_finger   = led_foil_right_ring_finger;
    (*phandle)->led_foil_right_pinky         = led_foil_right_pinky;
    (*phandle)->led_foil_arrow               = led_foil_arrow;
    (*phandle)->led_foil_error               = led_foil_error;
    (*phandle)->led_foil_ok                  = led_foil_ok;
    return 0;
} /* DemoLed_Init */

void DemoLed_Terminate(
    demo_led_handle_t   *phandle)
{
    CreateHandle_Terminate((void **)phandle);
} /* DemoLed_Terminate */

#define INC_TIME    500

execute_return_t DemoLed_Execute(
    demo_led_handle_t   handle)
{
    uint32_t *bgra = NULL;
    key_event_t key_event = Keyboard_GetEvent(handle->keyboard);

    if (key_event == KEY_EVENT_ERROR) {
        handle->state = SM_OFF;
        return EXECUTE_RETURN_ERROR;
    }
    if (key_event == KEY_EVENT_BACK) {
        handle->state = SM_OFF;
        LedRgb_Value(handle->led_status_rb, 0, 0, 0);
        LedRgb_Value(handle->led_status_rt, 0, 0, 0);
        LedRgb_Value(handle->led_status_lb, 0, 0, 0);
        LedRgb_Value(handle->led_status_lt, 0, 0, 0);
        Led_Value(handle->led_display_ok, 0);
        Led_Value(handle->led_display_active, 0);
        Led_Value(handle->led_display_hourglass, 0);
        Led_Value(handle->led_foil_left_thumb, 0);
        Led_Value(handle->led_foil_left_index_finger, 0);
        Led_Value(handle->led_foil_left_middle_finger, 0);
        Led_Value(handle->led_foil_left_ring_finger, 0);
        Led_Value(handle->led_foil_left_pinky, 0);
        Led_Value(handle->led_foil_right_thumb, 0);
        Led_Value(handle->led_foil_right_index_finger, 0);
        Led_Value(handle->led_foil_right_middle_finger, 0);  
        Led_Value(handle->led_foil_right_ring_finger, 0);
        Led_Value(handle->led_foil_right_pinky, 0);
        Led_Value(handle->led_foil_error, 0);
        Led_Value(handle->led_foil_ok, 0);
        Led_Value(handle->led_foil_arrow, 0);
        Led_Value(handle->led_laser_0, 0);
        Led_Value(handle->led_laser_1, 0);
        return EXECUTE_RETURN_FINISHED;
    }

    switch(handle->state) {
    case SM_OFF:
        /* Show LED icon on display */
        ClearText(handle->display);
        if (Display_GetImageBufferBGRA(handle->display, &bgra) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return -1;
        }

        if (WIDTH_LED_BACKGROUND  != Display_GetResolutionX(handle->display) ||
            HEIGHT_LED_BACKGROUND != Display_GetResolutionY(handle->display)) {
            printf("%s[%d]\n", __func__, __LINE__);
            return -1;
        }

        YtoBGRA(data_LED_BACKGROUND, WIDTH_LED_BACKGROUND, HEIGHT_LED_BACKGROUND, bgra);
        
        if (Display_PutImageBufferBGRA(handle->display) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return -1;
        }
        handle->state_time = GetCurrentTimeMs();
        handle->state = SM_STATUS_LED_GREEN;

        LedRgb_Value(handle->led_status_rb, 0, 255, 0);
        LedRgb_Value(handle->led_status_rt, 0, 255, 0);
        LedRgb_Value(handle->led_status_lb, 0, 255, 0);
        LedRgb_Value(handle->led_status_lt, 0, 255, 0);
        break;
    case SM_STATUS_LED_GREEN:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_STATUS_LED_RED;

            LedRgb_Value(handle->led_status_rb, 255, 0, 0);
            LedRgb_Value(handle->led_status_rt, 255, 0, 0);
            LedRgb_Value(handle->led_status_lb, 255, 0, 0);
            LedRgb_Value(handle->led_status_lt, 255, 0, 0);
        }
        break;
    case SM_STATUS_LED_RED:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_DISPLAY_OK;

            LedRgb_Value(handle->led_status_rb, 0, 0, 0);
            LedRgb_Value(handle->led_status_rt, 0, 0, 0);
            LedRgb_Value(handle->led_status_lb, 0, 0, 0);
            LedRgb_Value(handle->led_status_lt, 0, 0, 0);
            
            Led_Value(handle->led_display_ok, 255);
        }
        break;
    case SM_DISPLAY_OK:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_DISPLAY_ACTIVE;

            Led_Value(handle->led_display_ok, 0);
            Led_Value(handle->led_display_active, 255);
        }
        break;
    case SM_DISPLAY_ACTIVE:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_DISPLAY_HOUR_GLASS;

            Led_Value(handle->led_display_active, 0);
            Led_Value(handle->led_display_hourglass, 255);
        }
        break;
    case SM_DISPLAY_HOUR_GLASS:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_LEFT_THUMB;

            Led_Value(handle->led_display_hourglass, 0);
            Led_Value(handle->led_foil_left_thumb, 255);
        }
        break;
    case SM_FOIL_LEFT_THUMB:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_LEFT_INDEX;

            Led_Value(handle->led_foil_left_thumb, 0);
            Led_Value(handle->led_foil_left_index_finger, 255);
        }
        break;
    case SM_FOIL_LEFT_INDEX:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_LEFT_MIDDLE_FINGER;

            Led_Value(handle->led_foil_left_index_finger, 0);
            Led_Value(handle->led_foil_left_middle_finger, 255);
        }
        break;
    case SM_FOIL_LEFT_MIDDLE_FINGER:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_LEFT_RING_FINGER;

            Led_Value(handle->led_foil_left_middle_finger, 0);
            Led_Value(handle->led_foil_left_ring_finger, 255);
        }
        break;
    case SM_FOIL_LEFT_RING_FINGER:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_LEFT_PINKY;

            Led_Value(handle->led_foil_left_ring_finger, 0);
            Led_Value(handle->led_foil_left_pinky, 255);
        }
        break;
    case SM_FOIL_LEFT_PINKY:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_RIGHT_THUMB;

            Led_Value(handle->led_foil_left_pinky, 0);
            Led_Value(handle->led_foil_right_thumb, 255);
        }
        break;
    case SM_FOIL_RIGHT_THUMB:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_RIGHT_INDEX;

            Led_Value(handle->led_foil_right_thumb, 0);
            Led_Value(handle->led_foil_right_index_finger, 255);
        }
        break;
    case SM_FOIL_RIGHT_INDEX:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_RIGHT_MIDDLE_FINGER;

            Led_Value(handle->led_foil_right_index_finger, 0);
            Led_Value(handle->led_foil_right_middle_finger, 255);
        }
        break;
    case SM_FOIL_RIGHT_MIDDLE_FINGER:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_RIGHT_RING_FINGER;

            Led_Value(handle->led_foil_right_middle_finger, 0);
            Led_Value(handle->led_foil_right_ring_finger, 255);
        }
        break;
    case SM_FOIL_RIGHT_RING_FINGER:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_RIGHT_PINKY;

            Led_Value(handle->led_foil_right_ring_finger, 0);
            Led_Value(handle->led_foil_right_pinky, 255);
        }
        break;
    case SM_FOIL_RIGHT_PINKY:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_ERROR;

            Led_Value(handle->led_foil_right_pinky, 0);
            Led_Value(handle->led_foil_error, 255);
        }
        break;
    case SM_FOIL_ERROR:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_OK;

            Led_Value(handle->led_foil_error, 0);
            Led_Value(handle->led_foil_ok, 255);
        }
        break;
    case SM_FOIL_OK:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_FOIL_ARROW;

            Led_Value(handle->led_foil_ok, 0);
            Led_Value(handle->led_foil_arrow, 255);
        }
        break;
    case SM_FOIL_ARROW:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_LASER;

            Led_Value(handle->led_foil_arrow, 0);
            Led_Value(handle->led_laser_0, 255);
            Led_Value(handle->led_laser_1, 255);
        }
        break;
    case SM_LASER:
        if (GetCurrentTimeMs() > (handle->state_time + INC_TIME)) {
            handle->state_time = GetCurrentTimeMs();
            handle->state = SM_STATUS_LED_GREEN;

            Led_Value(handle->led_laser_0, 0);
            Led_Value(handle->led_laser_1, 0);
            LedRgb_Value(handle->led_status_rb, 0, 255, 0);
            LedRgb_Value(handle->led_status_rt, 0, 255, 0);
            LedRgb_Value(handle->led_status_lb, 0, 255, 0);
            LedRgb_Value(handle->led_status_lt, 0, 255, 0);
        }
        break;
    }
    return EXECUTE_RETURN_OK;
} /* DemoLed_Execute */