/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <errno.h>

#include "power.h"

#define GPIO_BUTTON             14
#define GPIO_POWER              49
#define S_GPIO_BUTTON           "14"
#define S_GPIO_POWER            "49"

struct _power_handle {
    char sprefix[128];
};

static int CommandWrite(const char *sprefix, const char *sfile, uint32_t val)
{
    int fd = 0;
    char sval[16], spath[256];

    memset(sval, 0, sizeof(sval));
    memset(spath, 0, sizeof(spath));

    snprintf(spath, sizeof(spath), "%s%s", sprefix, sfile);
    if ((fd = open(spath, O_WRONLY)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    snprintf(sval, sizeof(sval), "%u", val);
    if (write(fd, sval, strlen(sval)) != strlen(sval)) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        close(fd);
        return -1;
    }
    usleep(999999);
    close(fd);
    return 0;
} /* CommandWrite */

static int CommandWriteS(const char *sprefix, const char *sfile, char *sval)
{
    int fd = 0;
    char spath[256];

    memset(spath, 0, sizeof(spath));

    snprintf(spath, sizeof(spath), "%s%s", sprefix, sfile);
    if ((fd = open(spath, O_WRONLY)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if (write(fd, sval, strlen(sval)) != strlen(sval)) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        close(fd);
        return -1;
    }
    usleep(999999);
    close(fd);
    return 0;
} /* CommandWriteS */

static int CommandRead(const char *sprefix, const char *sfile, uint32_t *val)
{
    int fd = 0;
    char sval[2], spath[256];

    memset(sval, 0, sizeof(sval));
    memset(spath, 0, sizeof(spath));

    snprintf(spath, sizeof(spath), "%s%s", sprefix, sfile);
    if ((fd = open(spath, O_RDONLY)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    if (read(fd, sval, 1) <= 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        close(fd);
        return -1;
    }
    sscanf(sval, "%u", val);
    close(fd);
    return 0;
} /* CommandRead */

int Power_Init(power_handle_t *phandle, const char *sprefix)
{   
    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (power_handle_t) malloc(sizeof(struct _power_handle))) == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    power_handle_t handle = *phandle;

    strcpy(handle->sprefix, sprefix);

    /* Enable power button gpio (input) */
    if (CommandWrite(handle->sprefix, "export", GPIO_BUTTON) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Power_Terminate(phandle, 0);
        return -1;
    }

    /* Enable power enable gpio (output) */
    if (CommandWrite(handle->sprefix, "export", GPIO_POWER) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Power_Terminate(phandle, 0);
        return -1;
    }
    return 0;
} /* Power_Init */

int Power_ButtonPushed(power_handle_t handle)
{
    uint32_t v;
    if (CommandRead(handle->sprefix, "gpio" S_GPIO_BUTTON "/value", &v) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    return v == 0 ? 1 : 0;
} /* Power_ButtonPushed */

void Power_Terminate(power_handle_t *phandle, int switch_off)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }

    if (switch_off) {
        /* Device is switched instantly */
        if (CommandWriteS((*phandle)->sprefix, "gpio" S_GPIO_POWER "/direction", "out") != 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return;
        }
    }
    if (CommandWrite((*phandle)->sprefix, "unexport", 0) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    free(*phandle);
    *phandle = NULL;
} /* Power_Terminate */
