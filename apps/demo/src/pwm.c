/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "pwm.h"

struct _pwm_handle {
    char sprefix[128];

    uint32_t period;
};

static int Command(const char *sprefix, const char *sfile, uint32_t val)
{
    int fd = 0;
    char sval[16], spath[256];

    memset(sval, 0, sizeof(sval));
    memset(spath, 0, sizeof(spath));

    snprintf(spath, sizeof(spath), "%s%s", sprefix, sfile);
    if ((fd = open(spath, O_WRONLY)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    snprintf(sval, sizeof(sval), "%u", val);
    if (write(fd, sval, strlen(sval)) != strlen(sval)) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        close(fd);
        return -1;
    }
    close(fd);
    return 0;
} /* Command */

int Pwm_Init(pwm_handle_t *phandle, uint32_t period, const char *sprefix)
{   
    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (pwm_handle_t) malloc(sizeof(struct _pwm_handle))) == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    pwm_handle_t handle = *phandle;

    handle->period = period;
    strcpy(handle->sprefix, sprefix);

    if (Command(handle->sprefix, "export", 0) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Pwm_Terminate(phandle);
        return -1;
    }

    if (Command(handle->sprefix, "pwm0/period", handle->period) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Pwm_Terminate(phandle);
        return -1;
    }
    return 0;
} /* Pwm_Init */

int Pwm_Value(pwm_handle_t handle, uint32_t duty_cycle)
{
    uint32_t pwm_enable = 0;

    /* Duty cycle has range from 0..1000 */
    if (duty_cycle > 1000) duty_cycle = 1000;

    if (duty_cycle) {
        uint32_t dc = (uint32_t)(((uint64_t)handle->period * (uint64_t)duty_cycle) / (uint64_t)1000);

        if (Command(handle->sprefix, "pwm0/duty_cycle", dc) != 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return -1;
        }
        pwm_enable = 1;
    } else {
        pwm_enable = 0;
    }

    if (Command(handle->sprefix, "pwm0/enable", pwm_enable) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    return 0;
} /* Pwm_Value */

void Pwm_Terminate(pwm_handle_t *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    if (Command((*phandle)->sprefix, "unexport", 0) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    free(*phandle);
    *phandle = NULL;
} /* Pwm_Terminate */
