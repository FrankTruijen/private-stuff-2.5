/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "zbar.h"
#include "demo_barcode.h"
#include "create_handle.h"

#define CLIP(X)             ( (X) > 255 ? 255 : (X) < 0 ? 0 : (X))

#define C(Y)                ( (Y) - 16  )
#define D(U)                ( (U) - 128 )
#define E(V)                ( (V) - 128 )

#define YUV2R(Y, U, V)      CLIP(( 298 * C(Y)              + 409 * E(V) + 128) >> 8)
#define YUV2G(Y, U, V)      CLIP(( 298 * C(Y) - 100 * D(U) - 208 * E(V) + 128) >> 8)
#define YUV2B(Y, U, V)      CLIP(( 298 * C(Y) + 516 * D(U)              + 128) >> 8)

#define MAX_CAMERA_WIDTH    2592
#define MAX_CAMERA_HEIGHT   1944

#define UNUSED(x) (void)(x)

/* There are devices who have problems with switching 
 * to high resolution mode. This macro can 
 * enable/disable high resolution mode.
 */
#define noHIGH_RES_MODE

typedef enum {
    SM_OFF = 0,
    SM_CAPTURE,
    SM_BARCODE_DETECTED,
} demo_camera_state_machine_t;

struct _demo_barcode_handle {
    keyboard_handle_t               keyboard;
    display_handle_t                display;
    led_handle_t                    laser_0;
    led_handle_t                    laser_1;
    pwm_handle_t                    flashlight;
    camera_handle_t                 camera;

    /* Video output signals */
    uint8_t                         *y_decode;

    /* Camera */
    demo_camera_state_machine_t     state;

    /* Number of frames to have */
    uint32_t                        nr_frames_to_try;

    /* Used to add a string */
    char                            sbarcode_type[128];
    char                            sbarcode_nr[128];
};

/* Rotate 90, downscale factor 2 and convert yuyv to bgra (only gray scale) */
static void rotate_and_scale_yuyv_to_bgra(uint32_t *yuyv, uint32_t inwidth, uint32_t inheight, uint32_t *bgra, uint32_t outwidth, uint32_t outheight)
{
    int32_t px = 0, py = 0;

    //UNUSED(outheight);

    for (py = 0; py < outwidth; py++) {
        for (px = 0; px < outheight; px++) {
            uint32_t rotatex = outwidth-py-1;
            uint32_t rotatey = px;

            /* Skip line (multiply by 2) and every yuyv block 
             * contains 2 y values (divide by 2).
             */
            uint8_t y = (yuyv[2*py*(inwidth/2)+px] >>  0) & 0xFF;
            //uint8_t u = (yuyv[2*py*(inwidth/2)+px] >>  8) & 0xFF;
            //uint8_t v = (yuyv[2*py*(inwidth/2)+px] >> 16) & 0xFF;

            uint8_t b = y;//YUV2B(y, u, v);
            uint8_t g = y;//YUV2G(y, u, v);
            uint8_t r = y;//YUV2R(y, u, v);

            bgra[rotatey*outwidth+rotatex] = (b << 0) + (g << 8) + (r << 16);
        }
    }
} /* rotate_yuyv_to_bgra */

static void rotate_yuyv_to_y(uint32_t *yuyv, uint32_t width, uint32_t height, uint8_t *y)
{
    int32_t px = 0, py = 0;

    uint32_t inwidth   = width;
    uint32_t inheight  = height;
    uint32_t outwidth  = height;
    for (py = 0; py < inheight; py++) {
        for (px = 0; px < inwidth; px+=2) {
            uint32_t rotatex = outwidth-py-1;
            uint32_t rotatey = px;

            y[(rotatey+0)*outwidth+rotatex] = (yuyv[py*(inwidth/2)+(px/2)] >>  0) & 0xFF;
            y[(rotatey+1)*outwidth+rotatex] = (yuyv[py*(inwidth/2)+(px/2)] >> 16) & 0xFF;
        }
    }
} /* rotate_yuyv_to_y */

int extract_bar(uint8_t *y, uint32_t width, uint32_t height, char *sbarcode_type, char *sbarcode_nr)
{
    /* create a reader */
    zbar_image_scanner_t *scanner = zbar_image_scanner_create();

    /* configure the reader */
    zbar_image_scanner_set_config(scanner, 0, ZBAR_CFG_ENABLE, 1);

    /* wrap image data */
    zbar_image_t *image = zbar_image_create();
    zbar_image_set_format(image, *(int*)"Y800");
    zbar_image_set_size(image, width, height);
    zbar_image_set_data(image, (void *)y, width * height, NULL);

    /* scan the image for barcodes */
    if (zbar_scan_image(scanner, image) > 0) {
        /* extract results */
        const zbar_symbol_t *symbol = zbar_image_first_symbol(image);
        for(; symbol; symbol = zbar_symbol_next(symbol)) {
            /* do something useful with results */
            zbar_symbol_type_t typ = zbar_symbol_get_type(symbol);
            const char *data = zbar_symbol_get_data(symbol);
            
            sprintf(sbarcode_type, "%s", zbar_get_symbol_name(typ));
            sprintf(sbarcode_nr, "%s", data);
        }    
    } else {
        return -1;
    }

    /* clean up */
    zbar_image_destroy(image);
    zbar_image_scanner_destroy(scanner);
    return 0;
} /* extract_bar */

int DemoBarcode_Init(
    demo_barcode_handle_t   *phandle,
    keyboard_handle_t       keyboard,
    display_handle_t        display,
    led_handle_t            laser_0,
    led_handle_t            laser_1,
    pwm_handle_t            flashlight,
    camera_handle_t         camera)
{
    demo_barcode_handle_t handle = NULL;

    if (keyboard == NULL || display == NULL || laser_0 == NULL || laser_1 == NULL || camera == NULL) {
        return -1;
    }

    if (CreateHandle_Init((void **)phandle, sizeof(struct _demo_barcode_handle)) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    handle = *phandle;

    handle->keyboard = keyboard;
    handle->display = display;
    handle->laser_0 = laser_0;
    handle->laser_1 = laser_1;
    handle->flashlight = flashlight;
    handle->camera = camera;

    /* Allocate display (MODE_QSXGA_2592_1944) */
    if ((handle->y_decode = malloc(MAX_CAMERA_WIDTH * MAX_CAMERA_HEIGHT)) == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    memset(handle->y_decode, 128, MAX_CAMERA_WIDTH * MAX_CAMERA_HEIGHT);
    return 0;
} /* DemoBarcode_Init */

void DemoBarcode_Terminate(
    demo_barcode_handle_t   *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return;
    }
    demo_barcode_handle_t handle = *phandle;

    free(handle->y_decode);
    CreateHandle_Terminate((void **)phandle);
} /* DemoBarcode_Terminate */

execute_return_t DemoBarcode_Execute(
    demo_barcode_handle_t   handle)
{
    uint32_t *yuyv = NULL, *bgra = NULL;
    uint32_t high_res_x = 0, high_res_y = 0, low_res_x = 0, low_res_y = 0;

    key_event_t key_event = Keyboard_GetEvent(handle->keyboard);
    if (key_event == KEY_EVENT_ERROR) {
        return EXECUTE_RETURN_ERROR;
    }
    if (key_event == KEY_EVENT_BACK) {
        if (Camera_Stop(handle->camera) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return EXECUTE_RETURN_ERROR;
        }

        memset(handle->sbarcode_type, 0, sizeof(handle->sbarcode_type));
        memset(handle->sbarcode_nr, 0, sizeof(handle->sbarcode_nr));
        Led_Value(handle->laser_0, 0);
        Led_Value(handle->laser_1, 0);
        Pwm_Value(handle->flashlight, 0);
        handle->state = SM_OFF;
        return EXECUTE_RETURN_FINISHED;
    }

    switch (handle->state) {
    case SM_OFF:
        Led_Value(handle->laser_0, 255);
        Led_Value(handle->laser_1, 255);

        if (Camera_Start(handle->camera, MODE_VGA_640_480) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return EXECUTE_RETURN_ERROR;
        }

        /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
        /* ONLY USE 10% maximum (otherwise hardware damage) */
        /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
        Pwm_Value(handle->flashlight, 0/*100*/);
        ClearText(handle->display);
        
        handle->state = SM_CAPTURE;
        break;
    case SM_CAPTURE:
        if (Camera_GetImageBufferYUYV(handle->camera, &yuyv) == CAMERA_ERROR) {
            printf("%s[%d]\n", __func__, __LINE__);
            return EXECUTE_RETURN_ERROR;
        }

        if (Display_GetImageBufferBGRA(handle->display, &bgra) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return EXECUTE_RETURN_ERROR;
        }

        high_res_x = Camera_GetResolutionX(handle->camera);
        high_res_y = Camera_GetResolutionY(handle->camera);
        low_res_x = Display_GetResolutionX(handle->display);
        low_res_y = Display_GetResolutionY(handle->display);

        if ((high_res_y != 2*low_res_x) || (high_res_x != 2*low_res_y)) {
            printf("%s[%d]\n", __func__, __LINE__);
            return EXECUTE_RETURN_ERROR;
        }

        rotate_and_scale_yuyv_to_bgra(yuyv, high_res_x, high_res_y, bgra, low_res_x, low_res_y);

        if (Camera_PutImageBufferYUYV(handle->camera) == CAMERA_ERROR) {
            printf("%s[%d]\n", __func__, __LINE__);
            return EXECUTE_RETURN_ERROR;
        }

        if (Display_PutImageBufferBGRA(handle->display) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return EXECUTE_RETURN_ERROR;
        }

        if (key_event == KEY_EVENT_GREEN_CHECK) {
#ifdef HIGH_RES_MODE            
            handle->nr_frames_to_try = 1;
#else
            handle->nr_frames_to_try = 10;
#endif /* HIGH_RES_MODE */
        }


        if (handle->nr_frames_to_try > 0) {
            char sfile_name[512];
            memset(sfile_name, 0, sizeof(sfile_name));

#ifdef HIGH_RES_MODE
            /* Switch of camera to restart it again with a higher 
             * resolution which is needed for barcode detection.
             */
            if (Camera_Stop(handle->camera) != 0 || 
                Camera_Start(handle->camera, MODE_QSXGA_2592_1944) != 0) {
                printf("%s[%d]\n", __func__, __LINE__);
                return EXECUTE_RETURN_ERROR;
            }

            high_res_x = Camera_GetResolutionX(handle->camera);
            high_res_y = Camera_GetResolutionY(handle->camera);

            if (Camera_GetImageBufferYUYV(handle->camera, &yuyv) == CAMERA_ERROR) {
                printf("%s[%d]\n", __func__, __LINE__);
                return EXECUTE_RETURN_ERROR;
            }
#endif /* HIGH_RES_MODE */

            rotate_yuyv_to_y(yuyv, high_res_x, high_res_y, handle->y_decode);

#ifdef HIGH_RES_MODE
            if (Camera_PutImageBufferYUYV(handle->camera) == CAMERA_ERROR) {
                printf("%s[%d]\n", __func__, __LINE__);
                return EXECUTE_RETURN_ERROR;
            }

            if (Camera_Stop(handle->camera) != 0 || 
                Camera_Start(handle->camera, MODE_VGA_640_480) != 0) {
                printf("%s[%d]\n", __func__, __LINE__);
                return EXECUTE_RETURN_ERROR;
            }
#endif /* HIGH_RES_MODE */

            /* Toggle width and height */
            if (extract_bar(handle->y_decode, high_res_y, high_res_x, handle->sbarcode_type, handle->sbarcode_nr) == 0) {
                handle->nr_frames_to_try = 0;

                ClearText(handle->display);
                InsertText(handle->display, handle->sbarcode_type, 0, 280, 0, 155, 0, FONT_SIZE_16);
                InsertText(handle->display, handle->sbarcode_nr,   0, 296, 0, 155, 0, FONT_SIZE_16);

                snprintf(sfile_name, sizeof(sfile_name)-1, "/media/sd0/barcode_correct_%s.bmp", GetDateTime());
                R8G8B8toBMP(high_res_y, high_res_x, handle->y_decode, handle->y_decode, handle->y_decode, sfile_name);
            } else {
                handle->nr_frames_to_try--;
                if (handle->nr_frames_to_try == 0) {
                    ClearText(handle->display);
                    InsertText(handle->display, "   No Barcode   ",   0, 296, 0, 155, 0, FONT_SIZE_16);
                
                    snprintf(sfile_name, sizeof(sfile_name)-1, "/media/sd0/barcode_wrong_%s.bmp", GetDateTime());
                    R8G8B8toBMP(high_res_y, high_res_x, handle->y_decode, handle->y_decode, handle->y_decode, sfile_name);
                }
            }
            R8G8B8toBMP(high_res_y, high_res_x, handle->y_decode, handle->y_decode, handle->y_decode, sfile_name);
        } else {
            handle->state = SM_CAPTURE;
        }
        break;
    case SM_BARCODE_DETECTED:
        break;
    }
    return EXECUTE_RETURN_OK;
} /* DemoBarcode_Execute */
