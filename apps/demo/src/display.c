/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include "display.h"

#define BYTES_PER_PIXEL     4

#define CLIP(X)             ( (X) > 255 ? 255 : (X) < 0 ? 0 : X)

#define C(Y)                ( (Y) - 16  )
#define D(U)                ( (U) - 128 )
#define E(V)                ( (V) - 128 )

#define YUV2R(Y, U, V)      CLIP(( 298 * C(Y)              + 409 * E(V) + 128) >> 8)
#define YUV2G(Y, U, V)      CLIP(( 298 * C(Y) - 100 * D(U) - 208 * E(V) + 128) >> 8)
#define YUV2B(Y, U, V)      CLIP(( 298 * C(Y) + 516 * D(U)              + 128) >> 8)

#define MAX_NR_STRINGS      128

/* The values in this array are a 8x8 bitmap font for ascii characters */
static uint64_t font[128] = {
	0x7E7E7E7E7E7E0000,	/* NUL */
	0x7E7E7E7E7E7E0000,	/* SOH */
	0x7E7E7E7E7E7E0000,	/* STX */
	0x7E7E7E7E7E7E0000,	/* ETX */
	0x7E7E7E7E7E7E0000,	/* EOT */
	0x7E7E7E7E7E7E0000,	/* ENQ */
	0x7E7E7E7E7E7E0000,	/* ACK */
	0x7E7E7E7E7E7E0000,	/* BEL */
	0x7E7E7E7E7E7E0000,	/* BS */
	0x0,			    /* TAB */
	0x7E7E7E7E7E7E0000,	/* LF */
	0x7E7E7E7E7E7E0000,	/* VT */
	0x7E7E7E7E7E7E0000,	/* FF */
	0x7E7E7E7E7E7E0000,	/* CR */
	0x7E7E7E7E7E7E0000,	/* SO */
	0x7E7E7E7E7E7E0000,	/* SI */
	0x7E7E7E7E7E7E0000,	/* DLE */
	0x7E7E7E7E7E7E0000,	/* DC1 */
	0x7E7E7E7E7E7E0000,	/* DC2 */
	0x7E7E7E7E7E7E0000,	/* DC3 */
	0x7E7E7E7E7E7E0000,	/* DC4 */
	0x7E7E7E7E7E7E0000,	/* NAK */
	0x7E7E7E7E7E7E0000,	/* SYN */
	0x7E7E7E7E7E7E0000,	/* ETB */
	0x7E7E7E7E7E7E0000,	/* CAN */
	0x7E7E7E7E7E7E0000,	/* EM */
	0x7E7E7E7E7E7E0000,	/* SUB */
	0x7E7E7E7E7E7E0000,	/* ESC */
	0x7E7E7E7E7E7E0000,	/* FS */
	0x7E7E7E7E7E7E0000,	/* GS */
	0x7E7E7E7E7E7E0000,	/* RS */
	0x7E7E7E7E7E7E0000,	/* US */
	0x0,			/* (space) */
	0x808080800080000,	/* ! */
	0x2828000000000000,	/* " */
	0x287C287C280000,	/* # */
	0x81E281C0A3C0800,	/* $ */
	0x6094681629060000,	/* % */
	0x1C20201926190000,	/* & */
	0x808000000000000,	/* ' */
	0x810202010080000,	/* ( */
	0x1008040408100000,	/* ) */
	0x2A1C3E1C2A000000,	/* * */
	0x8083E08080000,	/* + */
	0x81000,		/* , */
	0x3C00000000,		/* - */
	0x80000,		/* . */
	0x204081020400000,	/* / */
	0x1824424224180000,	/* 0 */
	0x8180808081C0000,	/* 1 */
	0x3C420418207E0000,	/* 2 */
	0x3C420418423C0000,	/* 3 */
	0x81828487C080000,	/* 4 */
	0x7E407C02423C0000,	/* 5 */
	0x3C407C42423C0000,	/* 6 */
	0x7E04081020400000,	/* 7 */
	0x3C423C42423C0000,	/* 8 */
	0x3C42423E023C0000,	/* 9 */
	0x80000080000,		/* : */
	0x80000081000,		/* ; */
	0x6186018060000,	/* < */
	0x7E007E000000,		/* = */
	0x60180618600000,	/* > */
	0x3844041800100000,	/* ? */
	0x3C449C945C201C,	/* @ */
	0x1818243C42420000,	/* A */
	0x7844784444780000,	/* B */
	0x3844808044380000,	/* C */
	0x7844444444780000,	/* D */
	0x7C407840407C0000,	/* E */
	0x7C40784040400000,	/* F */
	0x3844809C44380000,	/* G */
	0x42427E4242420000,	/* H */
	0x3E080808083E0000,	/* I */
	0x1C04040444380000,	/* J */
	0x4448507048440000,	/* K */
	0x40404040407E0000,	/* L */
	0x4163554941410000,	/* M */
	0x4262524A46420000,	/* N */
	0x1C222222221C0000,	/* O */
	0x7844784040400000,	/* P */
	0x1C222222221C0200,	/* Q */
	0x7844785048440000,	/* R */
	0x1C22100C221C0000,	/* S */
	0x7F08080808080000,	/* T */
	0x42424242423C0000,	/* U */
	0x8142422424180000,	/* V */
	0x4141495563410000,	/* W */
	0x4224181824420000,	/* X */
	0x4122140808080000,	/* Y */
	0x7E040810207E0000,	/* Z */
	0x3820202020380000,	/* [ */
	0x4020100804020000,	/* \ */
	0x3808080808380000,	/* ] */
	0x1028000000000000,	/* ^ */
	0x7E0000,		/* _ */
	0x1008000000000000,	/* ` */
	0x3C023E463A0000,	/* a */
	0x40407C42625C0000,	/* b */
	0x1C20201C0000,		/* c */
	0x2023E42463A0000,	/* d */
	0x3C427E403C0000,	/* e */
	0x18103810100000,	/* f */
	0x344C44340438,		/* g */
	0x2020382424240000,	/* h */
	0x800080808080000,	/* i */
	0x800180808080870,	/* j */
	0x20202428302C0000,	/* k */
	0x1010101010180000,	/* l */
	0x665A42420000,		/* m */
	0x2E3222220000,		/* n */
	0x3C42423C0000,		/* o */
	0x5C62427C4040,		/* p */
	0x3A46423E0202,		/* q */
	0x2C3220200000,		/* r */
	0x1C201804380000,	/* s */
	0x103C1010180000,	/* t */
	0x2222261A0000,		/* u */
	0x424224180000,		/* v */
	0x81815A660000,		/* w */
	0x422418660000,		/* x */
	0x422214081060,		/* y */
	0x3C08103C0000,		/* z */
	0x1C103030101C0000,	/* { */
	0x808080808080800,	/* | */
	0x38080C0C08380000,	/* } */
	0x324C000000,		/* ~ */
	0x7E7E7E7E7E7E0000	/* DEL */
};

typedef struct {
    uint32_t x;
    uint32_t y;
    uint8_t r;
    uint8_t g;
    uint8_t b;
    font_size_t fs;
    char s[64];
} string_descriptor_t;

struct _display_handle {
    int                         fd;
    uint32_t                    *fb_bgra;
    struct fb_fix_screeninfo    finfo;
    struct fb_var_screeninfo    vinfo;

    uint32_t isd;
    string_descriptor_t         sd[MAX_NR_STRINGS];
};

static void InsertCharacter(
    uint32_t *bgra,
    uint32_t width,
    uint32_t height,
    uint32_t stride,
    char c,
    uint32_t x,
    uint32_t y,
    uint8_t r,
    uint8_t g,
    uint8_t b,
    font_size_t
    font_size)
{
    /* Character pixel position */
    uint32_t cx, cy;
    
    /* Test for supported character */
    if (c < (sizeof(font)/sizeof(font[0]))) {
        uint64_t char_font = font[(uint32_t)c];
        uint32_t char_size = 8 << font_size;

        for(cy=0; cy<char_size && (y+cy)<height; cy++) {
            for(cx=0; cx<char_size && (x+cx)<width; cx++) {
                uint32_t active = (char_font & ((uint64_t)0x1<<((7-(cy/(1<<font_size)))*8+7-(cx/(1<<font_size))))) != 0;
                if (active) {
                    bgra[(y+cy)*stride + (x+cx)] = (b << 0) + (g << 8) + (r << 16);
                }
            }
        }
    }
} /* InsertCharacter */

static void InsertString(uint32_t *bgra, uint32_t width, uint32_t height, uint32_t stride, const char *s, uint32_t x, uint32_t y, uint8_t r, uint8_t g, uint8_t b, font_size_t font_size)
{
    /* Character pixel position */
    uint32_t i = 0, px;
    
    px = x;
    for(i=0; i < strlen(s) && px<width; i++) {
        InsertCharacter(bgra, width, height, stride, s[i], px, y, r, g, b, font_size);
        px += 8 << font_size;
    }
} /* InsertString */

int Display_Init(display_handle_t *phandle, const char *sdevice)
{
    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (display_handle_t) malloc(sizeof(struct _display_handle))) == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    memset(*phandle, 0, sizeof(struct _display_handle));

    display_handle_t handle = *phandle;
    if ((handle->fd = open(sdevice, O_RDWR)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    // Get fixed screen information
    if (ioctl(handle->fd, FBIOGET_FSCREENINFO, &handle->finfo) == -1) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Display_Terminate(phandle);
        return -1;
    }

    // Get variable screen information
    if (ioctl(handle->fd, FBIOGET_VSCREENINFO, &handle->vinfo) == -1) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Display_Terminate(phandle);
        return -1;
    }

    if (handle->vinfo.bits_per_pixel != 32) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Display_Terminate(phandle);
        return -1;        
    }

    // Double virtual screen size for 'double buffering'.
    handle->vinfo.yres_virtual = 2 * handle->vinfo.yres;
    if (ioctl(handle->fd, FBIOPUT_VSCREENINFO, &handle->vinfo) == -1) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Display_Terminate(phandle);
        return -1;
    }
    
    if ((handle->fb_bgra = (uint32_t *)mmap(0, handle->vinfo.yres_virtual * handle->finfo.line_length, PROT_READ | PROT_WRITE, MAP_SHARED, handle->fd, 0)) == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Display_Terminate(phandle);
        return -1;
    }

    return 0;
} /* Display_Init */

int Display_ShowImageRGB888(display_handle_t handle, uint32_t width, uint32_t height, uint32_t stride, uint8_t *rgb)
{
    uint32_t px = 0, py = 0, i = 0;
    uint32_t w = width  < handle->vinfo.xres ? width  : handle->vinfo.xres;
    uint32_t h = height < handle->vinfo.yres ? height : handle->vinfo.yres;

    handle->vinfo.yoffset = handle->vinfo.yoffset == 0 ? handle->vinfo.yres : 0;

    for (py = 0; py < h; py++) {
        uint32_t idx_rgb   = py * stride;
        uint32_t idx_bgra = (py + handle->vinfo.yoffset) * handle->finfo.line_length/sizeof(*(handle->fb_bgra));

        for (px = 0; px < w; px++, idx_rgb+=3, idx_bgra++) {
            handle->fb_bgra[idx_bgra] = (rgb[idx_rgb+2] << 0) + 
                                        (rgb[idx_rgb+1] << 8) + 
                                        (rgb[idx_rgb+0] << 16);
        }
    }

    /* Insert text in image */
    for (i = 0; i < handle->isd; i++) {
        InsertString( &handle->fb_bgra[handle->vinfo.yoffset * handle->finfo.line_length/sizeof(*(handle->fb_bgra))],
                      w,
                      h,
                      handle->finfo.line_length/sizeof(*(handle->fb_bgra)),
                      handle->sd[i].s,
                      handle->sd[i].x,
                      handle->sd[i].y,
                      handle->sd[i].r,
                      handle->sd[i].g,
                      handle->sd[i].b,
                      handle->sd[i].fs);
    }

    if (ioctl(handle->fd, FBIOPAN_DISPLAY, &handle->vinfo) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    return 0;
} /* Display_ShowImageRGB888 */

int Display_GetImageBufferBGRA(display_handle_t handle, uint32_t **bgra)
{
    handle->vinfo.yoffset = handle->vinfo.yoffset == 0 ? handle->vinfo.yres : 0;
    (*bgra) = &handle->fb_bgra[handle->vinfo.yoffset * handle->finfo.line_length/sizeof(*(handle->fb_bgra))];
    return 0;
} /* Display_GetImageBufferBGRA */

int Display_PutImageBufferBGRA(display_handle_t handle)
{
    uint32_t i = 0;

    /* Insert text in image */
    for (i = 0; i < handle->isd; i++) {
        InsertString( &handle->fb_bgra[handle->vinfo.yoffset * handle->finfo.line_length/sizeof(*(handle->fb_bgra))],
                      handle->vinfo.xres,
                      handle->vinfo.yres,
                      handle->finfo.line_length/sizeof(*(handle->fb_bgra)),
                      handle->sd[i].s,
                      handle->sd[i].x,
                      handle->sd[i].y,
                      handle->sd[i].r,
                      handle->sd[i].g,
                      handle->sd[i].b,
                      handle->sd[i].fs);
    }

    if (ioctl(handle->fd, FBIOPAN_DISPLAY, &handle->vinfo) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    return 0;
} /* Display_PutImageBufferBGRA */

uint32_t Display_GetResolutionX(display_handle_t handle)
{
    return handle->vinfo.xres;
} /* Display_GetResolutionX */

uint32_t Display_GetResolutionY(display_handle_t handle)
{
    return handle->vinfo.yres;
} /* Display_GetResolutionY */

void InsertText(display_handle_t handle, char *s, uint32_t x, uint32_t y, uint8_t r, uint8_t g, uint8_t b, font_size_t font_size)
{
    if (handle->isd < (MAX_NR_STRINGS-1)) {
        handle->sd[handle->isd].x = x;
        handle->sd[handle->isd].y = y;
        handle->sd[handle->isd].r = r;
        handle->sd[handle->isd].g = g;
        handle->sd[handle->isd].b = b;
        handle->sd[handle->isd].fs = font_size;
        strncpy(handle->sd[handle->isd].s, s, sizeof(handle->sd[handle->isd].s));
        handle->sd[handle->isd].s[sizeof(handle->sd[handle->isd].s)-1] = '\0';
        handle->isd++;        
    }
} /* InsertText */

void ClearText(display_handle_t handle)
{
    handle->isd = 0;
} /* ClearText */

void Display_Terminate(display_handle_t *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    close((*phandle)->fd);
    free(*phandle);
    *phandle = NULL;
} /* Display_Terminate */

void YtoBGRA(uint8_t *y, uint32_t width, uint32_t height, uint32_t *bgra)
{   uint32_t px = 0, py = 0;

    for(py = 0; py < height; py++) {
        for(px = 0; px < width; px++) {
            uint32_t idx = py*width+px;
            bgra[idx] = (y[idx] << 0) + (y[idx] << 8) + (y[idx] << 16);
        }
    }
}
