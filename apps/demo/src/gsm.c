/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

#include "gsm.h"

struct _gsm_handle {
    int fd;
};

int Gsm_Init(gsm_handle_t *phandle, const char *sdevice)
{   
    struct termios tty;

    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (gsm_handle_t) malloc(sizeof(struct _gsm_handle))) == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    gsm_handle_t handle = *phandle;
    handle->fd = -1;

    if ((handle->fd = open(sdevice, O_RDWR)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Gsm_Terminate(phandle);
        return -1;
    }

    memset (&tty, 0, sizeof tty);
    if (tcgetattr (handle->fd, &tty) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Gsm_Terminate(phandle);
        return -1;
    }
    cfsetospeed (&tty, B9600);
    cfsetispeed (&tty, B9600);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8; // 8-bit chars
  // disable IGNBRK for mismatched speed tests; otherwise receive break
    tty.c_iflag &= ~IGNBRK;                     // disable break processing
    tty.c_lflag = 0;                            // no signaling chars, no echo,
                                                // no canonical processing
    tty.c_oflag = 0;                            // no remapping, no delays
    tty.c_cc[VMIN]  = 0;                        // read doesn't block
    tty.c_cc[VTIME] = 5;                        // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY);     // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);            // ignore modem controls,
                                                // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);          // shut off parity
    tty.c_cflag |= 0;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (handle->fd, TCSANOW, &tty) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Gsm_Terminate(phandle);
        return -1;
    }
    return 0;
} /* Gsm_Init */

void Gsm_Terminate(gsm_handle_t *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    if ((*phandle)->fd >=0) close((*phandle)->fd);
    free(*phandle);
    *phandle = NULL;
} /* Gsm_Terminate */

send_sms_return_t Gsm_SendSMS(gsm_handle_t handle, const char *sphone_number, const char *smessage)
{
    int i = 0;
    char swrite[128], sread[1024];

    if (handle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return SEND_SMS_RETURN_ERROR;
    }

    /* Read serial port to make sure there is no data in it */
    read(handle->fd, sread, sizeof(sread));

    snprintf(swrite, sizeof(swrite)-1, "AT+CMGF=1\r");
    if (write(handle->fd, swrite, strlen(swrite)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return SEND_SMS_RETURN_ERROR;
    }

    usleep(200000);
    memset(sread, 0, sizeof(sread));
    if (read(handle->fd, sread, sizeof(sread)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return SEND_SMS_RETURN_ERROR;
    }

    /* Check if OK is returned correct */
    if (strstr(sread, "OK") == NULL) {
        /* Error swicthing to text mode */
        return SEND_SMS_RETURN_FAILURE;
    }

    snprintf(swrite, sizeof(swrite)-1, "AT+CMGS=\"%s\",145\r", sphone_number);
    if (write(handle->fd, swrite, strlen(swrite)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    usleep(200000);
    memset(sread, 0, sizeof(sread));
    if (read(handle->fd, sread, sizeof(sread)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return SEND_SMS_RETURN_ERROR;
    }

    /* Check if OK is returned correct */
    if (strstr(sread, ">") == NULL) {
        /* Error swicthing to type mode */
        return SEND_SMS_RETURN_FAILURE;
    }

    snprintf(swrite, sizeof(swrite)-1, "%s\x1A", smessage);
    if (write(handle->fd, swrite, strlen(swrite)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return SEND_SMS_RETURN_ERROR;
    }

    for (i = 0; i < 20; i++) {
        usleep(200000);
        memset(sread, 0, sizeof(sread));
        if (read(handle->fd, sread, sizeof(sread)) < 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return SEND_SMS_RETURN_ERROR;
        }
        /* Check if OK is returned correct */
        if (strstr(sread, "+CMGS") != NULL) {
            /* Message is transmitetd correct */
            return SEND_SMS_RETURN_OK;
        }
    }
    return SEND_SMS_RETURN_FAILURE;
} /* Gsm_SendSMS */






