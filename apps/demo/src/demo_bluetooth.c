/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include "create_handle.h"
#include "demo_bluetooth.h"

struct _demo_bluetooth_handle {
    keyboard_handle_t   keyboard;
    display_handle_t    display;
    bluetooth_handle_t  bluetooth;
};

int DemoBluetooth_Init(
    demo_bluetooth_handle_t *phandle,
    keyboard_handle_t       keyboard,
    display_handle_t        display,
    bluetooth_handle_t      bluetooth)
{
    if (keyboard == NULL || display == NULL || bluetooth == NULL) {
        return -1;
    }
    return CreateHandle_Init((void **)phandle, sizeof(struct _demo_bluetooth_handle));
} /* DemoBluetooth_Init */

void DemoBluetooth_Terminate(
    demo_bluetooth_handle_t *phandle)
{
    CreateHandle_Terminate((void **)phandle);
} /* DemoBluetooth_Terminate */

execute_return_t DemoBluetooth_Execute(
    demo_bluetooth_handle_t handle)
{
    key_event_t key_event = Keyboard_GetEvent(handle->keyboard);
    if (key_event == KEY_EVENT_ERROR) {
        return EXECUTE_RETURN_ERROR;
    }
    if (key_event == KEY_EVENT_BACK) {
        return EXECUTE_RETURN_FINISHED;
    }
    printf("%s", __func__);
    return EXECUTE_RETURN_OK;
} /* DemoBluetooth_Execute */
