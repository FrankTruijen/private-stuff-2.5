/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bmp.h"

char *GetDateTime()
{
    static char s[128];
    time_t t = time(0);
    struct tm * now = localtime( & t );

    memset(s, 0, sizeof(s));
    snprintf(s, sizeof(s)-1, "%04d-%02d-%02d_%02dh%02dm%02ds", (now->tm_year + 1900), (now->tm_mon + 1), now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
    return s;
} /* GetDateTime */

void R8G8B8toBMP(int width, int height, unsigned char *r, unsigned char *g, unsigned char *b, char *sname)
{
  FILE *f;
  unsigned char *rgb = NULL;
  int filesize = 54 + 3*width*height;

  unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
  unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
  unsigned char bmppad[3] = {0,0,0};

  unsigned int i = 0;

  rgb =(unsigned char *) malloc(3*width*height*sizeof(unsigned char));
  memset(rgb, 0, 3*width*height);

  for (int i=0; i<width; i++) {
    for (int j=0; j<height; j++) {
      unsigned int x=i;
      unsigned int y=(height-1)-j;

      rgb[(x+y*width)*3+2] = r[j*width+i];
      rgb[(x+y*width)*3+1] = g[j*width+i];
      rgb[(x+y*width)*3+0] = b[j*width+i];
    }
  }

  bmpfileheader[ 2] = (unsigned char)(filesize    );
  bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
  bmpfileheader[ 4] = (unsigned char)(filesize>>16);
  bmpfileheader[ 5] = (unsigned char)(filesize>>24);

  bmpinfoheader[ 4] = (unsigned char)(width    );
  bmpinfoheader[ 5] = (unsigned char)(width>> 8);
  bmpinfoheader[ 6] = (unsigned char)(width>>16);
  bmpinfoheader[ 7] = (unsigned char)(width>>24);
  bmpinfoheader[ 8] = (unsigned char)(height    );
  bmpinfoheader[ 9] = (unsigned char)(height>> 8);
  bmpinfoheader[10] = (unsigned char)(height>>16);
  bmpinfoheader[11] = (unsigned char)(height>>24);

  if ((f = fopen(sname,"wb")) != NULL) {
    fwrite(bmpfileheader,1,14,f);
    fwrite(bmpinfoheader,1,40,f);
    for(i=0; i<height; i++) {
      fwrite(rgb+(width*(height-i-1)*3),3,width,f);
      fwrite(bmppad,1,(4-(width*3)%4)%4,f);
    }
    fclose(f);
  }
  else {
    fprintf(stderr, "Unable to open file: '%s'\n", sname);
  }
} /* R8G8B8toBMP */