/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bmp.h"
#include "create_handle.h"
#include "finger_background.h"
#include "demo_finger_print_scanner.h"

typedef enum {
    SM_OFF = 0,
    SM_STATUS_WAIT_FOR_FINGER,
    SM_STATUS_DETECTED_FINGER
} demo_fps_state_machine_t;

struct _demo_finger_print_scanner_handle {
    keyboard_handle_t               keyboard;
    display_handle_t                display;
    finger_print_scanner_handle_t   finger_print_scanner;

    /* Video output signals */
    uint32_t                        width;
    uint32_t                        height;
    uint8_t                         *y, *u, *v;

    /* Finger print scanner */
    uint32_t                        width_fps;
    uint32_t                        height_fps;
    uint8_t                         *y1x1_fps;
    uint8_t                         *y1x2_fps;

    demo_fps_state_machine_t        state;
};

static void HorDown2(uint8_t *in, uint8_t *out, uint32_t width, uint32_t height, uint32_t istride, uint32_t ostride)
{
    uint32_t py=0, px=0;
    for (py=0; py<height; py++) {
        for (px=0; px<(width/2); px++) {
            uint32_t pixm, pix, pixp;
            int32_t opxm = 2*px-1;
            int32_t  opx = 2*px;
            int32_t opxp = 2*px+1;
            if (opxm < 0) opxm = 0;
            if (opxp >= width) opxp = width-1;

            pixm = in[py*istride+opxm];
            pix  = in[py*istride+opx];
            pixp = in[py*istride+opxp];

            out[py*ostride+px] = (uint8_t)((pixm + (pix<<1) + pixp + 2) >> 2);
        }
    }
} /* hor_down2 */

static void VertDown2(uint8_t *in, uint8_t *out, uint32_t width, uint32_t height, uint32_t istride, uint32_t ostride)
{
    uint32_t py=0, px=0;
    for (py=0; py<height/2; py++) {
        for (px=0; px<width; px++) {
            uint32_t pixm, pix, pixp;
            uint32_t opym = 2*py-1;
            uint32_t opy  = 2*py;
            uint32_t opyp = 2*py+1;
            if (opym < 0) opym = 0;
            if (opyp >= height) opyp = height-1;

            pixm = in[opym*istride+px];
            pix  = in[opy *istride+px];
            pixp = in[opyp*istride+px];

            out[py*ostride+px] = (uint8_t)((pixm + (pix<<1) + pixp + 2) >> 2);
        }
    }
} /* vert_down2 */


int DemoFingerPrintScanner_Init(
    demo_finger_print_scanner_handle_t  *phandle,
    keyboard_handle_t                   keyboard,
    display_handle_t                    display,
    finger_print_scanner_handle_t       finger_print_scanner)
{
    demo_finger_print_scanner_handle_t  handle = NULL;

    if (keyboard == NULL || display == NULL || finger_print_scanner == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (CreateHandle_Init((void **)phandle, sizeof(struct _demo_finger_print_scanner_handle)) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    handle = *phandle;

    handle->keyboard = keyboard;
    handle->display = display;
    handle->finger_print_scanner = finger_print_scanner;

    if ((handle->width  = Display_GetResolutionX(handle->display)) < 0  ||
        (handle->height = Display_GetResolutionY(handle->display)) < 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if ((handle->y = malloc(handle->height * handle->width / 1)) == NULL ||
        (handle->u = malloc(handle->height * handle->width / 2)) == NULL ||
        (handle->v = malloc(handle->height * handle->width / 2)) == NULL) {
        
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    memset(handle->y, 0,   handle->height * handle->width / 1);
    memset(handle->u, 128, handle->height * handle->width / 2);
    memset(handle->v, 128, handle->height * handle->width / 2);

    if ((handle->width_fps  = FingerPrintScanner_GetResolutionX(handle->finger_print_scanner)) < 0  ||
        (handle->height_fps = FingerPrintScanner_GetResolutionY(handle->finger_print_scanner)) < 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if ((handle->y1x1_fps = malloc(handle->height_fps * handle->width_fps))     == NULL     ||
        (handle->y1x2_fps = malloc(handle->height_fps * handle->width_fps / 2)) == NULL) {

        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    return 0;
} /* DemoFingerPrintScanner_Init */

void DemoFingerPrintScanner_Terminate(
    demo_finger_print_scanner_handle_t  *phandle)
{
    free((*phandle)->y1x1_fps);
    free((*phandle)->y1x2_fps);
    free((*phandle)->y);
    free((*phandle)->u);
    free((*phandle)->v);
    CreateHandle_Terminate((void **)phandle);
} /* DemoFingerPrintScanner_Terminate */

execute_return_t DemoFingerPrintScanner_Execute(
    demo_finger_print_scanner_handle_t  handle)
{
    uint32_t *bgra = NULL;
    key_event_t key_event = Keyboard_GetEvent(handle->keyboard);

    if (key_event == KEY_EVENT_ERROR) {
        printf("%s[%d]\n", __func__, __LINE__);
        return EXECUTE_RETURN_ERROR;
    }
    if (key_event == KEY_EVENT_BACK) {
        handle->state = SM_OFF;
        return EXECUTE_RETURN_FINISHED;
    }

    switch(handle->state) {
    case SM_OFF:
        /* Show finger icon on display */
        ClearText(handle->display);
        if (Display_GetImageBufferBGRA(handle->display, &bgra) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return -1;
        }

        if (WIDTH_finger_background  != Display_GetResolutionX(handle->display) ||
            HEIGHT_finger_background != Display_GetResolutionY(handle->display)) {
            printf("%s[%d]\n", __func__, __LINE__);
            return -1;
        }

        YtoBGRA(data_finger_background, WIDTH_finger_background, HEIGHT_finger_background, bgra);
        
        if (Display_PutImageBufferBGRA(handle->display) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return -1;
        }
        handle->state = SM_STATUS_WAIT_FOR_FINGER;
        break;
    case SM_STATUS_WAIT_FOR_FINGER:
        /* Check if finger is available */
        if (FingerPrintScanner_IsFingerPresent(handle->finger_print_scanner)) {
            uint32_t width_fps_x2  = handle->width_fps/2;
            uint32_t height_fps_x2 = handle->height_fps/2;
            uint8_t *ycenter = &handle->y[((handle->height-height_fps_x2)/2)*handle->width + ((handle->width-width_fps_x2)/2)];
            char sfile_name[128];

            /* Get finger print */
            if (FingerPrintScanner_GetImageY(handle->finger_print_scanner, handle->y1x1_fps) != 0) {
                printf("%s[%d]\n", __func__, __LINE__);
                break;
            }

            HorDown2(handle->y1x1_fps, handle->y1x2_fps, handle->width_fps, handle->height_fps, handle->width_fps, width_fps_x2);
            VertDown2(handle->y1x2_fps, ycenter, width_fps_x2, handle->height_fps, width_fps_x2, handle->width);

            if (Display_GetImageBufferBGRA(handle->display, &bgra) != 0) {
                printf("%s[%d]\n", __func__, __LINE__);
                return -1;
            }

            if (handle->width  != Display_GetResolutionX(handle->display) ||
                handle->height != Display_GetResolutionY(handle->display)) {
                printf("%s[%d]\n", __func__, __LINE__);
                return -1;
            }

            YtoBGRA(handle->y, handle->width, handle->height, bgra);
            
            if (Display_PutImageBufferBGRA(handle->display) != 0) {
                printf("%s[%d]\n", __func__, __LINE__);
                return -1;
            }

            /* Store finger print on SD card */
            memset(sfile_name, 0, sizeof(sfile_name));
            snprintf(sfile_name, sizeof(sfile_name)-1, "/media/sd0/finger_print_%s.bmp", GetDateTime());

            R8G8B8toBMP(handle->width_fps, handle->height_fps, handle->y1x1_fps, handle->y1x1_fps, handle->y1x1_fps, sfile_name);

            handle->state = SM_STATUS_DETECTED_FINGER;
        }
        break;
    case SM_STATUS_DETECTED_FINGER:
        break;
    }
    return EXECUTE_RETURN_OK;
} /* DemoFingerPrintScanner_Execute */