/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "alsa/asoundlib.h"

#include "audio.h"

struct _audio_handle {
    snd_pcm_t               *pcm_handle;

    unsigned int            channels;
    snd_pcm_format_t        format;
    snd_pcm_sframes_t       buffer_size;
    snd_pcm_sframes_t       period_size;
    snd_output_t            *output;
    snd_pcm_hw_params_t     *hwparams;
    snd_pcm_sw_params_t     *swparams;
    snd_pcm_channel_area_t  *areas;
    signed short            *samples;

    const int16_t           *audio_data_head;
    const int16_t           *audio_data_curr;
    uint32_t                audio_data_size;
};

static int CopyAudio(audio_handle_t handle, int count)
{
    uint32_t  data_available;
    unsigned char *samples[2];
    int steps[2];
    unsigned int chn;

    /* Check if data available */
    if (handle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    /* Check if data available */
    data_available = handle->audio_data_size - ((uint8_t*)handle->audio_data_curr - (uint8_t*)handle->audio_data_head);
    if (data_available < (count * 4)) {
        return -1;
    }

    /* verify and prepare the contents of areas */
    for (chn = 0; chn < handle->channels; chn++) {
        if ((handle->areas[chn].first % 8) != 0) {
            printf("areas[%i].first == %i, aborting...\n", chn, handle->areas[chn].first);
            return -1;
        }
        samples[chn] = /*(signed short *)*/(((unsigned char *)handle->areas[chn].addr) + (handle->areas[chn].first / 8));
        if ((handle->areas[chn].step % 16) != 0) {
            printf("areas[%i].step == %i, aborting...\n", chn, handle->areas[chn].step);
            return -1;
        }
        steps[chn] = handle->areas[chn].step / 8;
        samples[chn] += steps[chn];
    }

    /* fill the channel areas */
    while (count-- > 0) {
        int16_t sample_channel_0, sample_channel_1;

        sample_channel_0 = *handle->audio_data_curr;
        *(samples[0]+0) = ((uint8_t *)&sample_channel_0)[0];
        *(samples[0]+1) = ((uint8_t *)&sample_channel_0)[1];
        samples[0] += steps[0];

        /* Audio speaker in bridge mode */
        sample_channel_1 = sample_channel_0 * -1;
        *(samples[1]+0) = ((uint8_t *)&sample_channel_1)[0];
        *(samples[1]+1) = ((uint8_t *)&sample_channel_1)[1];
        samples[1] += steps[1];

        handle->audio_data_curr+=2;
    }
    return 0;
} /* LoadWave */

static int SetHardwareParamteres(audio_handle_t handle, snd_pcm_hw_params_t *params)
{
    unsigned int rrate;
    snd_pcm_uframes_t size;
    int err, dir;

    int resample = 1;                                 /* enable alsa-lib resampling */
    unsigned int rate = 44100;                        /* stream rate */
    unsigned int buffer_time = 500000;                /* ring buffer length in us */
    unsigned int period_time = 100000;                /* period time in us */

    /* Check if data available */
    if (handle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    
    /* choose all parameters */
    err = snd_pcm_hw_params_any(handle->pcm_handle, params);
    if (err < 0) {
        printf("Broken configuration for playback: no configurations available: %s\n", snd_strerror(err));
        return -1;
    }
    /* set hardware resampling */
    err = snd_pcm_hw_params_set_rate_resample(handle->pcm_handle, params, resample);
    if (err < 0) {
        printf("Resampling setup failed for playback: %s\n", snd_strerror(err));
        return -1;
    }
    /* set the interleaved read/write format */
    err = snd_pcm_hw_params_set_access(handle->pcm_handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
    if (err < 0) {
        printf("Access type not available for playback: %s\n", snd_strerror(err));
        return -1;
    }
    /* set the sample format */
    err = snd_pcm_hw_params_set_format(handle->pcm_handle, params, handle->format);
    if (err < 0) {
        printf("Sample format not available for playback: %s\n", snd_strerror(err));
        return -1;
    }
    /* set the count of channels */
    err = snd_pcm_hw_params_set_channels(handle->pcm_handle, params, handle->channels);
    if (err < 0) {
        printf("Channels count (%i) not available for playbacks: %s\n", handle->channels, snd_strerror(err));
        return -1;
    }
    /* set the stream rate */
    rrate = rate;
    err = snd_pcm_hw_params_set_rate_near(handle->pcm_handle, params, &rrate, 0);
    if (err < 0) {
        printf("Rate %iHz not available for playback: %s\n", rate, snd_strerror(err));
        return -1;
    }
    if (rrate != rate) {
        printf("Rate doesn't match (requested %iHz, get %iHz)\n", rate, err);
        return -1;
    }
    /* set the buffer time */
    err = snd_pcm_hw_params_set_buffer_time_near(handle->pcm_handle, params, &buffer_time, &dir);
    if (err < 0) {
        printf("Unable to set buffer time %i for playback: %s\n", buffer_time, snd_strerror(err));
        return -1;
    }
    err = snd_pcm_hw_params_get_buffer_size(params, &size);
    if (err < 0) {
        printf("Unable to get buffer size for playback: %s\n", snd_strerror(err));
        return -1;
    }
    handle->buffer_size = size;
    /* set the period time */
    err = snd_pcm_hw_params_set_period_time_near(handle->pcm_handle, params, &period_time, &dir);
    if (err < 0) {
        printf("Unable to set period time %i for playback: %s\n", period_time, snd_strerror(err));
        return -1;
    }
    err = snd_pcm_hw_params_get_period_size(params, &size, &dir);
    if (err < 0) {
        printf("Unable to get period size for playback: %s\n", snd_strerror(err));
        return -1;
    }
    handle->period_size = size;
    /* write the parameters to device */
    err = snd_pcm_hw_params(handle->pcm_handle, params);
    if (err < 0) {
        printf("Unable to set hw params for playback: %s\n", snd_strerror(err));
        return -1;
    }
    return 0;
} /* SetHardwareParamteres */

static int SetSoftwareParameters(audio_handle_t handle, snd_pcm_sw_params_t *swparams)
{
    int err;
    const int period_event = 0;                     /* produce poll event after each period */

    /* Check if data available */
    if (handle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    
    /* get the current swparams */
    err = snd_pcm_sw_params_current(handle->pcm_handle, swparams);
    if (err < 0) {
        printf("Unable to determine current swparams for playback: %s\n", snd_strerror(err));
        return -1;
    }
    /* start the transfer when the buffer is almost full: */
    /* (buffer_size / avail_min) * avail_min */
    err = snd_pcm_sw_params_set_start_threshold(handle->pcm_handle, swparams, (handle->buffer_size / handle->period_size) * handle->period_size);
    if (err < 0) {
        printf("Unable to set start threshold mode for playback: %s\n", snd_strerror(err));
        return -1;
    }
    /* allow the transfer when at least period_size samples can be processed */
    /* or disable this mechanism when period event is enabled (aka interrupt like style processing) */
    err = snd_pcm_sw_params_set_avail_min(handle->pcm_handle, swparams, period_event ? handle->buffer_size : handle->period_size);
    if (err < 0) {
        printf("Unable to set avail min for playback: %s\n", snd_strerror(err));
        return -1;
    }
    /* enable period events when requested */
    if (period_event) {
        err = snd_pcm_sw_params_set_period_event(handle->pcm_handle, swparams, 1);
        if (err < 0) {
            printf("Unable to set period event: %s\n", snd_strerror(err));
            return -1;
        }
    }
    /* write the parameters to the playback device */
    err = snd_pcm_sw_params(handle->pcm_handle, swparams);
    if (err < 0) {
        printf("Unable to set sw params for playback: %s\n", snd_strerror(err));
        return -1;
    }
    return 0;
} /* SetSoftwareParameters */

int Audio_Init(audio_handle_t *phandle)
{
    audio_handle_t handle = NULL;
    int err;
    unsigned int chn;
    const char *device = "plughw:0,0";

    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (audio_handle_t) malloc(sizeof(struct _audio_handle))) == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    handle = *phandle;

    handle->channels = 2;
    handle->format = SND_PCM_FORMAT_S16;
    handle->buffer_size = 0;
    handle->period_size = 0;
    handle->output = NULL;

    handle->audio_data_size = 0;
    handle->audio_data_head = NULL;
    handle->audio_data_curr = NULL;

    snd_pcm_hw_params_alloca(&handle->hwparams);
    snd_pcm_sw_params_alloca(&handle->swparams);
    err = snd_output_stdio_attach(&handle->output, stdout, 0);
    if (err < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    if ((err = snd_pcm_open(&handle->pcm_handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return 0;
    }
    
    if ((err = SetHardwareParamteres(handle, handle->hwparams)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    if ((err = SetSoftwareParameters(handle, handle->swparams)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    handle->samples = malloc((handle->period_size * handle->channels * snd_pcm_format_physical_width(handle->format)) / 8);
    if (handle->samples == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    
    handle->areas = calloc(handle->channels, sizeof(snd_pcm_channel_area_t));
    if (handle->areas == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    for (chn = 0; chn < handle->channels; chn++) {
        handle->areas[chn].addr = handle->samples;
        handle->areas[chn].first = chn * snd_pcm_format_physical_width(handle->format);
        handle->areas[chn].step = handle->channels * snd_pcm_format_physical_width(handle->format);
    }
    return 0;
} /* Audio_Init */

void Audio_Terminate(audio_handle_t *phandle)
{
    audio_handle_t handle = NULL;
    
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    handle = *phandle;

    free(handle->areas);
    free(handle->samples);
    snd_pcm_close(handle->pcm_handle);

    free(handle);
    *phandle = NULL;
} /* Audio_Terminate */

int Audio_SetStream(audio_handle_t handle, const int16_t *data, uint32_t size)
{
    /* Check if data available */
    if (handle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    handle->audio_data_size = size;
    handle->audio_data_head = data;
    handle->audio_data_curr = handle->audio_data_head;
    
    if (snd_pcm_prepare(handle->pcm_handle) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    return 0;
} /* Audio_SetStream */

int Audio_Play(audio_handle_t handle)
{
    signed short *ptr;
    int err, cptr;
    ptr = handle->samples;
    cptr = handle->period_size;

    /* Check if data available */
    if (handle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if (CopyAudio(handle, handle->period_size) != 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;    
    }

    while (cptr > 0) {
        err = snd_pcm_writei(handle->pcm_handle, ptr, cptr);
        if (err == -EAGAIN) {
            continue;
        }
        if (err < 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return -1;
        }
        ptr += err * handle->channels;
        cptr -= err;
    }
    return 0;
} /* Audio_Play */
