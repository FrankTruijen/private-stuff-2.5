/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "bluetooth.h"

struct _bluetooth_handle {
    int fd;
};

int Bluetooth_Init(bluetooth_handle_t *phandle, const char *sdevice)
{   int ret = 0;

    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (bluetooth_handle_t) malloc(sizeof(struct _bluetooth_handle))) != NULL) {
        bluetooth_handle_t handle = *phandle;
        if ((handle->fd = open(sdevice, O_RDONLY|O_NONBLOCK)) < 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            ret = -1;
        }
    } else {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        ret = -1;
    }
    return ret;
} /* Bluetooth_Init */

void Bluetooth_Terminate(bluetooth_handle_t *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    close((*phandle)->fd);
    free(*phandle);
    *phandle = NULL;
} /* Bluetooth_Terminate */
