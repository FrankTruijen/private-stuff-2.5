/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <getopt.h>
#include <errno.h>
#include <malloc.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

#include "camera.h"

#define CLEAR(x) memset (&(x), 0, sizeof (x))

typedef struct {
        void *                  start;
        size_t                  length;
} buffer_t;

struct _camera_handle {
    int                 fd;

    resolution_mode_t   mode;
    
    struct v4l2_buffer  user_buf;
    buffer_t            *buffers;
    uint32_t            n_buffers;
};

typedef struct {
    uint32_t width;
    uint32_t height;
    uint32_t fps;
} camera_settings_t;

/* MODE_VGA_640_480 = 0,
 * MODE_NTSC_720_480 = 1,
 * MODE_720P_1280_720 = 2,
 * MODE_1080P_1920_1080 = 3,
 * MODE_QSXGA_2592_1944 = 4,
 */
const camera_settings_t camera_settings[MODE_MAX] = {
    { 640,  480, 30},
    { 720,  480, 30},
    {1280,  720, 30},
    {1920, 1080, 30},
    {2592, 1944, 15}
};

static int check_ioctl(int fd, int request, void* argp)
{
  int r;

  do r = ioctl(fd, request, argp);
  while (-1 == r && EINTR == errno);

  return r;
} /* check_ioctl */

static int ConfigureCameraSettings(camera_handle_t handle)
{
    struct v4l2_capability cap;
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;
    struct v4l2_format fmt;
    unsigned int min;

    if (check_ioctl(handle->fd, VIDIOC_QUERYCAP, &cap) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    /* Select video input, video standard and tune here. */
    CLEAR(cropcap);
    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (check_ioctl(handle->fd, VIDIOC_CROPCAP, &cropcap) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    crop.c = cropcap.defrect; /* reset to default */

    if (check_ioctl(handle->fd, VIDIOC_S_CROP, &crop) < 0) {
        if (errno != EINVAL) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return -1;
        }
    }
    CLEAR (fmt);

    // v4l2_format
    fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width       = camera_settings[handle->mode].width; 
    fmt.fmt.pix.height      = camera_settings[handle->mode].height;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;

    if (check_ioctl(handle->fd, VIDIOC_S_FMT, &fmt) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    /* Note VIDIOC_S_FMT may change width and height. */
//    if (handle->width != fmt.fmt.pix.width || handle->height != fmt.fmt.pix.height) {
//        printf("Failure function %s line %d\n", __func__, __LINE__);
//        return -1;
//    }

    /* Buggy driver paranoia. */
    min = fmt.fmt.pix.width * 2;
    if (fmt.fmt.pix.bytesperline < min) {
        fmt.fmt.pix.bytesperline = min;
    }
    min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
    if (fmt.fmt.pix.sizeimage < min) {
        fmt.fmt.pix.sizeimage = min;
    }
    return 0;
} /* ConfigureCameraSettings */

static int ConfigureCameraMmap(camera_handle_t handle)
{
    struct v4l2_requestbuffers req;

    CLEAR (req);

    req.count   = 4;
    req.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory  = V4L2_MEMORY_MMAP;

    if (check_ioctl(handle->fd, VIDIOC_REQBUFS, &req) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if (req.count < 2) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    handle->buffers = calloc(req.count, sizeof(*(handle->buffers)));

    if (!handle->buffers) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    for (handle->n_buffers = 0; handle->n_buffers < req.count; ++handle->n_buffers) {
        struct v4l2_buffer buf;

        CLEAR (buf);
        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = handle->n_buffers;

        if (check_ioctl(handle->fd, VIDIOC_QUERYBUF, &buf) < 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return -1;
        }

        handle->buffers[handle->n_buffers].length = buf.length;
        handle->buffers[handle->n_buffers].start = mmap (NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, handle->fd, buf.m.offset);

        if (handle->buffers[handle->n_buffers].start == MAP_FAILED) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return -1;
        }
    }
    return 0;
} /* ConfigureCameraMmap */

static int ConfigureCameraUnmap(camera_handle_t handle)
{
    unsigned int i;

    for (i = 0; i < handle->n_buffers; ++i) {
        if (munmap (handle->buffers[i].start, handle->buffers[i].length) < 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return -1;
        }
    }
    free(handle->buffers);
    return 0;
} /* ConfigureCameraUnmap */

static int ConfigureCameraCaptureStart(camera_handle_t handle)
{   uint32_t i;
    enum v4l2_buf_type type;
    struct v4l2_streamparm sp;

    for (i = 0; i < handle->n_buffers; ++i) {
        struct v4l2_buffer buf;

        CLEAR (buf);

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = i;

        if (check_ioctl(handle->fd, VIDIOC_QBUF, &buf) < 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return -1;
        }
    }

    memset(&sp, 0, sizeof(sp));

    sp.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    sp.parm.capture.capturemode = handle->mode;
    sp.parm.capture.timeperframe.denominator = camera_settings[handle->mode].fps;
    sp.parm.capture.timeperframe.numerator = 1;
    if (check_ioctl(handle->fd,  VIDIOC_S_PARM, &sp) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (check_ioctl(handle->fd, VIDIOC_STREAMON, &type) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    return 0;
} /* ConfigureCameraCaptureStart */

static int ConfigureCameraCaptureStop(camera_handle_t handle)
{   enum v4l2_buf_type type;

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (check_ioctl(handle->fd, VIDIOC_STREAMOFF, &type) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    return 0;
} /* ConfigureCameraCaptureStop */

int Camera_Init(camera_handle_t *phandle, const char *sdevice)
{
    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (camera_handle_t) malloc(sizeof(struct _camera_handle))) == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;    
    }
    memset((*phandle), 0, sizeof(struct _camera_handle));

    if (((*phandle)->fd = open(sdevice, O_RDWR | O_NONBLOCK, 0)) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        Camera_Terminate(phandle);
        return -1;
    }
    return 0;
} /* Camera_Init */

int Camera_Start(camera_handle_t handle, resolution_mode_t mode)
{
    if (handle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    handle->mode = mode;
    handle->buffers = NULL;
    handle->buffers = NULL;
    handle->n_buffers = 0;

    if (ConfigureCameraSettings(handle) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if (ConfigureCameraMmap(handle) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if (ConfigureCameraCaptureStart(handle) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    return 0;
} /* Camera_Start */

int Camera_Stop(camera_handle_t handle)
{
    if (handle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if (ConfigureCameraCaptureStop(handle) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    if (ConfigureCameraUnmap(handle) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    return 0;
} /* Camera_Stop */

camera_get_image_status_t Camera_GetImageYUV422(
    camera_handle_t     handle, 
    uint32_t            width, 
    uint32_t            height,
    uint32_t            stride_y,
    uint32_t            stride_uv,
    uint8_t             *y,
    uint8_t             *u,
    uint8_t             *v)
{
//    uint32_t px = 0, py = 0, h = 0, w = 0;
//    uint8_t *yuyv = NULL;
//    struct v4l2_buffer buf;
//    struct timeval tv;
//    fd_set fds;
//    int r;
//
//    FD_ZERO(&fds);
//    FD_SET(handle->fd, &fds);
//
//    /* Time-out */
//    tv.tv_sec = 2;
//    tv.tv_usec = 0;
//
//    r = select(handle->fd + 1, &fds, NULL, NULL, &tv);
//
//    if (r == -1) {
//        if (errno != EINTR) {
//            printf("Failure function %s line %d\n", __func__, __LINE__);
//            return CAMERA_ERROR;
//        }
//        return CAMERA_NO_FRAME;
//    }
//
//    if (r == 0) {
//        printf("Failure function %s line %d\n", __func__, __LINE__);
//        return CAMERA_ERROR;      /* Time-out error */
//    }
//
//    CLEAR (buf);
//
//    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//    buf.memory = V4L2_MEMORY_MMAP;
//
//    if (check_ioctl(handle->fd, VIDIOC_DQBUF, &buf) < 0) {
//        if (errno != EAGAIN) {
//            printf("Failure function %s line %d\n", __func__, __LINE__);
//            return CAMERA_ERROR;
//        }
//        return CAMERA_NO_FRAME;
//    }
//
//    assert(buf.index < handle->n_buffers);
//
//    h = height < handle->height ? height : handle->height;
//    w = width  < handle->width  ? width  : handle->width;
//
//    /* Convert combined buffers to separated buffers */
//    yuyv = handle->buffers[buf.index].start;
//    for (py = 0; py < h; py++) {
//        for (px = 0; px < w; px++) {
//            uint32_t idx_yuyv = py*handle->width*2 + px*2;
//            uint32_t idx_uv = py*stride_uv + px;
//            uint32_t idx_y  = py*stride_y + px;
//
//            y[idx_y] = yuyv[idx_yuyv+0];
//            if((px&0x1) == 0) {
//                u[idx_uv] = yuyv[idx_yuyv+1];
//            } else {
//                v[idx_uv] = yuyv[idx_yuyv+1];
//            }
//        }
//    }
//
//    if (check_ioctl(handle->fd, VIDIOC_QBUF, &buf) < 0) {
//        printf("Failure function %s line %d\n", __func__, __LINE__);
//        return CAMERA_ERROR;
//    }
    return CAMERA_NEW_FRAME;
} /* Camera_GetImageYUV422 */

camera_get_image_status_t Camera_GetImageBufferYUYV(
    camera_handle_t     handle, 
    uint32_t            **yuyv)
{
    struct timeval tv;
    fd_set fds;
    int r;

    FD_ZERO(&fds);
    FD_SET(handle->fd, &fds);

    /* Time-out */
    tv.tv_sec = 2;
    tv.tv_usec = 0;

    r = select(handle->fd + 1, &fds, NULL, NULL, &tv);

    if (r == -1) {
        if (errno != EINTR) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return CAMERA_ERROR;
        }
        return CAMERA_NO_FRAME;
    }

    if (r == 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return CAMERA_ERROR;      /* Time-out error */
    }

    CLEAR (handle->user_buf);

    handle->user_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    handle->user_buf.memory = V4L2_MEMORY_MMAP;

    if (check_ioctl(handle->fd, VIDIOC_DQBUF, &handle->user_buf) < 0) {
        if (errno != EAGAIN) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            return CAMERA_ERROR;
        }
        return CAMERA_NO_FRAME;
    }

    assert(handle->user_buf.index < handle->n_buffers);

    (*yuyv) = handle->buffers[handle->user_buf.index].start;
    return CAMERA_NEW_FRAME;
} /* Camera_GetImageBufferYUYV */

camera_get_image_status_t Camera_PutImageBufferYUYV(
    camera_handle_t     handle)
{
    if (check_ioctl(handle->fd, VIDIOC_QBUF, &handle->user_buf) < 0) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return CAMERA_ERROR;
    }
    return CAMERA_NEW_FRAME;
} /* Camera_PutImageBufferYUYV */

uint32_t Camera_GetResolutionX(camera_handle_t handle)
{
    return camera_settings[handle->mode].width;
} /* Camera_GetResolutionX */

uint32_t Camera_GetResolutionY(camera_handle_t handle)
{
    return camera_settings[handle->mode].height;
} /* Camera_GetResolutionY */

void Camera_Terminate(camera_handle_t *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    close((*phandle)->fd);
    free(*phandle);
    *phandle = NULL;
} /* Camera_Terminate */
