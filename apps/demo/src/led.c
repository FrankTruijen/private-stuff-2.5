/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "led.h"

struct _led_handle {
    int fd;
};

struct _led_rgb_handle {
    led_handle_t    red;
    led_handle_t    green;
    led_handle_t    blue;
};

int Led_Init(led_handle_t *phandle, const char *sdevice)
{   int ret = 0;

    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (led_handle_t) malloc(sizeof(struct _led_handle))) != NULL) {
        led_handle_t handle = *phandle;
        if ((handle->fd = open(sdevice, O_WRONLY )) < 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            ret = -1;
        }
    } else {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        ret = -1;
    }
    return ret;
} /* Led_Init */

int LedRgb_Init(led_rgb_handle_t *phandle, const char *sdevice_r, const char *sdevice_g, const char *sdevice_b)
{   int ret = 0;

    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (led_rgb_handle_t) malloc(sizeof(struct _led_rgb_handle))) != NULL) {
        led_rgb_handle_t handle = *phandle;

        if (Led_Init(&handle->red, sdevice_r)
        ||  Led_Init(&handle->green, sdevice_g)
        ||  Led_Init(&handle->blue, sdevice_b)) {
        
            return -1;
        }
    } else {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        ret = -1;
    }
    return ret;
} /* LedRgb_Init */

int Led_Value(led_handle_t handle, uint8_t brightness)
{
    char sbrightness[8];

    sprintf(sbrightness, "%d", brightness);
    if (write(handle->fd, sbrightness, strlen(sbrightness)) != strlen(sbrightness)) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }
    return 0;
} /* Led_Value */

int LedRgb_Value(led_rgb_handle_t handle, uint8_t brightness_r, uint8_t brightness_g, uint8_t brightness_b)
{
    if (Led_Value(handle->red,   brightness_r)
    ||  Led_Value(handle->green, brightness_g)
    ||  Led_Value(handle->blue,  brightness_b)) {

        return -1;
    }
    return 0;
} /* LedRgb_Value */

void Led_Terminate(led_handle_t *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    close((*phandle)->fd);
    free(*phandle);
    *phandle = NULL;
} /* Led_Terminate */

void LedRgb_Terminate(led_rgb_handle_t *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    Led_Terminate(&(*phandle)->red);
    Led_Terminate(&(*phandle)->green);
    Led_Terminate(&(*phandle)->blue);
    free(*phandle);
    *phandle = NULL;
} /* LedRgb_Terminate */
