/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/input.h>

#include "keyboard.h"

struct _keyboard_handle {
    int fd;
}; 

int Keyboard_Init(keyboard_handle_t *phandle, const char *sdevice)
{   int ret = 0;

    if (phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return -1;
    }

    if ((*phandle = (keyboard_handle_t) malloc(sizeof(struct _keyboard_handle))) != NULL) {
        keyboard_handle_t handle = *phandle;
        if ((handle->fd = open(sdevice, O_RDONLY|O_NONBLOCK)) < 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            ret = -1;
        }
    } else {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        ret = -1;
    }
    return ret;
} /* KeyboardInit */

key_event_t Keyboard_GetEvent(keyboard_handle_t handle)
{
    struct input_event scan_codes;
    key_event_t key_event = KEY_EVENT_NONE;

    if (read(handle->fd, (void *)&scan_codes, sizeof(scan_codes)) == sizeof(scan_codes)) {
        if ( scan_codes.type == EV_KEY ) {
            /* Only do something when key down event */
            if (scan_codes.value == 0) {
                switch (scan_codes.code) {
                case KEY_0:         key_event = KEY_EVENT_0;            break;
                case KEY_1:         key_event = KEY_EVENT_1;            break;
                case KEY_2:         key_event = KEY_EVENT_2;            break;
                case KEY_3:         key_event = KEY_EVENT_3;            break;
                case KEY_4:         key_event = KEY_EVENT_4;            break;
                case KEY_5:         key_event = KEY_EVENT_5;            break;
                case KEY_6:         key_event = KEY_EVENT_6;            break;
                case KEY_7:         key_event = KEY_EVENT_7;            break;
                case KEY_8:         key_event = KEY_EVENT_8;            break;
                case KEY_9:         key_event = KEY_EVENT_9;            break;
                case KEY_F1:        key_event = KEY_EVENT_F1;           break;
                case KEY_F2:        key_event = KEY_EVENT_F2;           break;
                case KEY_OK:        key_event = KEY_EVENT_GREEN_CHECK;  break;
                case KEY_BREAK:     key_event = KEY_EVENT_RED_FAIL;     break;
                case KEY_BACKSPACE: key_event = KEY_EVENT_BACK;         break;
                }
            }
        }
    }
    return key_event;
} /* KeyboardGetEvent */

void Keyboard_Terminate(keyboard_handle_t *phandle)
{
    if (phandle == NULL || *phandle == NULL) {
        printf("Failure function %s line %d\n", __func__, __LINE__);
        return;
    }
    close((*phandle)->fd);
    free(*phandle);
    *phandle = NULL;
} /* KeyboardTerminate */
