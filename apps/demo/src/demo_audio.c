/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "eagles.h"
#include "demo_audio.h"
#include "create_handle.h"
#include "speaker_background.h"

typedef enum {
    SM_OFF = 0,
    SM_PLAYING,
    SM_FINISHED,
} demo_audio_state_machine_t;

struct _demo_audio_handle {
    keyboard_handle_t           keyboard;
    display_handle_t            display;
    audio_handle_t              audio;

    demo_audio_state_machine_t  state;
};

int DemoAudio_Init(
    demo_audio_handle_t *phandle,
    keyboard_handle_t   keyboard,
    display_handle_t    display,
    audio_handle_t      audio)
{
    if (phandle == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;    
    }

    if (keyboard == NULL || display == NULL || audio == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (CreateHandle_Init((void **)phandle, sizeof(struct _demo_audio_handle)) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    (*phandle)->keyboard = keyboard;
    (*phandle)->display = display;
    (*phandle)->audio = audio;
    (*phandle)->state = SM_OFF;
    return 0;
} /* DemoAudio_Init */

void DemoAudio_Terminate(
    demo_audio_handle_t *phandle)
{
    CreateHandle_Terminate((void **)phandle);
} /* DemoAudio_Terminate */

execute_return_t DemoAudio_Execute(
    demo_audio_handle_t handle)
{
    uint32_t *bgra = NULL;
    key_event_t key_event = Keyboard_GetEvent(handle->keyboard);

    if (key_event == KEY_EVENT_ERROR) {
        printf("%s[%d]\n", __func__, __LINE__);
        return EXECUTE_RETURN_ERROR;
    }
    if (key_event == KEY_EVENT_BACK) {
        handle->state = SM_OFF;
        return EXECUTE_RETURN_FINISHED;
    }

    switch (handle->state) {
    case SM_OFF:
        /* Show speaker icon on display */
        ClearText(handle->display);

        if (Display_GetImageBufferBGRA(handle->display, &bgra) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return -1;
        }

        if (WIDTH_speaker_background  != Display_GetResolutionX(handle->display) ||
            HEIGHT_speaker_background != Display_GetResolutionY(handle->display)) {
            printf("%s[%d]\n", __func__, __LINE__);
            return -1;
        }

        YtoBGRA(data_speaker_background, WIDTH_speaker_background, HEIGHT_speaker_background, bgra);
        
        if (Display_PutImageBufferBGRA(handle->display) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return -1;
        }

        /* Show speaker icon on display */
        if (Audio_SetStream(handle->audio, (int16_t *)eagles_wav, eagles_wav_len) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
            return EXECUTE_RETURN_ERROR;
        }
        handle->state = SM_PLAYING;
    break;
    case SM_PLAYING:
        /* Show speaker icon on display */
        if (Audio_Play(handle->audio) != 0) {
            handle->state = SM_FINISHED;
        }
        break;
    default:
    case SM_FINISHED:
        break;
    }
    return EXECUTE_RETURN_OK;
} /* DemoAudio_Execute */

