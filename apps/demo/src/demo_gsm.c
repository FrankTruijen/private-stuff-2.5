/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "demo_gsm.h"
#include "create_handle.h"
#include "phone_background.h"

struct _demo_gsm_handle {
    keyboard_handle_t   keyboard;
    display_handle_t    display;
    gsm_handle_t        gsm;

    char                sphone_number[16];
    char                sphone_failure[16];
};

int DemoGsm_Init(
    demo_gsm_handle_t   *phandle,
    keyboard_handle_t   keyboard,
    display_handle_t    display,
    gsm_handle_t        gsm)
{
    demo_gsm_handle_t handle;

    if (keyboard == NULL || display == NULL || gsm == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (CreateHandle_Init((void **)phandle, sizeof(struct _demo_gsm_handle)) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    handle = *phandle;

    strcpy(handle->sphone_failure, "");
    strcpy(handle->sphone_number, "+");
    handle->keyboard = keyboard;
    handle->display = display;
    handle->gsm = gsm;
    return 0;
} /* DemoGsm_Init */

void DemoGsm_Terminate(
    demo_gsm_handle_t       *phandle)
{
    CreateHandle_Terminate((void **)phandle);
} /* DemoGsm_Terminate */

execute_return_t DemoGsm_Execute(
    demo_gsm_handle_t       handle)
{
    uint32_t *bgra = NULL;
    key_event_t key_event = Keyboard_GetEvent(handle->keyboard);

    switch (key_event) {
    case KEY_EVENT_ERROR:
        printf("%s[%d]\n", __func__, __LINE__);
        return EXECUTE_RETURN_ERROR;
    case KEY_EVENT_BACK:
        return EXECUTE_RETURN_FINISHED;
    case KEY_EVENT_0:
    case KEY_EVENT_1:
    case KEY_EVENT_2:
    case KEY_EVENT_3:
    case KEY_EVENT_4:
    case KEY_EVENT_5:
    case KEY_EVENT_6:
    case KEY_EVENT_7:
    case KEY_EVENT_8:
    case KEY_EVENT_9:
        if (strlen(handle->sphone_number) < 12) {
            char s[8];
            snprintf(s, sizeof(s), "%c", '0'+(int32_t)key_event-(int32_t)KEY_EVENT_0);
            strcat(handle->sphone_number, s);
        } 
        break;
    case KEY_EVENT_GREEN_CHECK:
        {
            send_sms_return_t ret = Gsm_SendSMS(handle->gsm, handle->sphone_number, "BVD3 test message");
            switch (ret) {
            case SEND_SMS_RETURN_OK:
                memset(handle->sphone_failure, 0, sizeof(handle->sphone_failure));
                break;
            case SEND_SMS_RETURN_FAILURE:
                snprintf(handle->sphone_failure, sizeof(handle->sphone_failure)-1, "GSM Failure");
                break;
            case SEND_SMS_RETURN_ERROR:
                break;
            }
        }
        break;
    case KEY_EVENT_RED_FAIL:
        if (strlen(handle->sphone_number) > 1) {
            handle->sphone_number[strlen(handle->sphone_number)-1] = '\0';
        } 
        break;
    default:
        break;
    }

    ClearText(handle->display);
    InsertText(handle->display, "Phone number",         0,  260, 0, 0, 0, FONT_SIZE_16);
    InsertText(handle->display, handle->sphone_number,  0,  276, 0, 0, 0, FONT_SIZE_16);
    InsertText(handle->display, handle->sphone_failure, 32, 296, 0, 0, 0, FONT_SIZE_16);

    /* Show finger icon on display */
    if (Display_GetImageBufferBGRA(handle->display, &bgra) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (WIDTH_phone_background  != Display_GetResolutionX(handle->display) ||
        HEIGHT_phone_background != Display_GetResolutionY(handle->display)) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    YtoBGRA(data_phone_background, WIDTH_phone_background, HEIGHT_phone_background, bgra);
    
    if (Display_PutImageBufferBGRA(handle->display) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    return EXECUTE_RETURN_OK;
} /* DemoGsm_Execute */