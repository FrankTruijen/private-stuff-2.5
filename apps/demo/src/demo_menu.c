/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "demo_menu.h"
#include "create_handle.h"
#include "genkey_background.h"

typedef enum {
    DEMO_AUDIO,
    DEMO_BARCODE,
    DEMO_FINGER_PRINT_SCANNER,
    DEMO_GPS,
    DEMO_GSM,
    DEMO_LED,
    DEMO_MENU
} demo_mode_t;

struct _demo_menu_handle {
    keyboard_handle_t                   keyboard;
    display_handle_t                    display;
    power_handle_t                      power;
    demo_barcode_handle_t               demo_barcode;
    demo_finger_print_scanner_handle_t  demo_finger_print_scanner;
    demo_audio_handle_t                 demo_audio;
    demo_gsm_handle_t                   demo_gsm;
    demo_gps_handle_t                   demo_gps;
    demo_led_handle_t                   demo_led;

    demo_mode_t                         demo_mode;
    
    uint8_t                             *u, *v;
};

int DemoMenu_Init(
    demo_menu_handle_t                  *phandle,
    keyboard_handle_t                   keyboard,
    display_handle_t                    display,
    power_handle_t                      power,
    demo_barcode_handle_t               demo_barcode,
    demo_finger_print_scanner_handle_t  demo_finger_print_scanner,
    demo_audio_handle_t                 demo_audio,
    demo_gsm_handle_t                   demo_gsm,
    demo_gps_handle_t                   demo_gps,
    demo_led_handle_t                   demo_led)
{
    demo_menu_handle_t  handle = NULL;

    if (keyboard == NULL || display == NULL || demo_barcode == NULL || demo_finger_print_scanner == NULL || demo_audio == NULL || demo_led == NULL) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }

    if (CreateHandle_Init((void **)phandle, sizeof(struct _demo_menu_handle)) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return -1;
    }
    handle = *phandle;

    handle->keyboard = keyboard;
    handle->display = display;
    handle->power = power;
    handle->demo_barcode = demo_barcode;
    handle->demo_finger_print_scanner = demo_finger_print_scanner;
    handle->demo_audio = demo_audio;
    handle->demo_gsm = demo_gsm;
    handle->demo_gps = demo_gps;
    handle->demo_led = demo_led;

    handle->demo_mode = DEMO_MENU;
    return 0;
} /* DemoMenu_Init */

void DemoMenu_Terminate(
    demo_menu_handle_t          *phandle)
{
    CreateHandle_Terminate((void **)phandle);
} /* DemoMenu_Terminate */

execute_return_t DemoMenu_Execute(
    demo_menu_handle_t          handle)
{
    uint32_t *bgra = NULL;
    execute_return_t ret = EXECUTE_RETURN_FINISHED;

    while (Power_ButtonPushed(handle->power) == 0) {
        /* Execute menu item which is currently active */
        if (handle->demo_mode == DEMO_MENU) {
            key_event_t key_event = Keyboard_GetEvent(handle->keyboard);

            switch(key_event) {
            case KEY_EVENT_1: 
                handle->demo_mode = DEMO_AUDIO;
                break;
            case KEY_EVENT_2: 
                handle->demo_mode = DEMO_BARCODE;
                break;
            case KEY_EVENT_3: 
                handle->demo_mode = DEMO_FINGER_PRINT_SCANNER;
                break;
            case KEY_EVENT_4:
                handle->demo_mode = DEMO_LED;
                break;
            case KEY_EVENT_5: 
                handle->demo_mode = DEMO_GPS;
                break;
            case KEY_EVENT_6:
                handle->demo_mode = DEMO_GSM;
                break;
            case KEY_EVENT_ERROR:
                printf("%s[%d]\n", __func__, __LINE__);
            default:
                break;
            }
        } 

        switch (handle->demo_mode) {
        case DEMO_AUDIO:
            ret = DemoAudio_Execute(handle->demo_audio);
            break;
        case DEMO_BARCODE:
            ret = DemoBarcode_Execute(handle->demo_barcode);
            break;
        case DEMO_FINGER_PRINT_SCANNER:
            ret = DemoFingerPrintScanner_Execute(handle->demo_finger_print_scanner);
            break;
        case DEMO_LED:
            ret = DemoLed_Execute(handle->demo_led);
            break;
        case DEMO_GPS:
            if (handle->demo_gps != NULL) {
                ret = DemoGps_Execute(handle->demo_gps);
            }
            break;
        case DEMO_GSM:
            if (handle->demo_gsm != NULL) {
                ret = DemoGsm_Execute(handle->demo_gsm);
            }
            break;
        case DEMO_MENU:
            ClearText(handle->display);
            InsertText(handle->display, "1.Audio",        0, 120, 0, 0, 0, FONT_SIZE_16);
            InsertText(handle->display, "2.Barcode",      0, 140, 0, 0, 0, FONT_SIZE_16);
            InsertText(handle->display, "3.Finger Print", 0, 160, 0, 0, 0, FONT_SIZE_16);
            InsertText(handle->display, "4.LED",          0, 180, 0, 0, 0, FONT_SIZE_16);
            if (handle->demo_gps != NULL) {
                InsertText(handle->display, "5.GPS",          0, 200, 0, 0, 0, FONT_SIZE_16);
            }
            if (handle->demo_gsm != NULL) {
                InsertText(handle->display, "6.GSM",          0, 220, 0, 0, 0, FONT_SIZE_16);
            }

            if (Display_GetImageBufferBGRA(handle->display, &bgra) != 0) {
                printf("%s[%d]\n", __func__, __LINE__);
                return -1;
            }

            if (WIDTH_genkey_background  != Display_GetResolutionX(handle->display) ||
                HEIGHT_genkey_background != Display_GetResolutionY(handle->display)) {
                printf("%s[%d]\n", __func__, __LINE__);
                return -1;
            }

            YtoBGRA(data_genkey_background, WIDTH_genkey_background, HEIGHT_genkey_background, bgra);
            
            if (Display_PutImageBufferBGRA(handle->display) != 0) {
                printf("%s[%d]\n", __func__, __LINE__);
                return -1;
            }
            break;
        }

        if (ret == EXECUTE_RETURN_ERROR) {
            /* Break out of 'while' loop in case of failure */
            break;
        }
        if (ret == EXECUTE_RETURN_FINISHED) {
            handle->demo_mode = DEMO_MENU;
        }
        usleep(10000);
    }
    return EXECUTE_RETURN_OK;
} /* DemoMenu_Execute */
