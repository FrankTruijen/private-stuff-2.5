/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include "demo_bluetooth.h"

int main()
{
    demo_bluetooth_handle_t handle = NULL;

    if (DemoBluetooth_Init(&handle, NULL, NULL, NULL) != 0) {
        printf("Failure: DemoBarcode_Init\n");
    }
    DemoBluetooth_Execute(handle);
    DemoBluetooth_Terminate(&handle);
    return 0;
} /* main */
