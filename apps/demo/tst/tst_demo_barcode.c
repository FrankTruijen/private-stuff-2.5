/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <unistd.h>

#include "demo_barcode.h"

int main()
{
    demo_barcode_handle_t   handle = NULL;
    led_handle_t            led_laser_0;
    led_handle_t            led_laser_1;
    pwm_handle_t            flashlight;
    keyboard_handle_t       keyboard;
    display_handle_t        display;
    camera_handle_t         camera;

    /* Laser LED 0 init */
    if (Led_Init(&led_laser_0, "/sys/class/leds/laser:mono:0/brightness") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Laser LED 1 init */
    if (Led_Init(&led_laser_1, "/sys/class/leds/laser:mono:1/brightness") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* PWM init */
    if (Pwm_Init(&flashlight, 1000000, "/sys/class/pwm/pwmchip0/") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Keyboard init */
    if (Keyboard_Init(&keyboard, "/dev/input/event0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display init */
    if (Display_Init(&display, "/dev/fb0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Camera Init */
    if (Camera_Init(&camera, "/dev/video0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Barcode Init */
    if (DemoBarcode_Init(&handle, keyboard, display, led_laser_0, led_laser_1, flashlight, camera) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    while (DemoBarcode_Execute(handle) == EXECUTE_RETURN_OK) {
    }
    DemoBarcode_Terminate(&handle);

END:
    Led_Terminate(&led_laser_0);
    Led_Terminate(&led_laser_1);
    Pwm_Terminate(&flashlight);
    Keyboard_Terminate(&keyboard);
    Display_Terminate(&display);
    Camera_Terminate(&camera);
    return 0;
} /* main */

