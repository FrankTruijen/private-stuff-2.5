/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include "finger_print_scanner.h"

static void R8G8B8toBMP(int width, int height, unsigned char *r, unsigned char *g, unsigned char *b, unsigned int nr)
{
  FILE *f;
  char sname[128];
  unsigned char *rgb = NULL;
  int filesize = 54 + 3*width*height;

  unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
  unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
  unsigned char bmppad[3] = {0,0,0};

  unsigned int i = 0;

  rgb =(unsigned char *) malloc(3*width*height*sizeof(unsigned char));
  memset(rgb, 0, 3*width*height);

  for (int i=0; i<width; i++) {
    for (int j=0; j<height; j++) {
      unsigned int x=i;
      unsigned int y=(height-1)-j;

      rgb[(x+y*width)*3+2] = r[j*width+i];
      rgb[(x+y*width)*3+1] = g[j*width+i];
      rgb[(x+y*width)*3+0] = b[j*width+i];
    }
  }

  bmpfileheader[ 2] = (unsigned char)(filesize    );
  bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
  bmpfileheader[ 4] = (unsigned char)(filesize>>16);
  bmpfileheader[ 5] = (unsigned char)(filesize>>24);

  bmpinfoheader[ 4] = (unsigned char)(width    );
  bmpinfoheader[ 5] = (unsigned char)(width>> 8);
  bmpinfoheader[ 6] = (unsigned char)(width>>16);
  bmpinfoheader[ 7] = (unsigned char)(width>>24);
  bmpinfoheader[ 8] = (unsigned char)(height    );
  bmpinfoheader[ 9] = (unsigned char)(height>> 8);
  bmpinfoheader[10] = (unsigned char)(height>>16);
  bmpinfoheader[11] = (unsigned char)(height>>24);

  sprintf(sname, "finger_print%04d.bmp", nr);
  f = fopen(sname,"wb");
  fwrite(bmpfileheader,1,14,f);
  fwrite(bmpinfoheader,1,40,f);
  for(i=0; i<height; i++) {
    fwrite(rgb+(width*(height-i-1)*3),3,width,f);
    fwrite(bmppad,1,(4-(width*3)%4)%4,f);
  }
  fclose(f);
} /* R8G8B8toBMP */

int main()
{   uint32_t width = 0, height = 0, i = 0, j = 0;
    finger_print_scanner_handle_t handle = NULL;
    uint8_t *memory = NULL;

    if (FingerPrintScanner_Init(&handle) != 0) {
        printf("FingerPrintScanner_Init failure!\n");
        return -1;
    }

    width  = FingerPrintScanner_GetResolutionX(handle);
    height = FingerPrintScanner_GetResolutionY(handle);

    if ((memory = malloc(width * height)) == NULL) {
        printf("Memory allocation failure!\n");
        FingerPrintScanner_Terminate(&handle);
        return -1;
    }

    for (j = 0; j < 4; j++) {
        printf("Present finger on scanner.\n");
        
        /* Wait 30 seconds to present finger */
        for (i = 0; i < 1000; i++) {
            if (FingerPrintScanner_IsFingerPresent(handle)) {
                /* Get finger print */
                if (FingerPrintScanner_GetImageY(handle, memory) != 0) {
                    printf("Failure getting finger print!\n");
                    break;
                }

                printf("Finger scanned correct\n");
                /* Store finger print in bitmap */
                R8G8B8toBMP(width, height, memory, memory, memory, j);
                
                /* Wait to remove finger before next scan */
                sleep(5);
                break;
            }
            usleep(30000);
        }
    }
    FingerPrintScanner_Terminate(&handle);
    return 0;
} /* main */