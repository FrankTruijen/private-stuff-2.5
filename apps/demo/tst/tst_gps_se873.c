/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "gps_se873.h"

int main()
{   int i = 0;
    gps_se873_handle_t handle = NULL;
    gps_se873_location_t loc;

    if (GpsSe873_Init(&handle, "/dev/ttymxc2") != 0) {
        printf("Failure: Gps_Init\n");
    }

    memset(&loc, 0, sizeof(loc));
    for (i = 0; i < 500; i++) {
        if (GpsSe873_GetLocation(handle, &loc) == 0) {
            printf( "Long: %s°%s\" %s    Lat: %s°%s\" %s\n",
                    loc.longitude.degree, 
                    loc.longitude.minutes, 
                    loc.longitude.hemisphere,
                    loc.latitude.degree, 
                    loc.latitude.minutes, 
                    loc.latitude.hemisphere);
        }
        usleep(100000);
    }
    GpsSe873_Terminate(&handle);
    return 0;
} /* main */