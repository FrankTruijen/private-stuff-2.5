/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "led.h"

int main()
{   uint32_t brightness = 0;
    led_handle_t handle = NULL;
    led_rgb_handle_t handle_rgb = NULL;

    
    /* Monochroom LED */
    if (Led_Init(&handle, "/sys/class/leds/display:mono:hour_glass/brightness") != 0) {
        printf("Failure: Led_Init\n");
        return -1;
    }

    for (brightness = 0; brightness < 256; brightness++) {
        Led_Value(handle, brightness);
        usleep(30000);
    }
    Led_Value(handle, 0);
    
    Led_Terminate(&handle);

    /* Test Colour LED */
    if (LedRgb_Init(&handle_rgb, 
                    "/sys/class/leds/status:red:left_bottom/brightness",
                    "/sys/class/leds/status:green:left_bottom/brightness",
                    "/sys/class/leds/status:blue:left_bottom/brightness") != 0) {

        printf("Failure: LedRgb_Init\n");
        return -1;
    }
    
    for (brightness = 0; brightness < 256; brightness++) {
        LedRgb_Value(handle_rgb, brightness, 0, 0);
        usleep(30000);
    }
    for (brightness = 0; brightness < 256; brightness++) {
        LedRgb_Value(handle_rgb, 0, brightness, 0);
        usleep(30000);
    }
    for (brightness = 0; brightness < 256; brightness++) {
        LedRgb_Value(handle_rgb, 0, 0, brightness);
        usleep(30000);
    }
    LedRgb_Value(handle_rgb, 0, 0, 0);
    
    LedRgb_Terminate(&handle_rgb);
    return 0;
} /* main */