/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "camera.h"

#define IMAGE_X         640
#define IMAGE_Y         480
#define IMAGE_HIGH_X    640
#define IMAGE_HIGH_Y    480

static void R8G8B8toBMP(int width, int height, unsigned char *r, unsigned char *g, unsigned char *b, unsigned int nr)
{
  FILE *f;
  char sname[128];
  unsigned char *rgb = NULL;
  int filesize = 54 + 3*width*height;

  unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
  unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
  unsigned char bmppad[3] = {0,0,0};

  unsigned int i = 0;

  rgb =(unsigned char *) malloc(3*width*height*sizeof(unsigned char));
  memset(rgb, 0, 3*width*height);

  for (int i=0; i<width; i++) {
    for (int j=0; j<height; j++) {
      unsigned int x=i;
      unsigned int y=(height-1)-j;

      rgb[(x+y*width)*3+2] = r[j*width+i];
      rgb[(x+y*width)*3+1] = g[j*width+i];
      rgb[(x+y*width)*3+0] = b[j*width+i];
    }
  }

  bmpfileheader[ 2] = (unsigned char)(filesize    );
  bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
  bmpfileheader[ 4] = (unsigned char)(filesize>>16);
  bmpfileheader[ 5] = (unsigned char)(filesize>>24);

  bmpinfoheader[ 4] = (unsigned char)(width    );
  bmpinfoheader[ 5] = (unsigned char)(width>> 8);
  bmpinfoheader[ 6] = (unsigned char)(width>>16);
  bmpinfoheader[ 7] = (unsigned char)(width>>24);
  bmpinfoheader[ 8] = (unsigned char)(height    );
  bmpinfoheader[ 9] = (unsigned char)(height>> 8);
  bmpinfoheader[10] = (unsigned char)(height>>16);
  bmpinfoheader[11] = (unsigned char)(height>>24);

  sprintf(sname, "camera%04d.bmp", nr);
  f = fopen(sname,"wb");
  fwrite(bmpfileheader,1,14,f);
  fwrite(bmpinfoheader,1,40,f);
  for(i=0; i<height; i++) {
    fwrite(rgb+(width*(height-i-1)*3),3,width,f);
    fwrite(bmppad,1,(4-(width*3)%4)%4,f);
  }
  free(rgb);
  fclose(f);
} /* R8G8B8toBMP */


int main()
{
    uint8_t  *y;

    uint32_t px =0, py = 0, i = 0;
    uint32_t width = 0, height = 0;

    camera_handle_t handle = NULL;

    if ((y = malloc(2592 * 1944)) == NULL) {
        printf("Failure: malloc\n");
        return -1;
    }

    if (Camera_Init(&handle, "/dev/video0") != 0) {
        printf("Failure: Camera_Init\n");
        return -1;
    }
    if (Camera_Start(handle, MODE_VGA_640_480) != 0) {
        printf("Failure: Camera_Start\n");
        return -1;
    }

    width  = Camera_GetResolutionX(handle);
    height = Camera_GetResolutionY(handle);
    for (i = 0; i < 7; i++) {
        uint8_t *yuyv;
        if (Camera_GetImageBufferYUYV(handle, (uint32_t**)&yuyv) == CAMERA_ERROR) {
            printf("Failure: Camera_GetImageYUV422\n");
            break;
        }

        /* Convert combined buffers to separated buffers */
        for (py = 0; py < height; py++) {
            for (px = 0; px < width; px++) {
                uint32_t idx_yuyv = py*width*2 + px*2;
                uint32_t idx_y  = py*width + px;

                y[idx_y] = yuyv[idx_yuyv+0];
            }
        }

        R8G8B8toBMP(width, height, y, y, y, i);
        if (Camera_PutImageBufferYUYV(handle) == CAMERA_ERROR) {
            printf("Failure: Camera_GetImageYUV422\n");
            break;
        }
    }
    if (Camera_Stop(handle) != 0) {
        printf("Failure: Camera_Stop\n");
        return -1;
    }











    if (Camera_Start(handle, MODE_QSXGA_2592_1944) != 0) {
        printf("Failure: Camera_Start\n");
        return -1;
    }

    width  = Camera_GetResolutionX(handle);
    height = Camera_GetResolutionY(handle);
    for (; i < 11; i++) {
        uint8_t *yuyv;
        if (Camera_GetImageBufferYUYV(handle, (uint32_t**)&yuyv) == CAMERA_ERROR) {
            printf("Failure: Camera_GetImageYUV422\n");
            break;
        }

        /* Convert combined buffers to separated buffers */
        for (py = 0; py < height; py++) {
            for (px = 0; px < width; px++) {
                uint32_t idx_yuyv = py*width*2 + px*2;
                uint32_t idx_y  = py*width + px;

                y[idx_y] = yuyv[idx_yuyv+0];
            }
        }

        R8G8B8toBMP(width, height, y, y, y, i);
        if (Camera_PutImageBufferYUYV(handle) == CAMERA_ERROR) {
            printf("Failure: Camera_GetImageYUV422\n");
            break;
        }
    }
    if (Camera_Stop(handle) != 0) {
        printf("Failure: Camera_Stop\n");
        return -1;
    }











    if (Camera_Start(handle, MODE_VGA_640_480) != 0) {
        printf("Failure: Camera_Start\n");
        return -1;
    }

    width  = Camera_GetResolutionX(handle);
    height = Camera_GetResolutionY(handle);
    for (; i < 17; i++) {
        uint8_t *yuyv;
        if (Camera_GetImageBufferYUYV(handle, (uint32_t**)&yuyv) == CAMERA_ERROR) {
            printf("Failure: Camera_GetImageYUV422\n");
            break;
        }

        /* Convert combined buffers to separated buffers */
        for (py = 0; py < height; py++) {
            for (px = 0; px < width; px++) {
                uint32_t idx_yuyv = py*width*2 + px*2;
                uint32_t idx_y  = py*width + px;

                y[idx_y] = yuyv[idx_yuyv+0];
            }
        }

        R8G8B8toBMP(width, height, y, y, y, i);
        if (Camera_PutImageBufferYUYV(handle) == CAMERA_ERROR) {
            printf("Failure: Camera_GetImageYUV422\n");
            break;
        }
    }
    if (Camera_Stop(handle) != 0) {
        printf("Failure: Camera_Stop\n");
        return -1;
    }

    Camera_Terminate(&handle);
    free(y);
    return 0;
} /* main */
