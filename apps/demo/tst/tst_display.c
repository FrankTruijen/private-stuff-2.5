/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "display.h"

#define RESX                240
#define RESY                320

typedef struct {
    uint8_t y, u, v;
    char scolor[128];
} test_color_yuv_t;

typedef struct {
    uint8_t r, g, b;
    char scolor[128];
} test_color_rgb_t;

int CheckResolution()
{
    display_handle_t handle = NULL;

    if (Display_Init(&handle, "/dev/fb0") != 0) {
        printf("Failure: Display_Init\n");
    }

    if (Display_GetResolutionX(handle) != RESX) {
        printf("Failure: Display_GetResolutionX\n");
        Display_Terminate(&handle);
        return -1;
    }

    if (Display_GetResolutionY(handle) != RESY) {
        printf("Failure: Display_GetResolutionY\n");
        Display_Terminate(&handle);
        return -1;
    }
    Display_Terminate(&handle);
    return 0;
} /* CheckResolution */

int Block()
{
    display_handle_t handle = NULL;
    uint32_t px = 0, py = 0, ox = 0, oy = 0, framenr = 0;

    if (Display_Init(&handle, "/dev/fb0") != 0) {
        printf("Failure: Display_Init\n");
    }
    
    for (framenr = 0; framenr < 500; framenr++, ox++, oy++) {
        uint32_t *bgra = NULL;
        if (Display_GetImageBufferBGRA(handle, &bgra) != 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            Display_Terminate(&handle);
            return -1;
        }
        /* Clear buffer to black */
        memset(bgra, 0, RESY * RESX * sizeof(*bgra));

        if (ox >= RESX) ox-= RESX;
        if (oy >= RESY) oy-= RESY;

        /* Moving square */
        for (py = 0; py < RESY/10; py++) {
            for (px = 0; px < RESX/10; px++) {
                uint32_t sx = ((px+ox) < RESX ? (px+ox) : (px+ox-RESX));
                uint32_t sy = ((py+oy) < RESY ? (py+oy) : (py+oy-RESY));

                bgra[sy*RESX+sx] = (0xFF << 0) + (0xFF << 8) + (0xFF << 16);
            }
        }

        if (Display_PutImageBufferBGRA(handle) != 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            Display_Terminate(&handle);
            return -1;
        }
        usleep(10000);
    }
    Display_Terminate(&handle);
    return 0;
} /* Block */

int ColorTest()
{
    display_handle_t handle = NULL;
    uint32_t i = 0, px = 0, py = 0;
    test_color_rgb_t tst_clr[] = {{255, 255, 255, "white"}, {0, 0, 0, "black"}, {0, 255, 0, "green"}, {155, 0, 0, "red"}, {0, 0, 255, "blue"}};

    if (Display_Init(&handle, "/dev/fb0") != 0) {
        printf("Failure: Display_Init\n");
        return -1;
    }

    /* YUV test without stride */
    for (i = 0; i < sizeof(tst_clr)/sizeof(tst_clr[0]); i++) {
        uint32_t *bgra = NULL;
        if (Display_GetImageBufferBGRA(handle, &bgra) != 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            Display_Terminate(&handle);
            return -1;
        }

        /* Moving square */
        for (py = 0; py < RESY; py++) {
            for (px = 0; px < RESX; px++) {
                bgra[py*RESX+px] = (tst_clr[i].r << 16) + (tst_clr[i].g << 8) + (tst_clr[i].b << 0);
            }
        }

        printf("Show color: %s\n", tst_clr[i].scolor);
        if (Display_PutImageBufferBGRA(handle) != 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            Display_Terminate(&handle);
            return -1;
        }
        sleep(6);
    }
    Display_Terminate(&handle);
    return 0;
} /* ColorTest */

int ChangingTextTest()
{
    display_handle_t handle = NULL;
    uint8_t *rgb = NULL;
    uint32_t i = 0;

    if (Display_Init(&handle, "/dev/fb0") != 0) {
        printf("Failure: Display_Init\n");
        return -1;
    }

    if ((rgb = malloc(RESY * RESX * 3)) == NULL) {
        printf("Failure: malloc\n");
        return -1;
    }

    for (i = 0; i < RESY; i++) {
        uint32_t *bgra = NULL;

        /* Create text */
        ClearText(handle);
        InsertText(handle, "Line 1", i%RESX, i,    (i%3)==0 ? 255 : 0, (i%3)==1 ? 255 : 0, (i%3)==2 ? 255 : 0, FONT_SIZE_16);
        InsertText(handle, "Line 2", 16,     i+16, (i%3)==0 ? 255 : 0, (i%3)==1 ? 255 : 0, (i%3)==2 ? 255 : 0, FONT_SIZE_16);

        if (Display_GetImageBufferBGRA(handle, &bgra) != 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            Display_Terminate(&handle);
            return -1;
        }
        
        memset(bgra, 0, RESY * RESX * sizeof(*bgra));

        if (Display_PutImageBufferBGRA(handle) != 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            Display_Terminate(&handle);
            return -1;
        }
        usleep(100000);
    }
    Display_Terminate(&handle);
    return 0;
} /* ChangingTextTest */

int ChangingTextBackgroundTest()
{
    display_handle_t handle = NULL;
    uint32_t i = 0, px = 0, py = 0;

    if (Display_Init(&handle, "/dev/fb0") != 0) {
        printf("Failure: Display_Init\n");
        return -1;
    }

    InsertText(handle, "Line 1", 0,  0, 0, 255, 0, FONT_SIZE_16);
    InsertText(handle, "Line 2", 0, 16, 0, 255, 0, FONT_SIZE_16);

    for (i = 0; i < 10; i++) {
        uint32_t *bgra = NULL;

        if (Display_GetImageBufferBGRA(handle, &bgra) != 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            Display_Terminate(&handle);
            return -1;
        }

        for (py = 0; py < RESY; py++) {
            uint8_t b = (0xFF)/16 * i, g = (0xFF)/16 * i, r = (0xFF)/16 * i;
            for (px = 0; px < RESX; px++) {
                bgra[py*RESX+px] = (b << 0) + (g << 8) + (r << 16);
            }
        }

        if (Display_PutImageBufferBGRA(handle) != 0) {
            printf("Failure function %s line %d\n", __func__, __LINE__);
            Display_Terminate(&handle);
            return -1;
        }
        usleep(500000);
    }
    Display_Terminate(&handle);
    return 0;
} /* ChangingTextBackgroundTest */

int main()
{
    printf("Start resolution check\n");
    CheckResolution();
    printf("Finished resolution check\n");

    printf("Start color check\n");
    ColorTest();
    printf("Finished color check\n");

    printf("Start motion check\n");
    Block();
    printf("Finished motion check\n");

    printf("start changing text check\n");
    ChangingTextTest();
    printf("finished changing text check\n");

    printf("start changing background text check\n");
    ChangingTextBackgroundTest();
    printf("finished changing background text check\n");
    return 0;
} /* main */
