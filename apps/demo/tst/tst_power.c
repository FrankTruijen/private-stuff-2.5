/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "power.h"

int main()
{
    power_handle_t handle = NULL;

    if (Power_Init(&handle, "/sys/class/gpio/") != 0) {
        printf("Failure: Power_Init\n");
        return -1;
    }

    printf("Please push power button\n");
    while (Power_ButtonPushed(handle) == 0) {
        usleep(10000);
    }

    printf("Please release power button\n");
    while (Power_ButtonPushed(handle) == 1) {
        usleep(10000);
    }

    printf("Please push power button and device will terminate\n");
    while (Power_ButtonPushed(handle) == 0) {
        usleep(10000);
    }
    Power_Terminate(&handle, 1);
    return 0;
} /* main */

