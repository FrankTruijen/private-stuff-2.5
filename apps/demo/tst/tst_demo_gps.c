/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <unistd.h>

#include "demo_gps.h"

int main()
{
    demo_gps_handle_t handle = NULL;
    keyboard_handle_t   keyboard;
    display_handle_t    display;
    gps_se873_handle_t  gps_se873;

    /* Keyboard init */
    if (Keyboard_Init(&keyboard, "/dev/input/event0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display init */
    if (Display_Init(&display, "/dev/fb0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* GPS init */
    if (GpsSe873_Init(&gps_se873, "/dev/ttymxc2") != 0) {
        printf("Failure: Gps_Init\n");
        goto END;
    }
    
    if (DemoGps_Init(&handle, keyboard, display, gps_se873) != 0) {
        printf("Failure: DemoBarcode_Init\n");
    }

    while (DemoGps_Execute(handle) == EXECUTE_RETURN_OK) {
        usleep(10000);
    }
    DemoGps_Terminate(&handle);

END:
    GpsSe873_Terminate(&gps_se873);   
    Display_Terminate(&display);
    Keyboard_Terminate(&keyboard);
    return 0;
} /* main */
