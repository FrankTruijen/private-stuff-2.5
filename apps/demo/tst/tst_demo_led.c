/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <unistd.h>

#include "demo_led.h"

#define DIR_BASE            "/sys/class/leds/"
#define BRIGHT              "/brightness"

int main()
{
    demo_led_handle_t   handle = NULL;
    led_rgb_handle_t    led_status_rb, led_status_rt, led_status_lb, led_status_lt;
    led_handle_t        led_foil_left_thumb, led_foil_left_index_finger, led_foil_left_middle_finger, led_foil_left_ring_finger, led_foil_left_pinky;
    led_handle_t        led_foil_right_thumb, led_foil_right_index_finger, led_foil_right_middle_finger, led_foil_right_ring_finger, led_foil_right_pinky;    
    led_handle_t        led_foil_arrow, led_foil_error, led_foil_ok;
    led_handle_t        led_laser_0, led_laser_1;
    led_handle_t        led_display_ok, led_display_active, led_display_hourglass;
    keyboard_handle_t   keyboard;
    display_handle_t    display;

    /* Init status LED's */
    if (LedRgb_Init(&led_status_rb, DIR_BASE"status:red:right_bottom"BRIGHT,DIR_BASE"status:green:right_bottom"BRIGHT,  DIR_BASE"status:blue:right_bottom"BRIGHT) != 0  ||
        LedRgb_Init(&led_status_rt, DIR_BASE"status:red:right_top"BRIGHT,   DIR_BASE"status:green:right_top"BRIGHT,     DIR_BASE"status:blue:right_top"BRIGHT) != 0     ||
        LedRgb_Init(&led_status_lb, DIR_BASE"status:red:left_bottom"BRIGHT, DIR_BASE"status:green:left_bottom"BRIGHT,   DIR_BASE"status:blue:left_bottom"BRIGHT) != 0   ||        
        LedRgb_Init(&led_status_lt, DIR_BASE"status:red:left_top"BRIGHT,    DIR_BASE"status:green:left_top"BRIGHT,      DIR_BASE"status:blue:left_top"BRIGHT) != 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Foil LED's init */
    if (Led_Init(&led_foil_left_thumb,           DIR_BASE"foil:mono:left_thumb"BRIGHT) != 0          ||
        Led_Init(&led_foil_left_index_finger,    DIR_BASE"foil:mono:left_index_finger"BRIGHT) != 0   ||
        Led_Init(&led_foil_left_middle_finger,   DIR_BASE"foil:mono:left_middle_finger"BRIGHT) != 0  ||
        Led_Init(&led_foil_left_ring_finger,     DIR_BASE"foil:mono:left_ring_finger"BRIGHT) != 0    ||
        Led_Init(&led_foil_left_pinky,           DIR_BASE"foil:mono:left_pinky"BRIGHT) != 0          ||
        Led_Init(&led_foil_right_thumb,          DIR_BASE"foil:mono:right_thumb"BRIGHT) != 0         ||
        Led_Init(&led_foil_right_index_finger,   DIR_BASE"foil:mono:right_index_finger"BRIGHT) != 0  ||
        Led_Init(&led_foil_right_middle_finger,  DIR_BASE"foil:mono:right_middle_finger"BRIGHT) != 0 ||
        Led_Init(&led_foil_right_ring_finger,    DIR_BASE"foil:mono:right_ring_finger"BRIGHT) != 0   ||
        Led_Init(&led_foil_right_pinky,          DIR_BASE"foil:mono:right_pinky"BRIGHT) != 0         ||
        Led_Init(&led_foil_arrow,                DIR_BASE"foil:mono:arrow"BRIGHT) != 0               ||
        Led_Init(&led_foil_error,                DIR_BASE"foil:mono:finger_error"BRIGHT) != 0        ||
        Led_Init(&led_foil_ok,                   DIR_BASE"foil:mono:finger_ok"BRIGHT) != 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display LED's init */
    if (Led_Init(&led_display_hourglass, DIR_BASE"display:mono:hour_glass"BRIGHT) != 0  ||
        Led_Init(&led_display_active,    DIR_BASE"display:mono:busy"BRIGHT) != 0        ||
        Led_Init(&led_display_ok,        DIR_BASE"display:mono:ok"BRIGHT) != 0) {
            
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }


    /* Laser LED's init */
    if (Led_Init(&led_laser_0, DIR_BASE"laser:mono:0"BRIGHT) != 0   ||
        Led_Init(&led_laser_1, DIR_BASE"laser:mono:1"BRIGHT) != 0) {
            
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Keyboard init */
    if (Keyboard_Init(&keyboard, "/dev/input/event0") != 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display init */
    if (Display_Init(&display, "/dev/fb0") != 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Led Demo Init */
    if (DemoLed_Init(&handle, 
                     keyboard,
                     display,
                     led_status_rb,
                     led_status_rt,
                     led_status_lb,
                     led_status_lt,
                     led_laser_0,
                     led_laser_1,
                     led_display_ok,
                     led_display_active,
                     led_display_hourglass,
                     led_foil_left_thumb,
                     led_foil_left_index_finger,
                     led_foil_left_middle_finger,
                     led_foil_left_ring_finger,
                     led_foil_left_pinky,
                     led_foil_right_thumb,
                     led_foil_right_index_finger,
                     led_foil_right_middle_finger,
                     led_foil_right_ring_finger,
                     led_foil_right_pinky,
                     led_foil_arrow,
                     led_foil_error,
                     led_foil_ok) != 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    while (DemoLed_Execute(handle) == EXECUTE_RETURN_OK) {
        usleep(10000);
    }
    DemoLed_Terminate(&handle);

END:
    LedRgb_Terminate(&led_status_rb);
    LedRgb_Terminate(&led_status_rt);
    LedRgb_Terminate(&led_status_lb);
    LedRgb_Terminate(&led_status_lt);
    Led_Terminate(&led_foil_left_thumb);
    Led_Terminate(&led_foil_left_index_finger);
    Led_Terminate(&led_foil_left_middle_finger);
    Led_Terminate(&led_foil_left_ring_finger);
    Led_Terminate(&led_foil_left_pinky);
    Led_Terminate(&led_foil_right_thumb);
    Led_Terminate(&led_foil_right_index_finger);
    Led_Terminate(&led_foil_right_middle_finger);
    Led_Terminate(&led_foil_right_ring_finger);
    Led_Terminate(&led_foil_right_pinky);
    Led_Terminate(&led_foil_arrow);
    Led_Terminate(&led_foil_error);
    Led_Terminate(&led_foil_ok);
    Led_Terminate(&led_display_hourglass);
    Led_Terminate(&led_display_active);
    Led_Terminate(&led_display_ok);
    Led_Terminate(&led_laser_0);
    Led_Terminate(&led_laser_1);
    Keyboard_Terminate(&keyboard);
    Display_Terminate(&display);
    return 0;
} /* main */




