/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <unistd.h>

#include "demo_gsm.h"

int main()
{
    demo_gsm_handle_t   handle = NULL;
    keyboard_handle_t   keyboard;
    display_handle_t    display;
    gsm_handle_t        gsm;

    /* Keyboard init */
    if (Keyboard_Init(&keyboard, "/dev/input/event0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display init */
    if (Display_Init(&display, "/dev/fb0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* GSM init */
    if (Gsm_Init(&gsm, "/dev/ttyACM0") != 0) {
        printf("Failure: Gsm_Init\n");
        goto END;
    }

    if (DemoGsm_Init(&handle, keyboard, display, gsm) != 0) {
        printf("Failure: DemoGsm_Init\n");
    }

    while (DemoGsm_Execute(handle) == EXECUTE_RETURN_OK) {
        usleep(10000);
    }
    DemoGsm_Terminate(&handle);

END:
    Gsm_Terminate(&gsm);   
    Display_Terminate(&display);
    Keyboard_Terminate(&keyboard);
    return 0;
} /* main */
