/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "pwm.h"

int main()
{   int32_t value = 0;
    pwm_handle_t handle = NULL;

    /* Flash light camera */
    if (Pwm_Init(&handle, 1000000, "/sys/class/pwm/pwmchip0/") != 0) {
        printf("Failure: Pwm_Init\n");
    }

    for (value = 0; value < 100; value++) {
        /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        /* !!!!!!!!!!!!!!!!  DO NOT CONTROL ABOVE 10% (HARDWARE DAMAGE)  !!!!!!!!!!!!!!!!*/
        /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        Pwm_Value(handle, (uint32_t)value);
        usleep(50000);
    }

    for ( ;value >= 0; value--) {
        /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        /* !!!!!!!!!!!!!!!!  DO NOT CONTROL ABOVE 10% (HARDWARE DAMAGE)  !!!!!!!!!!!!!!!!*/
        /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        Pwm_Value(handle, (uint32_t)value);
        usleep(50000);
    }
    Pwm_Terminate(&handle);
    return 0;
} /* main */