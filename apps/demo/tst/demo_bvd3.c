/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "demo_led.h"
#include "demo_menu.h"
#include "demo_audio.h"
#include "demo_barcode.h"

#define DIR_BASE                    "/sys/class/leds/"
#define BRIGHT                      "/brightness"

static void DisableCursor()
{
    int fd;
    if ((fd = open("/sys/class/graphics/fbcon/cursor_blink", O_WRONLY | O_NONBLOCK, 0)) < 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        return;
    }
    write(fd, "0", sizeof("0"));
    close(fd);
}

int main()
{
    demo_led_handle_t                   demo_led = NULL;
    demo_audio_handle_t                 demo_audio = NULL;
    demo_barcode_handle_t               demo_barcode = NULL;
    demo_finger_print_scanner_handle_t  demo_finger_print_scanner = NULL;
    demo_gps_handle_t                   demo_gps = NULL;
    demo_gsm_handle_t                   demo_gsm = NULL;
    demo_menu_handle_t                  demo_menu = NULL;

    keyboard_handle_t                   keyboard;
    display_handle_t                    display;
    power_handle_t                      power;
    led_rgb_handle_t                    led_status_rb, led_status_rt, led_status_lb, led_status_lt;
    led_handle_t                        led_foil_left_thumb, led_foil_left_index_finger, led_foil_left_middle_finger, led_foil_left_ring_finger, led_foil_left_pinky;
    led_handle_t                        led_foil_right_thumb, led_foil_right_index_finger, led_foil_right_middle_finger, led_foil_right_ring_finger, led_foil_right_pinky;
    led_handle_t                        led_foil_arrow, led_foil_error, led_foil_ok;
    led_handle_t                        led_laser_0, led_laser_1;
    led_handle_t                        led_display_ok, led_display_active, led_display_hourglass;
    pwm_handle_t                        flashlight;
    audio_handle_t                      audio;
    camera_handle_t                     camera;
    finger_print_scanner_handle_t       finger_print_scanner;
    gps_se873_handle_t                  gps_se873;
    gsm_handle_t                        gsm;

    uint32_t                            switch_off = 0;

    printf(__DATE__ "  " __TIME__ "\n");

    DisableCursor();

    /* Keyboard init */
    if (Keyboard_Init(&keyboard, "/dev/input/event0") != 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display init */
    if (Display_Init(&display, "/dev/fb0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display init */
    if (Power_Init(&power, "/sys/class/gpio/") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Init status LED's */
    if (LedRgb_Init(&led_status_rb, DIR_BASE"status:red:right_bottom"BRIGHT,DIR_BASE"status:green:right_bottom"BRIGHT,  DIR_BASE"status:blue:right_bottom"BRIGHT) != 0  ||
        LedRgb_Init(&led_status_rt, DIR_BASE"status:red:right_top"BRIGHT,   DIR_BASE"status:green:right_top"BRIGHT,     DIR_BASE"status:blue:right_top"BRIGHT) != 0     ||
        LedRgb_Init(&led_status_lb, DIR_BASE"status:red:left_bottom"BRIGHT, DIR_BASE"status:green:left_bottom"BRIGHT,   DIR_BASE"status:blue:left_bottom"BRIGHT) != 0   ||        
        LedRgb_Init(&led_status_lt, DIR_BASE"status:red:left_top"BRIGHT,    DIR_BASE"status:green:left_top"BRIGHT,      DIR_BASE"status:blue:left_top"BRIGHT) != 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Foil LED's init */
    if (Led_Init(&led_foil_left_thumb,           DIR_BASE"foil:mono:left_thumb"BRIGHT) != 0          ||
        Led_Init(&led_foil_left_index_finger,    DIR_BASE"foil:mono:left_index_finger"BRIGHT) != 0   ||
        Led_Init(&led_foil_left_middle_finger,   DIR_BASE"foil:mono:left_middle_finger"BRIGHT) != 0  ||
        Led_Init(&led_foil_left_ring_finger,     DIR_BASE"foil:mono:left_ring_finger"BRIGHT) != 0    ||
        Led_Init(&led_foil_left_pinky,           DIR_BASE"foil:mono:left_pinky"BRIGHT) != 0          ||
        Led_Init(&led_foil_right_thumb,          DIR_BASE"foil:mono:right_thumb"BRIGHT) != 0         ||
        Led_Init(&led_foil_right_index_finger,   DIR_BASE"foil:mono:right_index_finger"BRIGHT) != 0  ||
        Led_Init(&led_foil_right_middle_finger,  DIR_BASE"foil:mono:right_middle_finger"BRIGHT) != 0 ||
        Led_Init(&led_foil_right_ring_finger,    DIR_BASE"foil:mono:right_ring_finger"BRIGHT) != 0   ||
        Led_Init(&led_foil_right_pinky,          DIR_BASE"foil:mono:right_pinky"BRIGHT) != 0         ||
        Led_Init(&led_foil_arrow,                DIR_BASE"foil:mono:arrow"BRIGHT) != 0               ||
        Led_Init(&led_foil_error,                DIR_BASE"foil:mono:finger_error"BRIGHT) != 0        ||
        Led_Init(&led_foil_ok,                   DIR_BASE"foil:mono:finger_ok"BRIGHT) != 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display LED's init */
    if (Led_Init(&led_display_hourglass, DIR_BASE"display:mono:hour_glass"BRIGHT) != 0  ||
        Led_Init(&led_display_active,    DIR_BASE"display:mono:busy"BRIGHT) != 0        ||
        Led_Init(&led_display_ok,        DIR_BASE"display:mono:ok"BRIGHT) != 0) {
            
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }


    /* Laser LED's init */
    if (Led_Init(&led_laser_0, DIR_BASE"laser:mono:0"BRIGHT) != 0   ||
        Led_Init(&led_laser_1, DIR_BASE"laser:mono:1"BRIGHT) != 0) {
            
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Flashlight init */
    if (Pwm_Init(&flashlight, 1000000, "/sys/class/pwm/pwmchip0/") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }
    
    /* Audio init */
    if (Audio_Init(&audio) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }
    
    
    /* Laser LED 0 init */
    if (Led_Init(&led_laser_0, "/sys/class/leds/laser:mono:0/brightness") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Laser LED 1 init */
    if (Led_Init(&led_laser_1, "/sys/class/leds/laser:mono:1/brightness") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Keyboard init */
    if (Keyboard_Init(&keyboard, "/dev/input/event0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display init */
    if (Display_Init(&display, "/dev/fb0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Camera Init */
    if (Camera_Init(&camera, "/dev/video0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    if (FingerPrintScanner_Init(&finger_print_scanner) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    if (GpsSe873_Init(&gps_se873, "/dev/ttymxc2") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        gps_se873 = NULL;
    }

    if (Gsm_Init(&gsm, "/dev/ttyACM0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        gsm = NULL;
    }

    if (DemoLed_Init(&demo_led,
                     keyboard,
                     display,
                     led_status_rb,
                     led_status_rt,
                     led_status_lb,
                     led_status_lt,
                     led_laser_0,
                     led_laser_1,
                     led_display_ok,
                     led_display_active,
                     led_display_hourglass,
                     led_foil_left_thumb,
                     led_foil_left_index_finger,
                     led_foil_left_middle_finger,
                     led_foil_left_ring_finger,
                     led_foil_left_pinky,
                     led_foil_right_thumb,
                     led_foil_right_index_finger,
                     led_foil_right_middle_finger,
                     led_foil_right_ring_finger,
                     led_foil_right_pinky,
                     led_foil_arrow,
                     led_foil_error,
                     led_foil_ok) != 0) {

        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    if (DemoAudio_Init(&demo_audio, keyboard, display, audio) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
    }

    if (DemoBarcode_Init(&demo_barcode, keyboard, display, led_laser_0, led_laser_1, flashlight, camera) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    if (DemoFingerPrintScanner_Init(&demo_finger_print_scanner, keyboard, display, finger_print_scanner) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    if (gps_se873 != NULL) {
        if (DemoGps_Init(&demo_gps, keyboard, display, gps_se873) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
        }
    } else {
        demo_gps = NULL;
    }

    if (gsm != NULL) {
        if (DemoGsm_Init(&demo_gsm, keyboard, display, gsm) != 0) {
            printf("%s[%d]\n", __func__, __LINE__);
        }
    } else {
        demo_gsm = NULL;
    }

    if (DemoMenu_Init(  &demo_menu,
                        keyboard,
                        display,
                        power,
                        demo_barcode,
                        demo_finger_print_scanner,
                        demo_audio,
                        demo_gsm,
                        demo_gps,
                        demo_led) != 0) {

        printf("%s[%d]\n", __func__, __LINE__);
    }

    switch_off = 1;
    DemoMenu_Execute(demo_menu);

    DemoMenu_Terminate(&demo_menu);

END:
    DemoLed_Terminate(&demo_led);
    DemoAudio_Terminate(&demo_audio);
    DemoBarcode_Terminate(&demo_barcode);
    DemoFingerPrintScanner_Terminate(&demo_finger_print_scanner);
    DemoGps_Terminate(&demo_gps);
    DemoGsm_Terminate(&demo_gsm);

    Keyboard_Terminate(&keyboard);
    Display_Terminate(&display);
    LedRgb_Terminate(&led_status_rb);
    LedRgb_Terminate(&led_status_rt);
    LedRgb_Terminate(&led_status_lb);
    LedRgb_Terminate(&led_status_lt);
    Led_Terminate(&led_foil_left_thumb);
    Led_Terminate(&led_foil_left_index_finger);
    Led_Terminate(&led_foil_left_middle_finger);
    Led_Terminate(&led_foil_left_ring_finger);
    Led_Terminate(&led_foil_left_pinky);
    Led_Terminate(&led_foil_right_thumb);
    Led_Terminate(&led_foil_right_index_finger);
    Led_Terminate(&led_foil_right_middle_finger);
    Led_Terminate(&led_foil_right_ring_finger);
    Led_Terminate(&led_foil_right_pinky);
    Led_Terminate(&led_foil_arrow);
    Led_Terminate(&led_foil_error);
    Led_Terminate(&led_foil_ok);
    Led_Terminate(&led_display_hourglass);
    Led_Terminate(&led_display_active);
    Led_Terminate(&led_display_ok);
    Led_Terminate(&led_laser_0);
    Led_Terminate(&led_laser_1);
    Pwm_Terminate(&flashlight);
    Audio_Terminate(&audio);
    Camera_Terminate(&camera);
    FingerPrintScanner_Terminate(&finger_print_scanner);
    GpsSe873_Terminate(&gps_se873);
    Gsm_Terminate(&gsm);
    Power_Terminate(&power, switch_off);
    return 0;
} /* main */
