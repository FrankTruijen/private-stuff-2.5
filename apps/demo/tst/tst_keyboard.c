/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <keyboard.h>

void EvaluateKeyEvent( key_event_t key_event_test, const char *skey_name_test)
{   
    keyboard_handle_t handle = NULL;
    key_event_t key_event = KEY_EVENT_NONE;

    if (Keyboard_Init(&handle, "/dev/input/event0") != 0) {
        printf("Failure: KeyboardInit\n");
    }

    printf("Push key '%s'\n", skey_name_test);
    while (key_event != key_event_test) {
        key_event = Keyboard_GetEvent(handle);
        if (key_event != KEY_EVENT_NONE) {
            if (key_event != key_event_test) {
                printf("Wrong key event, expected key '%s'!\n", skey_name_test);
            }
        }
    }
    Keyboard_Terminate(&handle);
}

int main()
{
    EvaluateKeyEvent(KEY_EVENT_0, "0");
    EvaluateKeyEvent(KEY_EVENT_1, "1");
    EvaluateKeyEvent(KEY_EVENT_2, "2");
    EvaluateKeyEvent(KEY_EVENT_3, "3");
    EvaluateKeyEvent(KEY_EVENT_4, "4");
    EvaluateKeyEvent(KEY_EVENT_5, "5");
    EvaluateKeyEvent(KEY_EVENT_6, "6");
    EvaluateKeyEvent(KEY_EVENT_7, "7");
    EvaluateKeyEvent(KEY_EVENT_8, "8");
    EvaluateKeyEvent(KEY_EVENT_9, "9");
    EvaluateKeyEvent(KEY_EVENT_F1, "F1");
    EvaluateKeyEvent(KEY_EVENT_F2, "F2");
    EvaluateKeyEvent(KEY_EVENT_GREEN_CHECK, "GREEN CHECK");
    EvaluateKeyEvent(KEY_EVENT_RED_FAIL, "RED FAIL");
    EvaluateKeyEvent(KEY_EVENT_BACK, "Back");
    return 0;
}