/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <unistd.h>

#include "audio.h"
#include "eagles.h"

int main()
{
    audio_handle_t handle = NULL;

    if (Audio_Init(&handle) != 0) {
        printf("Failure: Audio_Init\n");
        return -1;
    }

    if (Audio_SetStream(handle, (int16_t *)eagles_wav, sizeof(eagles_wav)) != 0) {
        printf("Failure: Audio_SetStream\n");
        return -1;        
    }
    while (Audio_Play(handle) == 0) ;

    sleep(3);
    if (Audio_SetStream(handle, (int16_t *)eagles_wav, sizeof(eagles_wav)) != 0) {
        printf("Failure: Audio_SetStream\n");
        return -1;        
    }
    while (Audio_Play(handle) == 0) {
        usleep(10000);
    }

    Audio_Terminate(&handle);
    return 0;
}