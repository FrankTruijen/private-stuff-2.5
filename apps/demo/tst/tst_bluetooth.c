/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>

#include "bluetooth.h"

#define SDEVICE_DUMMY       "dummy"

int main()
{
    bluetooth_handle_t handle = NULL;

    if (Bluetooth_Init(&handle, SDEVICE_DUMMY) != 0) {
        printf("Failure: Bluetooth_Init\n");
    }
    Bluetooth_Terminate(&handle);
    return 0;
}