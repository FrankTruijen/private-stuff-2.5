/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>

#include "gsm.h"

int main()
{
    gsm_handle_t handle = NULL;
    char sgsm[128];
    
    sprintf(sgsm, "/dev/ttyACM0");
    if (Gsm_Init(&handle, sgsm) != 0) {
        printf("Failure: Gsm_Init\n");
    }
    if (Gsm_SendSMS(handle, "+31622421317", "BVD3 test 1") != SEND_SMS_RETURN_OK) {
        printf("Failure: Test1 Gsm_SendSMS\n");
    }
    if (Gsm_SendSMS(handle, "+31000000000", "BVD3 test 2") != SEND_SMS_RETURN_FAILURE) {
        printf("Failure: Test2 Gsm_SendSMS\n");
    }
    if (Gsm_SendSMS(handle, "+31622421317", "BVD3 test 3") != SEND_SMS_RETURN_OK) {
        printf("Failure: Test3 Gsm_SendSMS\n");
    }
    Gsm_Terminate(&handle);
    return 0;
}