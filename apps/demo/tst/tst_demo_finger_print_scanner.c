/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <unistd.h>
#include "demo_finger_print_scanner.h"

int main()
{
    demo_finger_print_scanner_handle_t  handle = NULL;
    keyboard_handle_t                   keyboard = NULL;
    display_handle_t                    display = NULL;
    finger_print_scanner_handle_t       fps = NULL;

    /* Keyboard init */
    if (Keyboard_Init(&keyboard, "/dev/input/event0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display init */
    if (Display_Init(&display, "/dev/fb0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Finger Print Scanner init */
    if (FingerPrintScanner_Init(&fps) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }
    
    if (DemoFingerPrintScanner_Init(&handle, keyboard, display, fps) != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    while (DemoFingerPrintScanner_Execute(handle) == EXECUTE_RETURN_OK) {
        usleep(10000);
    }
    DemoFingerPrintScanner_Terminate(&handle);

END:
    FingerPrintScanner_Terminate(&fps);   
    Display_Terminate(&display);
    Keyboard_Terminate(&keyboard);
    return 0;
} /* main */
