/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#include <stdio.h>
#include <unistd.h>

#include "demo_audio.h"

int main()
{
    demo_audio_handle_t handle;
    keyboard_handle_t   keyboard;
    display_handle_t    display;
    audio_handle_t      audio;

    /* Keyboard init */
    if (Keyboard_Init(&keyboard, "/dev/input/event0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Display init */
    if (Display_Init(&display, "/dev/fb0") != 0) {
        printf("%s[%d]\n", __func__, __LINE__);
        goto END;
    }

    /* Audio init */
    if (Audio_Init(&audio) != 0) {
        printf("Failure: Audio_Init\n");
        goto END;
    }
    
    if (DemoAudio_Init(&handle, keyboard, display, audio) != 0) {
        printf("Failure: DemoAudio_Init\n");
    }

    while (DemoAudio_Execute(handle) == EXECUTE_RETURN_OK) {
        usleep(10000);
    }
    DemoAudio_Terminate(&handle);

END:
    Audio_Terminate(&audio);
    Display_Terminate(&display);
    Keyboard_Terminate(&keyboard);
    return 0;
} /* main */