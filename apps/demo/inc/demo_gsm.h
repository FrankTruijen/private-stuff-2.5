/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __DEMO_GSM_H__
#define __DEMO_GSM_H__

#include "global.h"
#include "display.h"
#include "keyboard.h"
#include "gsm.h"

typedef struct _demo_gsm_handle *demo_gsm_handle_t;

int DemoGsm_Init(
    demo_gsm_handle_t   *phandle,
    keyboard_handle_t   keyboard,
    display_handle_t    display,
    gsm_handle_t        gsm);

void DemoGsm_Terminate(
    demo_gsm_handle_t   *phandle);

execute_return_t DemoGsm_Execute(
    demo_gsm_handle_t   handle);

#endif /* __DEMO_GSM_H__ */
