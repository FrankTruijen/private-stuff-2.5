/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __DEMO_BARCODE_H__
#define __DEMO_BARCODE_H__

#include "led.h"
#include "pwm.h"
#include "global.h"
#include "camera.h"
#include "display.h"
#include "keyboard.h"

typedef struct _demo_barcode_handle *demo_barcode_handle_t;

int DemoBarcode_Init(
    demo_barcode_handle_t   *phandle,
    keyboard_handle_t       keyboard,
    display_handle_t        display,
    led_handle_t            laser_0,
    led_handle_t            laser_1,
    pwm_handle_t            flashlight,
    camera_handle_t         camera);

void DemoBarcode_Terminate(
    demo_barcode_handle_t   *phandle);

execute_return_t DemoBarcode_Execute(
    demo_barcode_handle_t   handle);

#endif /* __DEMO_BARCODE_H__ */
