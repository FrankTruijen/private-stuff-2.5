/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __DEMO_GPS_H__
#define __DEMO_GPS_H__

#include "global.h"
#include "display.h"
#include "keyboard.h"
#include "gps_se873.h"

typedef struct _demo_gps_handle *demo_gps_handle_t;

int DemoGps_Init(
    demo_gps_handle_t       *phandle,
    keyboard_handle_t       keyboard,
    display_handle_t        display,
    gps_se873_handle_t      gps);

void DemoGps_Terminate(
    demo_gps_handle_t       *phandle);

execute_return_t DemoGps_Execute(
    demo_gps_handle_t       handle);

#endif /* __DEMO_GPS_H__ */
