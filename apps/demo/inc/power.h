/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __POWER_H__
#define __POWER_H__

#include <stdint.h>

typedef struct _power_handle *power_handle_t;

int Power_Init(power_handle_t *phandle, const char *sprefix);

int Power_ButtonPushed(power_handle_t handle);

void Power_Terminate(power_handle_t *phandle, int switch_off);

#endif /* __POWER_H__ */
