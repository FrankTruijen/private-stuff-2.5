/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __CREATE_HANDLE_H__
#define __CREATE_HANDLE_H__

#include "stdint.h"

int CreateHandle_Init(void **phandle, uint32_t size);

void CreateHandle_Terminate(void **phandle);

#endif /* __CREATE_HANDLE_H__ */