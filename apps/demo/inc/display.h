/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include <stdint.h>

typedef struct _display_handle *display_handle_t;

typedef enum {
    FONT_SIZE_8,
    FONT_SIZE_16,
    FONT_SIZE_32,
} font_size_t;

int Display_Init(display_handle_t *phandle, const char *sdevice);

int Display_ShowImageRGB888(display_handle_t handle, uint32_t width, uint32_t height, uint32_t stride, uint8_t *rgb);

int Display_ShowImageRGB888(display_handle_t handle, uint32_t width, uint32_t height, uint32_t stride, uint8_t *rgb);

int Display_GetImageBufferBGRA(display_handle_t handle, uint32_t **bgra);

int Display_PutImageBufferBGRA(display_handle_t handle);

uint32_t Display_GetResolutionX(display_handle_t handle);

uint32_t Display_GetResolutionY(display_handle_t handle);

void Display_Terminate(display_handle_t *phandle);

void InsertText(display_handle_t handle, char *s, uint32_t x, uint32_t y, uint8_t r, uint8_t g, uint8_t b, font_size_t font_size);

void ClearText(display_handle_t handle);

void YtoBGRA(uint8_t *y, uint32_t width, uint32_t height, uint32_t *bgra);

#endif /* __DISPLAY_H__ */