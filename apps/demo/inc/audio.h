/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __AUDIO_H__
#define __AUDIO_H__

#include <stdint.h>

typedef struct _audio_handle *audio_handle_t;

int Audio_Init(audio_handle_t *phandle);

void Audio_Terminate(audio_handle_t *phandle);

int Audio_SetStream(audio_handle_t handle, const int16_t *data, uint32_t size);

int Audio_Play(audio_handle_t handle);

#endif /* __AUDIO_H__ */