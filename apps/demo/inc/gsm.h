/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __GSM_H__
#define __GSM_H__

typedef enum {
    SEND_SMS_RETURN_ERROR = -1,
    SEND_SMS_RETURN_OK = 0,
    SEND_SMS_RETURN_FAILURE,
} send_sms_return_t;


typedef struct _gsm_handle *gsm_handle_t;

int Gsm_Init(gsm_handle_t *phandle, const char *sdevice);

void Gsm_Terminate(gsm_handle_t *phandle);

send_sms_return_t Gsm_SendSMS(gsm_handle_t handle, const char *sphone_number, const char *smessage);

#endif /* __GSM_H__ */
