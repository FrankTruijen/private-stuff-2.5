/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __BMP_H__
#define __BMP_H__

char *GetDateTime();

void R8G8B8toBMP(int width, int height, unsigned char *r, unsigned char *g, unsigned char *b, char *sname);

#endif /* __BMP_H__ */