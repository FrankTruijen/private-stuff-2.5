/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

typedef struct _keyboard_handle *keyboard_handle_t;

typedef enum {
	KEY_EVENT_ERROR = -1,
    KEY_EVENT_NONE = 0,
    KEY_EVENT_0,
    KEY_EVENT_1,
    KEY_EVENT_2,
    KEY_EVENT_3,
    KEY_EVENT_4,
    KEY_EVENT_5,
    KEY_EVENT_6,
    KEY_EVENT_7,
    KEY_EVENT_8,
    KEY_EVENT_9,
    KEY_EVENT_F1,
    KEY_EVENT_F2,
    KEY_EVENT_GREEN_CHECK,
    KEY_EVENT_RED_FAIL,
    KEY_EVENT_BACK
} key_event_t;

int Keyboard_Init(keyboard_handle_t *phandle, const char *sdevice);

key_event_t Keyboard_GetEvent(keyboard_handle_t handle);

void Keyboard_Terminate(keyboard_handle_t *phandle);

#endif /* __KEYBOARD_H__ */