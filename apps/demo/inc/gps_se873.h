/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __GPS_SE873_H__
#define __GPS_SE873_H__

typedef struct _gps_se873_handle *gps_se873_handle_t;

typedef struct {
    char degree[8];
    char minutes[8];
    char hemisphere[8];
} gps_se873_location_unit_t;

typedef struct {
    gps_se873_location_unit_t longitude;
    gps_se873_location_unit_t latitude;
} gps_se873_location_t;

int GpsSe873_Init(gps_se873_handle_t *phandle, const char *sdevice);

int GpsSe873_GetLocation(gps_se873_handle_t handle, gps_se873_location_t *plocation);

void GpsSe873_Terminate(gps_se873_handle_t *phandle);

#endif /* __GPS_SE873_H__ */
