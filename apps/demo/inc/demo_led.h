/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __DEMO_LED_H__
#define __DEMO_LED_H__

#include "led.h"
#include "global.h"
#include "display.h"
#include "keyboard.h"


typedef struct _demo_led_handle *demo_led_handle_t;

int DemoLed_Init(
    demo_led_handle_t   *phandle,
    keyboard_handle_t   keyboard,
    display_handle_t    display,
    led_rgb_handle_t    led_status_rb,
    led_rgb_handle_t    led_status_rt,
    led_rgb_handle_t    led_status_lb,
    led_rgb_handle_t    led_status_lt,
    led_handle_t        led_laser_0,
    led_handle_t        led_laser_1,
    led_handle_t        led_display_ok,
    led_handle_t        led_display_active,
    led_handle_t        led_display_hourglass,
    led_handle_t        led_foil_left_thumb,
    led_handle_t        led_foil_left_index_finger,
    led_handle_t        led_foil_left_middle_finger,
    led_handle_t        led_foil_left_ring_finger,
    led_handle_t        led_foil_left_pinky,
    led_handle_t        led_foil_right_thumb,
    led_handle_t        led_foil_right_index_finger,
    led_handle_t        led_foil_right_middle_finger,
    led_handle_t        led_foil_right_ring_finger,
    led_handle_t        led_foil_right_pinky,
    led_handle_t        led_foil_arrow,
    led_handle_t        led_foil_error,
    led_handle_t        led_foil_ok);

void DemoLed_Terminate(
    demo_led_handle_t   *phandle);

execute_return_t DemoLed_Execute(
    demo_led_handle_t   handle);

#endif /* __DEMO_LED_H__ */