/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __GLOBAL_H__
#define __GLOBAL_H__

typedef enum {
    EXECUTE_RETURN_ERROR = -1,
    EXECUTE_RETURN_OK = 0,
    EXECUTE_RETURN_FINISHED = 1,
} execute_return_t;

#endif /* __GLOBAL_H__ */