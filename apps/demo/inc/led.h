/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __LED_H__
#define __LED_H__

#include <stdint.h>

typedef struct _led_handle *led_handle_t;
typedef struct _led_rgb_handle *led_rgb_handle_t;

int Led_Init(led_handle_t *phandle, const char *sdevice);
int LedRgb_Init(led_rgb_handle_t *phandle, const char *sdevice_r, const char *sdevice_g, const char *sdevice_b);

int Led_Value(led_handle_t handle, uint8_t brightness);
int LedRgb_Value(led_rgb_handle_t handle, uint8_t brightness_r, uint8_t brightness_g, uint8_t brightness_b);

void Led_Terminate(led_handle_t *phandle);
void LedRgb_Terminate(led_rgb_handle_t *phandle);

#endif /* __LED_H__ */