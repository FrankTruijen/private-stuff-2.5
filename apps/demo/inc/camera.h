/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <stdint.h>

typedef struct _camera_handle *camera_handle_t;

/* Diffent supported modes are copied from driver sources
 * file (drivers/media/platform/mxc/subdev/ov5640_mipi.c). 
 * 
 *      enum ov5640_mode {
 *              ov5640_mode_MIN = 0,
 *              ov5640_mode_VGA_640_480 = 0,
 *              ov5640_mode_NTSC_720_480 = 1,
 *              ov5640_mode_720P_1280_720 = 2,
 *              ov5640_mode_1080P_1920_1080 = 3,
 *              ov5640_mode_QSXGA_2592_1944 = 4,
 *              ov5640_mode_MAX = 5,
 *              ov5640_mode_INIT = 0xff,
 *      };
 */
typedef enum {
    MODE_VGA_640_480 = 0,
    MODE_NTSC_720_480 = 1,
    MODE_720P_1280_720 = 2,
    MODE_1080P_1920_1080 = 3,
    MODE_QSXGA_2592_1944 = 4,
    MODE_MAX
} resolution_mode_t;

typedef enum {
    CAMERA_ERROR = -1,
    CAMERA_NEW_FRAME = 0,
    CAMERA_NO_FRAME = 1,
} camera_get_image_status_t;

int Camera_Init(camera_handle_t *phandle, const char *sdevice);

int Camera_Start(camera_handle_t handle, resolution_mode_t mode);

int Camera_Stop(camera_handle_t handle);

camera_get_image_status_t Camera_GetImageYUV422(
    camera_handle_t     handle, 
    uint32_t            width, 
    uint32_t            height,
    uint32_t            stride_y,
    uint32_t            stride_uv,
    uint8_t             *y,
    uint8_t             *u,
    uint8_t             *v);

camera_get_image_status_t Camera_GetImageBufferYUYV(
    camera_handle_t     handle, 
    uint32_t            **yuyv);

camera_get_image_status_t Camera_PutImageBufferYUYV(
    camera_handle_t     handle);

uint32_t Camera_GetResolutionX(camera_handle_t handle);

uint32_t Camera_GetResolutionY(camera_handle_t handle);

void Camera_Terminate(camera_handle_t *phandle);

#endif /* __CAMERA_H__ */