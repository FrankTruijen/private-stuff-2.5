/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __PWM_H__
#define __PWM_H__

#include <stdint.h>

typedef struct _pwm_handle *pwm_handle_t;

int Pwm_Init(pwm_handle_t *phandle, uint32_t period, const char *sprefix);

int Pwm_Value(pwm_handle_t handle, uint32_t duty_cycle);

void Pwm_Terminate(pwm_handle_t *phandle);

#endif /* __PWM_H__ */