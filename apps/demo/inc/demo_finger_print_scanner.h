/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __DEMO_FINGER_PRINT_SCANNER_H__
#define __DEMO_FINGER_PRINT_SCANNER_H__

#include "global.h"
#include "display.h"
#include "keyboard.h"
#include "finger_print_scanner.h"

typedef struct _demo_finger_print_scanner_handle *demo_finger_print_scanner_handle_t;

int DemoFingerPrintScanner_Init(
    demo_finger_print_scanner_handle_t  *phandle,
    keyboard_handle_t                   keyboard,
    display_handle_t                    display,
    finger_print_scanner_handle_t       finger_print_scanner);

void DemoFingerPrintScanner_Terminate(
    demo_finger_print_scanner_handle_t  *phandle);

execute_return_t DemoFingerPrintScanner_Execute(
    demo_finger_print_scanner_handle_t  handle);

#endif /* __DEMO_FINGER_PRINT_SCANNER_H__ */
