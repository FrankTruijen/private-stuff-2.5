/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __DEMO_AUDIO_H__
#define __DEMO_AUDIO_H__

#include "audio.h"
#include "global.h"
#include "display.h"
#include "keyboard.h"

typedef struct _demo_audio_handle *demo_audio_handle_t;

int DemoAudio_Init(
    demo_audio_handle_t *phandle,
    keyboard_handle_t   keyboard,
    display_handle_t    display,
    audio_handle_t      audio);

void DemoAudio_Terminate(
    demo_audio_handle_t *phandle);

execute_return_t DemoAudio_Execute(
    demo_audio_handle_t handle);

#endif /* __DEMO_AUDIO_H__ */
