/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __DEMO_MENU_H__
#define __DEMO_MENU_H__

#include "power.h"
#include "global.h"
#include "display.h"
#include "keyboard.h"
#include "demo_audio.h"
#include "demo_barcode.h"
#include "demo_bluetooth.h"
#include "demo_finger_print_scanner.h"
#include "demo_gps.h"
#include "demo_gsm.h"
#include "demo_led.h"

typedef struct _demo_menu_handle *demo_menu_handle_t;

int DemoMenu_Init(
    demo_menu_handle_t                  *phandle,
    keyboard_handle_t                   keyboard,
    display_handle_t                    display,
    power_handle_t                      power,
    demo_barcode_handle_t               demo_barcode,
    demo_finger_print_scanner_handle_t  demo_finger_print_scanner,
    demo_audio_handle_t                 demo_audio,
    demo_gsm_handle_t                   demo_gsm,
    demo_gps_handle_t                   demo_gps,
    demo_led_handle_t                   demo_led);

void DemoMenu_Terminate(
    demo_menu_handle_t                  *phandle);

execute_return_t DemoMenu_Execute(
    demo_menu_handle_t                  handle);

#endif /* __DEMO_MENU_H__ */