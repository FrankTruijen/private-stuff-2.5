/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __FINGER_PRINT_SCANNER_H__
#define __FINGER_PRINT_SCANNER_H__

typedef struct _finger_print_scanner_handle *finger_print_scanner_handle_t;

int FingerPrintScanner_Init(finger_print_scanner_handle_t *phandle);

int FingerPrintScanner_IsFingerPresent(finger_print_scanner_handle_t handle);

int FingerPrintScanner_GetImageY(finger_print_scanner_handle_t handle, uint8_t *y);

uint32_t FingerPrintScanner_GetResolutionX(finger_print_scanner_handle_t handle);

uint32_t FingerPrintScanner_GetResolutionY(finger_print_scanner_handle_t handle);

void FingerPrintScanner_Terminate(finger_print_scanner_handle_t *phandle);

#endif /* __FINGER_PRINT_SCANNER_H__ */