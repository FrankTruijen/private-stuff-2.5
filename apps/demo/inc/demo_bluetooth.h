/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __DEMO_BLUETOOTH_H__
#define __DEMO_BLUETOOTH_H__

#include "global.h"
#include "display.h"
#include "keyboard.h"
#include "bluetooth.h"

typedef struct _demo_bluetooth_handle *demo_bluetooth_handle_t;

int DemoBluetooth_Init(
    demo_bluetooth_handle_t     *phandle,
    keyboard_handle_t           keyboard,
    display_handle_t            display,
    bluetooth_handle_t          bluetooth);

void DemoBluetooth_Terminate(
    demo_bluetooth_handle_t     *phandle);

execute_return_t DemoBluetooth_Execute(
    demo_bluetooth_handle_t     handle);

#endif /* __DEMO_BLUETOOTH_H__ */
