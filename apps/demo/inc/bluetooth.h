/*
 * Copyright (C) 2017 VEDS Group Eindhoven (Netherlands)
 *
 * Author: Frank Truijen <frank.truijen@woutru.nl>
 * 
 * Software is created for test/demo application purpose 
 * and not fully tested.
 */

#ifndef __BLUETOOTH_H__
#define __BLUETOOTH_H__

typedef struct _bluetooth_handle *bluetooth_handle_t;

int Bluetooth_Init(bluetooth_handle_t *phandle, const char *sdevice);

void Bluetooth_Terminate(bluetooth_handle_t *phandle);

#endif /* __BLUETOOTH_H__ */