### What is this repository for? ###

This repository contains some files from third parties and some useful stuff that needs to be archived in the downloads section.

# Demo Package Creation Guide #
===============================

### Retrieve Project Source ###
BASE_DIR=~/BVD3/    
mkdir $BASE_DIR    
cd $BASE_DIR    
git clone https://xxxxxxxxxxxx@bitbucket.org/bvd3/mfgtools.git    
git clone https://xxxxxxxxxxxx@bitbucket.org/bvd3/private-stuff.git    

### Create Kernel Image ###
cd $BASE_DIR    
cd private-stuff/kernel/    
./get_kernel_source.sh    
(cd linux; patch -Np1 --ignore-whitespace -i ../patches/demo.patch) (Creeted with: 'diff -urN . ../linux > ../patches/demo.patch')    
./setup_build_dir.sh defconfigs/defconfig_demo    
./make_bvd3_kernel.sh    

### Create Device Tree ###
cd $BASE_DIR    
cd private-stuff/device_tree/    
make    

### Create Bootloader ###
cd $BASE_DIR    
cd private-stuff/u-boot-imx/    
./bvd3_defconfig.sh    
./build.sh    

### Create Root File System (RAM boot and Flash boot) ###
cd $BASE_DIR    
cd private-stuff/initramfs/    
cp ../../mfgtools/Profiles/BVD3/OS\ Firmware/files/initramfs-mfgtool.u-boot initramfs-mfgtool.u-boot.repo    
./extract_initramfs.sh initramfs-mfgtool.u-boot.repo    
./include_addons.sh    
./create_initramfs.sh    

### Create Demo Application ###
cd $BASE_DIR    
cd private-stuff/apps/demo    
make    
make install_demo (during development make install_test)    
    
BE AWARE THAT ROOTFS HAS TO BE CREARTED AGAIN !    

### Create Library ALSA (Audio) ###
Needed library is already available demo application and rootfs, not needed to build.    
cd $BASE_DIR    
cd private-stuff/apps/libs    
tar xvjf alsa-lib-1.1.3.tar.bz2    
cd alsa-lib-1.1.3    
./configure CC=arm-linux-gnueabihf-gcc LD=arm-linux-gnueabihf-ld --host=arm-linux-gnueabi --enable-static=yes --enable-shared=no --prefix /alsa    
make    

### Create Library ZBAR (Barcode Scanner Algorithm) ###
Needed library is already available demo application and rootfs, not needed to build.    
cd $BASE_DIR    
cd private-stuff/apps/libs/zbar-0.10    
CC=arm-linux-gnueabihf-gcc LD=arm-linux-gnueabihf-ld CFLAGS="" ./configure --prefix /home/ict043/WS/private-stuff/apps/zbar-0.10/zbar-0.10/result/ --host=arm-none-linux-gnueabi --without-xshm --without-xshm --without-xv --without-jpeg --without-imagemagick --without-gtk --without-npapi --without-python --without-qt --enable-video=no --enable-video=no --enable-pthread=no --enable-shared=no    
make    

### Create Library ScanAPI (Finger Print Scanner) ###
Needed library is already available demo application and rootfs, not needed to build.    
Library is delivered pre-compiled.    
tar xvzf ScanAPI_v1028_armlinux_linaro_4.9-2014.11_gnueabihf.tar.gz

