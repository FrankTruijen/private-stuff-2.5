#!/bin/bash

# Install git-lfs repo and install git-lfs client
# Afterwards, follow the instructions from Atlassian to setup git to use lfs
#
# Documentation:
#
# https://confluence.atlassian.com/bitbucket/use-git-lfs-with-bitbucket-828781636.html
#
# https://github.com/git-lfs/git-lfs/wiki/Installation
#
#
# NOTICE: this script runs commands as root using sudo
#
# Bas Mevissen, March 2017
#

# Setup repo for git-lfs and check prerequisites.
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash

# Install git-lfs
sudo yum install -y git-lfs

# Now install git-lfs inside git (user global)
git lfs install

# Optionally, only install for a single repository by calling the following line inside a repository:
# git lfs install --local

# Uninstalling:
# git lfs uninstall

exit 0

