#!/bin/bash

# Requires an arm cross toolchain installed and in path

# Do not autodetect if set in the environment
if [ "$CROSS_COMPILE" != "" ]; then
	echo "Using \"CROSS_COMPILE=$CROSS_COMPILE\" as set in environment"
else
	# Autodetect a few common cross toolchains
	CROSS_COMPILE=""
	if [ "`which arm-linux-gnueabihf-gcc 2>/dev/null`" != "" ] && [ "${CROSS_COMPILE}" = "" ]; then
		CROSS_COMPILE="arm-linux-gnueabihf-"
	fi
	if [ "`which arm-linux-gnueabi-gcc 2>/dev/null`" != "" ] && [ "${CROSS_COMPILE}" = "" ]; then
		CROSS_COMPILE="arm-linux-gnueabi-"
	fi
	if [ "`which arm-none-eabi-gcc 2>/dev/null`" != "" ] && [ "${CROSS_COMPILE}" = "" ]; then
		CROSS_COMPILE="arm-none-eabi-"
	fi
	if [ "`which arm-linux-gnu-gcc 2>/dev/null`" != "" ] && [ "${CROSS_COMPILE}" = "" ]; then
		CROSS_COMPILE="arm-linux-gnu-"
	fi

	if [ "$CROSS_COMPILE" != "" ]; then
		echo "Using \"CROSS_COMPILE=$CROSS_COMPILE\" as auto-detected"
	else
		echo "Could not find ARM cross compiler in path, exiting"
		exit 1
	fi
fi

make bvd3_imx7d_config ARCH=arm CROSS_COMPILE=$CROSS_COMPILE

exit $?

