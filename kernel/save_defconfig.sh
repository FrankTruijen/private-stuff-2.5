#!/bin/bash

# Save the .config as a defconfig to the given file

# Kernel source directory name
KERNEL_DIR="linux"
# Kernel build directory name relative to kernel source directory
BUILD_DIR="../build"

# Mandatory argument is file name to save defconfig file too
if [ "$1" != "" ]; then
	DEFCONFIG_FILE="$1"
else
	echo "Usage: `basename $0` <defconfig_file>"
	exit 1
fi

if [ ! -d "${KERNEL_DIR}" ]; then
	echo "Kernel source directory does not exist, exiting"
	exit 2
fi

if [ ! -d "${KERNEL_DIR}/${BUILD_DIR}" ]; then
	echo "Kernel build directory does not exist, exiting"
	exit 3
fi

(cd "${KERNEL_DIR}"; make ARCH=arm O="${BUILD_DIR}" savedefconfig) && \
(cd "${KERNEL_DIR}"; cat "${BUILD_DIR}/defconfig") > "${DEFCONFIG_FILE}"

echo "Done"
exit 0

