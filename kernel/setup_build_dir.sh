#!/bin/bash

# Creates build directory, populates it with the given config file and runs config on it

# Kernel source directory name
KERNEL_DIR="linux"
# Kernel build directory name relative to kernel source directory
BUILD_DIR="../build"

# Mandatory argument is what kernel configuration file to use
if [ "$1" != "" ]; then
	CONFIG_FILE="$1"
else
	echo "Usage: `basename $0` <config_file> [config_cmd]"
	exit 1
fi

# Optional argument is what kernel configuration command to use
if [ "$2" != "" ]; then
	CONFIG="$2"
else
	CONFIG="olddefconfig"
fi


if [ ! -f "${CONFIG_FILE}" ]; then
	echo "Kernel configuration file does not exist, exiting"
	exit 1
fi

if [ ! -d "${KERNEL_DIR}" ]; then
	echo "Kernel source directory does not exist, exiting"
	exit 2
fi

if [ ! -d "${KERNEL_DIR}/${BUILD_DIR}" ]; then
	echo "Create kernel build directory"
	mkdir -p "${KERNEL_DIR}/${BUILD_DIR}"
else
	echo "Kernel build directory already exists, exiting"
	exit 3
fi

cp -a "${CONFIG_FILE}" "${KERNEL_DIR}/${BUILD_DIR}/.config"
(cd "${KERNEL_DIR}"; make ARCH=arm O="${BUILD_DIR}" ${CONFIG})

echo "Done"
exit 0

