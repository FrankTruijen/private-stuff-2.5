diff --git a/drivers/staging/fbtft/Kconfig b/drivers/staging/fbtft/Kconfig
index 6cf0c58..ec40031 100644
--- a/drivers/staging/fbtft/Kconfig
+++ b/drivers/staging/fbtft/Kconfig
@@ -134,6 +134,12 @@ config FB_TFT_ST7735R
 	help
 	  Generic Framebuffer support for ST7735R
 
+config FB_TFT_ST7789V
+	tristate "FB driver for the ST7789V LCD Controller"
+	depends on FB_TFT
+	help
+	  Generic Framebuffer support for ST7789V
+
 config FB_TFT_TINYLCD
 	tristate "FB driver for tinylcd.com display"
 	depends on FB_TFT
diff --git a/drivers/staging/fbtft/Makefile b/drivers/staging/fbtft/Makefile
index 9e73bee..5309174 100644
--- a/drivers/staging/fbtft/Makefile
+++ b/drivers/staging/fbtft/Makefile
@@ -24,6 +24,7 @@ obj-$(CONFIG_FB_TFT_SSD1306)     += fb_ssd1306.o
 obj-$(CONFIG_FB_TFT_SSD1331)     += fb_ssd1331.o
 obj-$(CONFIG_FB_TFT_SSD1351)     += fb_ssd1351.o
 obj-$(CONFIG_FB_TFT_ST7735R)     += fb_st7735r.o
+obj-$(CONFIG_FB_TFT_ST7789V)     += fb_st7789v.o
 obj-$(CONFIG_FB_TFT_TINYLCD)     += fb_tinylcd.o
 obj-$(CONFIG_FB_TFT_TLS8204)     += fb_tls8204.o
 obj-$(CONFIG_FB_TFT_UC1701)      += fb_uc1701.o
diff --git a/drivers/staging/fbtft/fb_st7789v.c b/drivers/staging/fbtft/fb_st7789v.c
new file mode 100644
index 0000000..3fb1757
--- /dev/null
+++ b/drivers/staging/fbtft/fb_st7789v.c
@@ -0,0 +1,170 @@
+/*
+ * FB driver for the ST7789V LCD Controller (derived from ST7735R driver)
+ *
+ * Copyright (C) 2013 Noralf Tronnes
+ * Copyright (C) 2017 Bas Mevissen
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 2 of the License, or
+ * (at your option) any later version.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License
+ * along with this program; if not, write to the Free Software
+ * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
+ */
+
+#include <linux/module.h>
+#include <linux/kernel.h>
+#include <linux/init.h>
+
+#include "fbtft.h"
+
+#define DRVNAME "fb_st7789v"
+#define DEFAULT_GAMMA "d0 00 02 07 0b 1a 31 54 40 29 12 12 12 17\n" \
+                      "d0 00 02 07 05 25 2d 44 45 1c 18 16 1c 1d"
+
+
+static int default_init_sequence[] = {
+	/* SWRESET - Software reset */
+	-1, 0x11,
+	-2, 2000,
+
+	/* Frame rate settings */
+	-1, 0xb2, 0x0c, 0x0c, 0x00, 0x33, 0x33,
+	-1, 0xb7, 0x61,
+
+	/* Power settings */
+	-1, 0xbb, 0x30,
+	-1, 0xc0, 0x2c,
+	-1, 0xc2, 0x01,
+	-1, 0xc3, 0x0b,
+	-1, 0xc4, 0x20,
+	-1, 0xc6, 0x0f,
+	-1, 0xca, 0x0f,
+	-1, 0xc8, 0x08,
+	-1, 0x55, 0x00,
+	-1, 0xd0, 0xa4, 0xa1,
+
+#if 1
+	/* Set gamma at init */
+	-1, 0xe0, 0xd0, 0x00, 0x02, 0x07, 0x0b, 0x1a, 0x31, 0x54, 0x40, 0x29, 0x12, 0x12, 0x12, 0x17,
+	-1, 0xe1, 0xd0, 0x00, 0x02, 0x07, 0x05, 0x25, 0x2d, 0x44, 0x45, 0x1c, 0x18, 0x16, 0x1c, 0x1d,
+#endif
+
+	-1, 0xe9, 0x08, 0x08, 0x4,
+	-1, 0xe4, 0x27, 0x00, 0x10,
+	-1, 0x3a, 0x66,
+	-1, 0x36, 0x00,
+	-1, 0xb0, 0x11, 0xf0,
+
+	-1, 0xb1, 0x40, 0x02, 0x14,
+
+	-1, 0x29,
+
+	/* end marker */
+	-3
+};
+
+#if 0
+static void set_addr_win(struct fbtft_par *par, int xs, int ys, int xe, int ye)
+{
+	fbtft_par_dbg(DEBUG_SET_ADDR_WIN, par,
+		"%s(xs=%d, ys=%d, xe=%d, ye=%d)\n", __func__, xs, ys, xe, ye);
+
+	/* Column address */
+	write_reg(par, 0x2A, xs >> 8, xs & 0xFF, xe >> 8, xe & 0xFF);
+
+	/* Row address */
+	write_reg(par, 0x2B, ys >> 8, ys & 0xFF, ye >> 8, ye & 0xFF);
+
+	/* Memory write */
+	write_reg(par, 0x2C);
+}
+
+#define MY (1 << 7)
+#define MX (1 << 6)
+#define MV (1 << 5)
+static int set_var(struct fbtft_par *par)
+{
+	fbtft_par_dbg(DEBUG_INIT_DISPLAY, par, "%s()\n", __func__);
+
+	/* MADCTL - Memory data access control
+	     RGB/BGR:
+	     1. Mode selection pin SRGB
+	        RGB H/W pin for color filter setting: 0=RGB, 1=BGR
+	     2. MADCTL RGB bit
+	        RGB-BGR ORDER color filter panel: 0=RGB, 1=BGR */
+	switch (par->info->var.rotate) {
+	case 0:
+		write_reg(par, 0x36, MX | MY | (par->bgr << 3));
+		break;
+	case 270:
+		write_reg(par, 0x36, MY | MV | (par->bgr << 3));
+		break;
+	case 180:
+		write_reg(par, 0x36, par->bgr << 3);
+		break;
+	case 90:
+		write_reg(par, 0x36, MX | MV | (par->bgr << 3));
+		break;
+	}
+
+	return 0;
+}
+
+#define CURVE(num, idx)  curves[num*par->gamma.num_values + idx]
+static int set_gamma(struct fbtft_par *par, unsigned long *curves)
+{
+	int i, j;
+
+	fbtft_par_dbg(DEBUG_INIT_DISPLAY, par, "%s()\n", __func__);
+
+	/* apply mask */
+	for (i = 0; i < par->gamma.num_curves; i++)
+		for (j = 0; j < par->gamma.num_values; j++)
+			CURVE(i, j) &= 0x3f;
+
+	for (i = 0; i < par->gamma.num_curves; i++)
+		write_reg(par, 0xE0 + i,
+			CURVE(i, 0), CURVE(i, 1), CURVE(i, 2), CURVE(i, 3),
+			CURVE(i, 4), CURVE(i, 5), CURVE(i, 6), CURVE(i, 7),
+			CURVE(i, 8), CURVE(i, 9), CURVE(i, 10), CURVE(i, 11),
+			CURVE(i, 12), CURVE(i, 13);
+
+	return 0;
+}
+#undef CURVE
+#endif
+
+static struct fbtft_display display = {
+	.regwidth = 8,
+	.width = 240,
+	.height = 320,
+	.init_sequence = default_init_sequence,
+	.gamma_num = 2,
+	.gamma_len = 14,
+	.gamma = DEFAULT_GAMMA,
+#if 0
+	.fbtftops = {
+		.set_addr_win = set_addr_win,
+		.set_var = set_var,
+		.set_gamma = set_gamma,
+	},
+#endif
+};
+FBTFT_REGISTER_DRIVER(DRVNAME, "sitronix,st7789v", &display);
+
+MODULE_ALIAS("spi:" DRVNAME);
+MODULE_ALIAS("platform:" DRVNAME);
+MODULE_ALIAS("spi:st7789v");
+MODULE_ALIAS("platform:st7789v");
+
+MODULE_DESCRIPTION("FB driver for the ST7789V LCD Controller");
+MODULE_AUTHOR("Noralf Tronnes");
+MODULE_LICENSE("GPL");
