#!/bin/bash

# Configure kernel in the build directory
#

# Kernel source directory name
KERNEL_DIR="linux"
# Kernel build directory name relative to kernel source directory
BUILD_DIR="../build"

# Optional argument is what kernel config to use, with "menuconfig" as default
if [ "$1" != "" ]; then
	CONFIG="$1"
else
	CONFIG="menuconfig"
fi

if [ ! -d "${KERNEL_DIR}" ]; then
	echo "Kernel source directory does not exist, exiting"
	exit 1
fi

if [ ! -d "${BUILD_DIR}" ]; then
	echo "Create kernel build directory"
	mkdir -p "${BUILD_DIR}"
else
	echo "Reconfigure existing build"
fi

(cd "${KERNEL_DIR}"; make ARCH=arm O="${BUILD_DIR}" "${CONFIG}")

echo "Done"
exit 0

