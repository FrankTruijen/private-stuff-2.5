#!/bin/bash

# Configure kernel in the build directory
#
# Requires an arm cross toolchain installed and in path

# Kernel source directory name
KERNEL_DIR="linux"
# Kernel build directory name relative to kernel source directory
BUILD_DIR="../build"
# Kernel install directory name relative to kernel source directory
INSTALL_DIR="../install"

# Do not autodetect if set in the environment
if [ "$CROSS_COMPILE" != "" ]; then
	echo "Using \"CROSS_COMPILE=$CROSS_COMPILE\" as set in environment"
else
	# Autodetect a few common cross toolchains
	CROSS_COMPILE=""
	if [ "`which arm-linux-gnueabihf-gcc 2>/dev/null`" != "" ] && [ "${CROSS_COMPILE}" = "" ]; then
		CROSS_COMPILE="arm-linux-gnueabihf-"
	fi
	if [ "`which arm-linux-gnueabi-gcc 2>/dev/null`" != "" ] && [ "${CROSS_COMPILE}" = "" ]; then
		CROSS_COMPILE="arm-linux-gnueabi-"
	fi
	if [ "`which arm-none-eabi-gcc 2>/dev/null`" != "" ] && [ "${CROSS_COMPILE}" = "" ]; then
		CROSS_COMPILE="arm-none-eabi-"
	fi
	if [ "`which arm-linux-gnu-gcc 2>/dev/null`" != "" ] && [ "${CROSS_COMPILE}" = "" ]; then
		CROSS_COMPILE="arm-linux-gnu-"
	fi

	if [ "$CROSS_COMPILE" != "" ]; then
		echo "Using \"CROSS_COMPILE=$CROSS_COMPILE\" as auto-detected"
	else
		echo "Could not find ARM cross compiler in path, exiting"
		exit 1
	fi
fi

if [ ! -d "${KERNEL_DIR}" ]; then
	echo "Kernel source directory does not exist, exiting"
	exit 1
fi

if [ ! -d "${KERNEL_DIR}/${BUILD_DIR}" ]; then
	echo "Kernel build directory does not exist, exiting"
	exit 2
fi

(cd "${KERNEL_DIR}"; make -j4 ARCH=arm CROSS_COMPILE="${CROSS_COMPILE}" O="${BUILD_DIR}") && \
(cd "${KERNEL_DIR}"; make -j4 ARCH=arm CROSS_COMPILE="${CROSS_COMPILE}" O="${BUILD_DIR}" INSTALL_MOD_PATH="${INSTALL_DIR}" modules_install) && \
(cd "${KERNEL_DIR}"; make -j4 ARCH=arm CROSS_COMPILE="${CROSS_COMPILE}" O="${BUILD_DIR}" INSTALL_MOD_PATH="${INSTALL_DIR}" firmware_install) && \
(cd "${KERNEL_DIR}"; install "${BUILD_DIR}/arch/arm/boot/zImage" "${INSTALL_DIR}/zImage")

echo "Done"
exit 0

