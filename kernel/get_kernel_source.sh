#!/bin/bash

# Get kernel source and check out specified tag and branch
#
# If changing the kernel source URL, manually remove the "${KERNEL_DIR}" directory
# Subsequent calls of this script will update (pull) the source and check out the specified
# tag and branch.

# Specify which kernel source to clone
KERNEL_URL="https://github.com/Freescale/linux-fslc.git"
# Specify what command to use to check out a tag or branch (optional)
KERNEL_BRANCH_CMD="-B 4.1-2.0.x-imx origin/4.1-2.0.x-imx"
KERNEL_TAG_CMD=""
# Kernel source directory name
KERNEL_DIR="linux"

if [ ! -d "${KERNEL_DIR}" ]; then
	echo "Clone kernel source"
	git clone "${KERNEL_URL}" "${KERNEL_DIR}"
else
	echo "Update kernel source"
	(cd "${KERNEL_DIR}"; git pull)
fi

if [ "${KERNEL_BRANCH_CMD}" != "" ] || [ "${KERNEL_TAG_CMD}" != "" ]; then
	echo "Check out specified tag and branch"
	(cd "${KERNEL_DIR}"; git fetch origin)

	# Current patches are created for repository version noted below. 
	# Patches do not apply for the current repository version. Work to  
	# keep up with the head of the software is posponed.
	(cd "${KERNEL_DIR}"; git checkout a4418c438de6ad397fd17f006ca86257fb9ec5a6) 
#	(cd "${KERNEL_DIR}"; git checkout ${KERNEL_TAG_CMD} ${KERNEL_BRANCH_CMD})
fi

echo "Done"
exit 0

