#!/bin/bash

# Requires "mkimage" from u-boot tools
# Creates and removes tmpfile "/tmp/`basename $0`.tmp"

progname="`basename $0`"
rootfs_dir="$1"
initramfs="$2"
imagename="$3"

rootfs_dir_default="rootfs"
initramfs_default="initramfs-mfgtool.u-boot"
tmpfile="/tmp/${progname}.tmp"

usage() {
	echo "Usage: ${progname} [<rootfs_dir> [<initramfs.u-boot> [image_name]]]"
	exit 1
}

if [ "$1" = "--help" ]; then
	usage
fi

if [ "${rootfs_dir}" = "" ]; then
	rootfs_dir="${rootfs_dir_default}"
	echo "No directory name given for rootfs_dir; assuming \"${rootfs_dir}\""
fi

if [ "${initramfs}" = "" ]; then
	initramfs="${initramfs_default}"
	echo "No file name given for initramfs; assuming \"${initramfs}\""
fi

if [ "${imagename}" = "" ]; then
	imagename="`basename ${initramfs}`"
	echo "No image name given; assuming \"${imagename}\""
fi

if [ ! -d "${rootfs_dir}" ]; then
	echo "The rootfs directory \"${rootfs_dir}\" does not exist, exiting"
	#too dangerous
	#rm -rf "${rootfs_dir}"
	exit 2
fi

echo "Creating initramfs \"${initramfs}\" from contents of \"${rootfs_dir}\""
rm -f "${tmpfile}" || (echo "Could not remove file \"${tmpfile}\", exiting"; exit 3)

# Behavior of cpio command differs bwteen Ubuntu 16.04 and CentOS. Help function of 
# cpio command also differs on Ubuntu 16.04 and internet. Modified next line for use 
# on Ubunto 16.04 and usage for CentOS is added as comment (Frank Truijen). 
#(cd "${rootfs_dir}"; find . | cpio -R 0:0 -c -o | gzip > "${tmpfile}")
(cd "${rootfs_dir}"; find . | cpio -R 0:0 -H newc -o | gzip > "${tmpfile}")
mkimage -A arm -T ramdisk -C gzip -n "${imagename}" -d "${tmpfile}" "${initramfs}"
rm -f "${tmpfile}"

# Also create initramfs for usage in flash image.
(cd "${rootfs_dir}"; tar -cJf ../rootfs.tar.xz *)

echo "Done"

exit 0

