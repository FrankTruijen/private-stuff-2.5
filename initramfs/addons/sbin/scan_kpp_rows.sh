#!/bin/bash

# Script to scan keypad rows, displaying in the following format
#
# "<date and time> Keypad row[0..4] = {a, b, c, d, e}"
#
# With a = row[0], value <0|1>
#      b = row[1], value <0|1>
#      c = row[2], value <0|1>
#      d = row[3], value <0|1>
#      e = row[4], value <0|1>
#
# Bas Mevissen, January 2017
#

# Linux sysfs GPIO numbers of keypad rows
kpp_gpio_nr_row0=38 # GPIO2_IO6
kpp_gpio_nr_row1=36 # GPIO2_IO4
kpp_gpio_nr_row2=34 # GPIO2_IO2
kpp_gpio_nr_row3=32 # GPIO2_IO0
kpp_gpio_nr_row4=6  # GPIO1_IO6 (LPSR)

# Export GPIO pins
for gpio_nr in $kpp_gpio_nr_row0 $kpp_gpio_nr_row1 $kpp_gpio_nr_row2 $kpp_gpio_nr_row3 $kpp_gpio_nr_row4; do
	echo $gpio_nr > /sys/class/gpio/export
done

# Show kp row values in endless loop
while true; do
	kpp_gpio_val_row0="`cat /sys/class/gpio/gpio$kpp_gpio_nr_row0/value`"
	kpp_gpio_val_row1="`cat /sys/class/gpio/gpio$kpp_gpio_nr_row1/value`"
	kpp_gpio_val_row2="`cat /sys/class/gpio/gpio$kpp_gpio_nr_row2/value`"
	kpp_gpio_val_row3="`cat /sys/class/gpio/gpio$kpp_gpio_nr_row3/value`"
	kpp_gpio_val_row4="`cat /sys/class/gpio/gpio$kpp_gpio_nr_row4/value`"
	echo "`date` Keypad row[0..4] = {$kpp_gpio_val_row0, $kpp_gpio_val_row1, $kpp_gpio_val_row2, $kpp_gpio_val_row3, $kpp_gpio_val_row4}"
	usleep 200000
done

