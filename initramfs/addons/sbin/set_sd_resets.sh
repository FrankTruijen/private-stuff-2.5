#!/bin/bash

# Script to set or alternate SD1 and SD2 reset lines
#
# Bas Mevissen, January 2017
#

progname="`basename $0`"
arg1=$1

# Linux sysfs GPIO numbers of SD1 and SD2 NRESET lines
sd1_reset_gpio_nr=130 # GPIO5_IO02
sd2_reset_gpio_nr=139 # GPIO5_IO11

# Export GPIO pins and set as output
setup_gpio() {
	for gpio_nr in $sd1_reset_gpio_nr $sd2_reset_gpio_nr; do
		if [ ! -d /sys/class/gpio/gpio${gpio_nr} ]; then
			echo "${gpio_nr}" > /sys/class/gpio/export
		fi
		echo "out" > /sys/class/gpio/gpio${gpio_nr}/direction
	done
}

# Usage help
usage() {
	echo "Usage: ${progname} <flash|0|1>"
	echo "0 or 1 sets both reset lines to the requested value"
	echo "flash lets both lines change between 0 and 1 every 500ms"
	exit 1
}

# Set GPIO value
set_gpio_val() {
	gpio_nr=$1
	val=$2
	echo "${val}" > /sys/class/gpio/gpio${gpio_nr}/value
}
	
# Main program
if [ "$arg1" = "" ]; then
	usage
else 
	echo "$progname - Setting SD1_RESETN and SD2_RESETN"
	echo "Requires pinmux of both pens to be at GPIO!!!"
if [ "$arg1" = "0" ] || [ "$arg1" = "1" ]; then
	setup_gpio
	set_gpio_val $sd1_reset_gpio_nr	$arg1
	set_gpio_val $sd2_reset_gpio_nr	$arg1
	echo "SD1_RESETN=$arg1; SD2_RESETN=$arg1"
else if [ "$arg1" = "flash" ]; then
	setup_gpio
	set_gpio_val $sd1_reset_gpio_nr	$value 1
	set_gpio_val $sd2_reset_gpio_nr	$value 1
	echo "SD1_RESETN=1; SD2_RESETN=1"
	while true; do
		usleep 500000
		set_gpio_val $sd1_reset_gpio_nr	$value 0
		set_gpio_val $sd2_reset_gpio_nr	$value 0
		echo "SD1_RESETN=0; SD2_RESETN=0"
		usleep 500000
		set_gpio_val $sd1_reset_gpio_nr	$value 1
		set_gpio_val $sd2_reset_gpio_nr	$value 1
		echo "SD1_RESETN=1; SD2_RESETN=1"
	done
else
	usage
fi
fi
fi

