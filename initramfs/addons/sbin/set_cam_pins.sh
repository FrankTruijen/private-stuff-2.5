#!/bin/bash

# Script to set or alternate CAM_RESET and CAM_PWRDWN lines
#
# Bas Mevissen, January 2017
#

progname="`basename $0`"
arg1=$1
arg2=$2

# Linux sysfs GPIO numbers of CAM_RESET and CAM_PWRDWN lines
cam_reset_gpio_nr=9 # GPIO1_IO09
cam_power_gpio_nr=10 # GPIO1_IO10

# Export GPIO pins and set as output
setup_gpio() {
	for gpio_nr in $cam_reset_gpio_nr $cam_power_gpio_nr; do
		if [ ! -d /sys/class/gpio/gpio${gpio_nr} ]; then
			echo "${gpio_nr}" > /sys/class/gpio/export
		fi
		echo "out" > /sys/class/gpio/gpio${gpio_nr}/direction
	done
}

# Usage help
usage() {
	echo "Usage: ${progname} <flash | <0|1> <0|1>>"
	echo "0 or 1 sets the CAM_RESET and CAM_PWRDWN lines to the requested value"
	echo "flash lets both lines change between 0 and 1 every 500ms"
	exit 1
}

# Set GPIO value
set_gpio_val() {
	gpio_nr=$1
	val=$2
	echo "${val}" > /sys/class/gpio/gpio${gpio_nr}/value
}
	
# Main program
if [ "$arg1" = "" ]; then
	usage
else 
	echo "$progname - Setting CAM_RESET and CAM_PWRDWN"
	echo "Requires pinmux of both pens to be at GPIO."
	echo ""
if [ "$arg1" = "flash" ]; then
	setup_gpio
	set_gpio_val $cam_reset_gpio_nr	$value 1
	set_gpio_val $cam_power_gpio_nr	$value 1
	echo "CAM_RESET=1; CAM_PWRDWN=1"
	while true; do
		usleep 500000
		set_gpio_val $cam_reset_gpio_nr	$value 0
		set_gpio_val $cam_power_gpio_nr	$value 0
		echo "CAM_RESET=0; CAM_PWRDWN=0"
		usleep 500000
		set_gpio_val $cam_reset_gpio_nr	$value 1
		set_gpio_val $cam_power_gpio_nr	$value 1
		echo "CAM_RESET=1; CAM_PWRDWN=1"
	done

else if [[ ("$arg1" = "0" || "$arg1" = "1") && ("$arg2" = "0" || "$arg2" = "1") ]]; then
	setup_gpio
	set_gpio_val $cam_reset_gpio_nr	$arg1
	set_gpio_val $cam_power_gpio_nr	$arg2
	echo "CAM_RESET=$arg1; CAM_PWRDWN=$arg2"
else
	usage
fi
fi
fi

