#!/bin/bash

# Includes contents of addons directory in rootfs directory

progname="`basename $0`"
rootfs_dir="$1"
addons_dir="$2"

rootfs_dir_default="rootfs"
addons_dir_default="addons"

usage() {
	echo "Usage: ${progname} [<rootfs_dir> [addons_dir]]"
	exit 1
}

if [ "$1" = "--help" ]; then
	usage
fi

if [ "${rootfs_dir}" = "" ]; then
	rootfs_dir="${rootfs_dir_default}"
	echo "No directory name given for rootfs_dir; assuming \"${rootfs_dir}\""
fi

if [ "${addons_dir}" = "" ]; then
	addons_dir="${addons_dir_default}"
	echo "No directory name given for addons_dir; assuming \"${addons_dir}\""
fi

if [ ! -d "${rootfs_dir}" ]; then
	echo "The rootfs directory \"${rootfs_dir}\" does not exist, exiting"
	exit 2
fi

if [ ! -d "${addons_dir}" ]; then
	echo "The addons directory \"${addons_dir}\" does not exist, exiting"
	exit 3
fi

echo "Adding contents of \"${addons_dir}\" to \"${rootfs_dir}\""
cp -arTv "${addons_dir}" "${rootfs_dir}"

echo "Done"

exit 0

