#!/bin/bash

# Requires "mkimage" from u-boot tools

progname="`basename $0`"
initramfs="$1"
rootfs_dir="$2"

rootfs_dir_default="rootfs"

usage() {
	echo "Usage: ${progname} <initramfs.u-boot> [rootfs_dir]"
	exit 1
}

if [ "$1" = "--help" ]; then
	usage
fi

if [ "${initramfs}" = "" ]; then
	usage
fi

if [ "${rootfs_dir}" = "" ]; then
	rootfs_dir="${rootfs_dir_default}"
	echo "No directory name given for rootfs_dir; assuming \"${rootfs_dir}\""
fi

if [ -e "${rootfs_dir}" ]; then
	echo "The rootfs directory (or file) \"${rootfs_dir}\" exists, please remove it or choose different directory, exiting"
	#too dangerous
	#rm -rf "${rootfs_dir}"
	exit 2
fi

echo "Header information of initramfs file \"${initramfs}\":"
mkimage -l "${initramfs}"

echo "Extracting \"${initramfs}\" to \"${rootfs_dir}\""
mkdir -p "${rootfs_dir}" || (echo "Could not create directory \"${rootfs_dir}\", exiting"; exit 3)
cat "${initramfs}" | (cd "${rootfs_dir}"; tail -c +65 | gunzip | cpio -idmv)

echo "Done"

exit 0

